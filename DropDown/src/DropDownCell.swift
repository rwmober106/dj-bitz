//
//  DropDownCellTableViewCell.swift
//  DropDown
//
//  Created by Kevin Hirsch on 28/07/15.
//  Copyright (c) 2015 Kevin Hirsch. All rights reserved.
//

import UIKit

open class DropDownCell: UITableViewCell {
		
	//UI
    @IBOutlet open weak var selectedShadow: UIView!
	@IBOutlet open weak var optionLabel: UILabel!
	
	var selectedBackgroundColor: UIColor?

}

//MARK: - UI

extension DropDownCell {
	
	override open func awakeFromNib() {
		super.awakeFromNib()
		
		backgroundColor = .clear
        
        self.selectedShadow.backgroundColor = UIColor(red: 69.0/255.0, green: 69.0/255.0, blue: 69.0/255.0, alpha: 1.0)
        self.selectedShadow.layer.shadowColor = UIColor(white: 0.0, alpha: 0.6).cgColor
        self.selectedShadow.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.selectedShadow.layer.shadowRadius = 2
        self.selectedShadow.layer.shadowOpacity = 1
        self.selectedShadow.clipsToBounds = false
        self.selectedShadow.isHidden = true
	}
	
	override open var isSelected: Bool {
		willSet {
			setSelected(newValue, animated: false)
		}
	}
	
	override open var isHighlighted: Bool {
		willSet {
			setSelected(newValue, animated: false)
		}
	}
	
	override open func setHighlighted(_ highlighted: Bool, animated: Bool) {
		setSelected(highlighted, animated: animated)
	}
	
	override open func setSelected(_ selected: Bool, animated: Bool) {
		let executeSelection: () -> Void = { [weak self] in
			guard let `self` = self else { return }

			if let selectedBackgroundColor = self.selectedBackgroundColor {
				if selected {
					self.backgroundColor = selectedBackgroundColor
                    self.selectedShadow.isHidden = false
				} else {
					self.backgroundColor = .clear
                    self.selectedShadow.isHidden = true
				}
			}
		}
		
		if animated {
			UIView.animate(withDuration: 0.3, animations: {
				executeSelection()
			})
		} else {
			executeSelection()
		}
	}
	
}
