//
//  CommentTableViewCell.swift
//  DJBITZ
//
//  Created by dung on 5/4/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet var avatar: UIImageView!
    @IBOutlet var labelName: UILabel!
    @IBOutlet var labelComment: UILabel!
    @IBOutlet var viewComment: UIView!
    @IBOutlet var viewRemove: UIView!
    @IBOutlet var buttonRemove: UIButton!

    //Control Section
    @IBOutlet var avatarImage: UIImageView!
    @IBOutlet fileprivate weak var buttonShuffle: UIButton!
    @IBOutlet fileprivate weak var buttonRepeat: UIButton!
    @IBOutlet fileprivate weak var buttonShare: UIButton!

    @IBOutlet fileprivate weak var HQView: UIView!
    @IBOutlet var labelArtist: UILabel!
    @IBOutlet var labelSong: UILabel!

    @IBOutlet var buttonLike: UIButton!
    @IBOutlet var labelLikeCount: UILabel!
    @IBOutlet var labelListenCount: UILabel!

    @IBOutlet var tfComment: UITextField!
    @IBOutlet fileprivate weak var buttonComment: UIButton!

    var music:Music? = nil
    
    public var callback: ((_ comments:[Comment]) -> ())? = nil
    public var removeComment: ((_ cell:CommentTableViewCell) -> ())? = nil
    public var didFavourite: (() -> ())? = nil

    //MARK:-
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if self.tfComment != nil {
            self.tfComment.text = ""
        }
        
        if self.viewComment != nil {
            self.viewComment.layer.cornerRadius = 5
            self.viewComment.clipsToBounds = true
        }
        
        if (self.buttonShuffle != nil) {
            self.buttonShuffle.layer.cornerRadius = self.buttonShuffle.frame.height/2
            self.buttonShuffle.clipsToBounds = true

            if AppDelegate.shared().isShuffled {
                self.buttonShuffle.setImage(UIImage(named: "ic_shuffle"), for: .normal)
            }
            else {
                self.buttonShuffle.setImage(UIImage(named: "ic_shuffle_disable"), for: .normal)
            }
        }
        
        if self.buttonRepeat != nil {
            self.buttonRepeat.layer.cornerRadius = self.buttonRepeat.frame.height/2
            self.buttonRepeat.clipsToBounds = true

            if AppDelegate.shared().isRepeat {
                self.buttonRepeat.setImage(UIImage(named: "ic_repeat"), for: .normal)
            }
            else {
                self.buttonRepeat.setImage(UIImage(named: "ic_repeat_disable"), for: .normal)
            }
        }
        
        if self.buttonShare != nil {
            self.buttonShare.layer.shadowColor = UIColor.black.cgColor
            self.buttonShare.layer.shadowOffset = CGSize(width: 0, height: 0)
            self.buttonShare.layer.shadowRadius = 1
            self.buttonShare.layer.shadowOpacity = 1.0
        }
        
        if self.avatarImage != nil {
            self.avatarImage.layer.cornerRadius = self.avatarImage.frame.height/2
            self.avatarImage.clipsToBounds = true
        }
        
        if self.avatar != nil {
            self.avatar.layer.cornerRadius = self.avatar.frame.height/2
            self.avatar.clipsToBounds = true
        }
        
        if self.HQView != nil {
            self.HQView.layer.cornerRadius = 3
            self.HQView.clipsToBounds = true
        }
        
        if self.buttonComment != nil {
            self.buttonComment.layer.cornerRadius = self.buttonComment.frame.height/2
            self.buttonComment.clipsToBounds = true
            
            self.buttonComment.setTitle("Comment", for: .normal)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //MARK:- IBAction
    @IBAction func onButtonShuffle(_ sender: UIButton) {
        AppDelegate.shared().shuffle()
        
        if AppDelegate.shared().isShuffled {
            self.buttonShuffle.setImage(UIImage(named: "ic_shuffle"), for: .normal)
        }
        else {
            self.buttonShuffle.setImage(UIImage(named: "ic_shuffle_disable"), for: .normal)
        }
        self.buttonRepeat.setImage(UIImage(named: "ic_repeat_disable"), for: .normal)
    }
    
    @IBAction func onButtonRepeat(_ sender: UIButton) {
        AppDelegate.shared().setRepeat()
        
        if AppDelegate.shared().isRepeat {
            self.buttonRepeat.setImage(UIImage(named: "ic_repeat"), for: .normal)
        }
        else {
            self.buttonRepeat.setImage(UIImage(named: "ic_repeat_disable"), for: .normal)
        }
        self.buttonShuffle.setImage(UIImage(named: "ic_shuffle_disable"), for: .normal)
    }
    
    @IBAction func onButtonShare(_ sender: UIButton) {
        var message = "\n"
        
        if let productName = self.music?.name {
            message += productName
        }

        if let DJ = self.music?.DJ {
            message = message + "\n\n" + DJ
        }

        if let descriptionString = self.music?.desc {
            message = message + "\n\n" + descriptionString
        }

        if let url = self.music?.music {
            message = message + "\n\n" + String(format: "http://18.235.241.120/%@", url)
        }

        let imageToShare = [message] as [Any]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
//        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash

        // present the view controller
//        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func onButtonLike(_ sender: UIButton) {
        if didFavourite != nil {
            didFavourite!()
        }
    }
    
    @IBAction func onButtonComment(_ sender: UIButton) {
        tfComment.resignFirstResponder()
        
        if let comment = self.tfComment.text, comment.count > 0, let id = self.music?.id {
            if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                if let viewController = navi.viewControllers.first as? ViewController {
                    viewController.activeActivity(true)
                    DJBitzAPI.shared.addComment(email: ReferenceHelper.sharedInstance.loginInfo![ReferenceHelper.key_login_email]!, music_id: id, comment: comment) { (success, comment_id, message) in
                        if success {
                            DJBitzAPI.shared.getMusicInfo(self.music!.id!, { (success, music, comments, message) in
                                viewController.activeActivity(false)
                                if success {
                                    if self.callback != nil {
                                        self.callback!(comments)
                                    }
                                }
                            })
                        }
                        else {
                            viewController.activeActivity(false)
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func onButtonRemove(_ sender: UIButton) {
        if self.removeComment != nil {
            self.removeComment!(self)
        }
    }

    
    //MARK:- UITextFieldDelegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        self.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
