//
//  LiveInfoViewController.swift
//  DJBITZ
//
//  Created by Rakinder on 04/06/20.
//  Copyright © 2020 BUGUNSOFT. All rights reserved.
//

import UIKit
import  AgoraRtcKit

class LiveInfoViewController: UIViewController {
    
    //MARK: - Outlet Declaration
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnLive: UIButton!

    var viewController:ViewController? = nil
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
        super.viewDidLoad()
        btnLive.layer.cornerRadius =  btnLive.frame.size.height/2
        btnCancel.layer.cornerRadius = btnLive.frame.size.height/2
        
        if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            if let viewController = navi.viewControllers.first as? ViewController {
                self.viewController = viewController
                
            }
        }
       
    }
    
    //MARK: - Button Actions
    @IBAction func cancelClicked(_ sender: Any) {
        hideView()
//        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func liveClicked(_ sender: Any) {
        
        btnLive.isUserInteractionEnabled = false
        btnCancel.isUserInteractionEnabled = false
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        btnLive.setImage(nil, for: .normal)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LiveViewController") as! LiveViewController
        hideView()
        vc.isFromNotification = true
        vc.isBroadcaster = true
        vc.strChannel = ""
        vc.strUsername = ReferenceHelper.sharedInstance.userInfo!.username!
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK: - Common Function
    func hideView(){
        if  self.viewController?.tabbar != nil{
             self.viewController?.tabbar.isHidden = false
        }
        if self.tabBarController != nil{
             self.tabBarController?.tabBar.isHidden = false
        }
        self.view.isHidden = true
        UIView.transition(with: view, duration: 0.0, options: .curveEaseInOut, animations: {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.setNeedsLayout()
            self.view.frame = CGRect(x:  0  ,y: 0 ,width: self.view.frame.size.width,height: self.view.frame.size.height);
        }, completion: {(_ finished: Bool) -> Void in
            self.view.removeFromSuperview()
        })
    }
}

