//
//  ChosenGenreViewController.swift
//  DJBITZ
//
//  Created by dung on 2/21/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import UIKit
import Alamofire

class ChosenGenreViewController: ActivityIndicatorViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {

    @IBOutlet fileprivate weak var collectionView: UICollectionView!
    @IBOutlet fileprivate weak var headerView: UIView!
    @IBOutlet fileprivate weak var buttonBack: UIButton!
    @IBOutlet fileprivate weak var buttonSearch: UIButton!
    @IBOutlet fileprivate weak var labelTitle: UILabel!
    @IBOutlet fileprivate weak var searchBar: UISearchBar!
    @IBOutlet var downloadProgessView: UIProgressView!
    private var localMixes: [Music]? = nil
    
    var isSearching = false
    
    var genre:Genre? = nil
    var chosenMusicsList:MusicsList? = nil
    var results = [Music]()

    var openComment = false
    var openIndex:Int = -1

    private var currentTextField: UITextField?

    //MARK:- Life Circle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        
        self.headerView.layer.shadowColor = UIColor.black.cgColor
        self.headerView.layer.shadowOpacity = 0.7
        self.headerView.layer.shadowOffset = CGSize(width: 0, height: 1)

        // Do any additional setup after loading the view.
        
        self.labelTitle.text = self.genre?.name
        
        if let records = self.chosenMusicsList?.musicRecords {
            self.results = records.filter({$0.music!.count > 0})
            self.collectionView.reloadData()
        }
        
        self.searchBar.customizedSearchBar()
        
        self.activeSearchBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
        
        if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            if let viewController = navi.viewControllers.first as? ViewController {
                if AppDelegate.shared().player != nil {
                    viewController.initUI()
                    viewController.playerView.isHidden = false
                }
                else {
                    viewController.playerView.isHidden = true
                }
            }
        }
    }
    
    //MARK:- Action Handle
    @IBAction func onButtonBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onButtonSearch(_ sender: UIButton) {
        self.isSearching = !self.isSearching
        self.activeSearchBar()
    }

    func activeSearchBar() {
        if self.isSearching {
            self.searchBar.isHidden = false
        }
        else {
            self.searchBar.isHidden = true
        }
    }

    //MARK:- UISearchBarDelegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let _ = self.chosenMusicsList, let ms = self.chosenMusicsList?.musicRecords?.filter({($0.music?.count)! > 0}) else {
            return
        }
        
        if searchText.count > 0 {
            self.results = ms.filter({($0.name?.contains(searchText))!})
        }
        else {
            self.results = ms
        }
        
        self.collectionView.reloadData()
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.isSearching = false
        self.searchBar.isHidden = true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:- UICollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.results.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.bounds.width*0.9, height: (openComment && openIndex == indexPath.row) ? 180: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let recentlyCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChosenOneCollectionViewCell", for: indexPath) as! RecentlyCollectionViewCell
        
        recentlyCell.setupCell(self.results[indexPath.row])
        recentlyCell.complete = self.downloadMusic
        recentlyCell.callback = self.comment
        recentlyCell.didFavourite = favouriteMusic
        recentlyCell.textFieldDidBeginEditing = self.textFieldDidBeginEditing

        return recentlyCell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        // Plays audio from remote url
        AppDelegate.shared().islocal = false
        
        let newList = MusicsList.init(self.results, self.chosenMusicsList!.searchText)
        AppDelegate.shared().currentSongIndex = indexPath.row
        AppDelegate.shared().updateMusicsList(newList!)

        let controller = CurrentMixViewController.sharedPlayer()
        if let _ = controller.view.superview {
            controller.navigationController?.popViewController(animated: false)
        }
        
        controller.music = AppDelegate.shared().musicsList?.musicRecords![indexPath.row]

        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:-
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.currentTextField = textField
    }
    
    func downloadMusic(_ cell: RecentlyCollectionViewCell) {
        if let indexPath = self.collectionView.indexPath(for: cell) {
            let music = self.results[indexPath.row]
            let url = String(format: "%@%@", DJBitzAPI.mainURL, music.music!)
            let mixName = music.name
            let imgUrl = String(format: "%@%@", DJBitzAPI.mainURL, music.thumb!)
            let dj = music.DJ
            let duration = music.duration
            
            // Downloads mix and thumb
            self.downloadProgessView.isHidden = false
            guard let requestURL = URL(string: url) else {
                return
            }            
           
            let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                documentsURL.appendPathComponent(String(format: "%@.mp3", mixName!))
                return (documentsURL, [.removePreviousFile])
            }
            
            self.activeActivity(true)
            alamofireManager.download(requestURL, to: destination).downloadProgress { (progress) in
                print("download progress: ", progress.fractionCompleted*100)
                DispatchQueue.main.async {
                    self.downloadProgessView.setProgress(Float(progress.fractionCompleted), animated: true)
                }
                
            }.response { (res) in
                self.activeActivity(false)
                self.downloadProgessView.isHidden = true
                self.downloadProgessView.setProgress(0.0, animated: false)
                if let error  = res.error {
                    print("download error: %@", error.localizedDescription)
                } else {
                    print("download success")
                    if let mixPath = res.destinationURL?.path {
                        print("mixPath: %@", mixPath)
                    }
                    
                    guard let requestURL1 = URL(string: imgUrl) else {
                        return
                    }
                    
                    let imgDestination: DownloadRequest.DownloadFileDestination = { _, _ in
                        var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                        documentsURL.appendPathComponent(String(format: "%@.jpg", mixName!))
                        return (documentsURL, [.removePreviousFile])
                    }
                    
                    self.activeActivity(true)
                    alamofireManager.download(requestURL1, to: imgDestination).downloadProgress { (progress) in
                        print(progress.fractionCompleted)
                    }.response { (res) in
                        self.activeActivity(false)
                        if let error  = res.error {
                            print("download error: %@", error.localizedDescription)
                        } else {
                            print("download success")
                            
                            //Save local mixes
                                                        
                            let userdefaults = UserDefaults.standard
                            if let localMixData = userdefaults.object(forKey: "localmixes") {
                                let localmixes = NSKeyedUnarchiver.unarchiveObject(with: localMixData as! Data) as! [Music]
                                self.localMixes = localmixes
                            } else {
                                self.localMixes = [Music]()
                            }
                            self.localMixes?.append(music)
                            
                            userdefaults.setValue(NSKeyedArchiver.archivedData(withRootObject: self.localMixes!), forKey: "localmixes")
                            
                            userdefaults.set(music.id, forKey: music.id!)
                            
                        }
                        
                    }
                    
                    
                }
                
                
            }
            
            
        }
    }
    
    func addPlaylist(_ cell: RecentlyCollectionViewCell) {
        if let indexPath = self.collectionView.indexPath(for: cell) {
            let music = self.results[indexPath.row]
            if music.is_playlist == "0" {
                self.activeActivity(true)
                DJBitzAPI.shared.addPlaylist(email: ReferenceHelper.sharedInstance.loginInfo![ReferenceHelper.key_login_email]!, music_id: music.id!) { (success, message) in
                    if success {
                        DJBitzAPI.shared.getMusicsList(byGenre: self.genre!.id) { (success, list, error) in
                            // Must call in main thread
                            DispatchQueue.main.async {
                                // Hide progress activity
                                self.activeActivity(false)
                                
                                if (!success) {
                                    //Show error alert
                                    self.showAlert(error!)
                                    
                                    return
                                }
                                
                                if let records = list!.musicRecords {
                                    self.results = records.filter({$0.music!.count > 0})
                                    self.collectionView.reloadData()
                                }

                            }
                        }
                    }
                    else {
                        self.activeActivity(false)
                        self.showAlert(message!)
                    }
                }
            }
        }
    }

    func favouriteMusic(_ cell:RecentlyCollectionViewCell) {
        if let indexPath = self.collectionView.indexPath(for: cell) {
            let music = self.results[indexPath.row]
            if let liked = music.is_liked {
                self.activeActivity(true)
                DJBitzAPI.shared.likeMusic(email: ReferenceHelper.sharedInstance.loginInfo![ReferenceHelper.key_login_email]!, music_id: music.id!, liked == "1" ? true:false) { (success, message) in
                    if success {
                        DJBitzAPI.shared.getMusicsList(byGenre: self.genre!.id) { (success, list, error) in
                            // Must call in main thread
                            DispatchQueue.main.async {
                                // Hide progress activity
                                self.activeActivity(false)
                                
                                if (!success) {
                                    //Show error alert
                                    self.showAlert(error!)
                                    
                                    return
                                }
                                
                                if let records = list!.musicRecords {
                                    self.results = records.filter({$0.music!.count > 0})
                                    self.collectionView.reloadData()
                                }

                            }
                        }
                    }
                    else {
                        self.activeActivity(false)
                        self.showAlert(message!)
                    }
                }
            }
            
            
        }
    }

    func comment(_ cell:RecentlyCollectionViewCell) {
        
        var needReloadCells:[IndexPath] = []
        
        if self.collectionView.cellForItem(at: IndexPath(row: openIndex, section: 0)) != nil {
            needReloadCells.append(IndexPath(row: openIndex, section: 0))
        }
        
        if let indexpath = self.collectionView.indexPath(for: cell) {
            
            if openIndex != indexpath.row {
                openIndex = indexpath.row
                openComment = true
                
                needReloadCells.append(IndexPath(row: openIndex, section: 0))
            }
            else {
                openIndex = -1
                openComment = false
            }
        }
        
        if needReloadCells.count > 0 {
            UIView.animate(withDuration: 0.8, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.9, options: UIView.AnimationOptions.curveEaseInOut, animations: {
                self.collectionView.reloadItems(at: needReloadCells)
            }, completion: { success in
                print("success")
            })
        }
    }
    
    func showAlert(_ message:String) {
        let alert = UIAlertController(title: "ERROR", message:
            message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel,handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}
