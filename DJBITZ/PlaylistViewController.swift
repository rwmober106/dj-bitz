//
//  PlaylistViewController.swift
//  DJBITZ
//
//  Created by MacLover on 10/7/20.
//  Copyright © 2020 BUGUNSOFT. All rights reserved.
//

import UIKit

class PlaylistViewController: UIViewController, UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout {

    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var headerView: UIView!
    
    var musicsList: MusicsList? = nil
    var results = [Music]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        
        self.headerView.layer.shadowColor = UIColor.black.cgColor
        self.headerView.layer.shadowOpacity = 0.7
        self.headerView.layer.shadowOffset = CGSize(width: 0, height: 1)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 15, right: 0)
                
        if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            if let viewController = navi.viewControllers.first as? ViewController {
                viewController.activeActivity(true)
                
                let urlString = String(format: "%@index.php/getplaylist", DJBitzAPI.mainURL)
                let email = ReferenceHelper.sharedInstance.userInfo!.email!
                
                DJBitzAPI.shared.getPlaylist(urlString, email) { (success, list, error) in
                    // Must call in main thread
                    DispatchQueue.main.async {
                        // Hide progress activity
                        viewController.activeActivity(false)
                        
                        if (!success) {
                            let alert = UIAlertController(title: "Error", message: "Error while loading data from server. Please check your network connection and try again later.", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                            self.present(alert, animated: true)
                            
                            return
                        }

                        //reload data
                        if list?.searchText == nil {
                            list?.searchText = false
                        }
                        self.musicsList = list
                        self.results = (self.musicsList?.musicRecords)!.filter({($0.music?.count)! > 0})
                        self.collectionView.reloadData()
                    }
                }
            }
        }
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
        
        if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            if let viewController = navi.viewControllers.first as? ViewController {
                if AppDelegate.shared().player != nil {
                    viewController.initUI()
                    viewController.playerView.isHidden = false
                    viewController.closeMediaPlayerCallback = closeMediaPlayer
                    
                    self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 60, right: 0)
                }
                else {
                    if viewController.playerView != nil{
                         viewController.playerView.isHidden = true
                    }
                   
                }
            }
        }
    }
        
    //MARK:- UICollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return results.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.bounds.width*0.9, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let recentlyCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlaylistCollectionViewCell", for: indexPath) as! RecentlyCollectionViewCell
        
        recentlyCell.setupCell(results[indexPath.row])
        recentlyCell.didDelete = self.deleteMusicFromPlaylist

        return recentlyCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {           
        
        let newList = MusicsList.init(self.results, self.musicsList?.searchText)
        AppDelegate.shared().currentSongIndex = indexPath.row
        AppDelegate.shared().updateMusicsList(newList!)
        
        let controller = CurrentMixViewController.sharedPlayer()
        if let _ = controller.view.superview {
            controller.navigationController?.popViewController(animated: false)
        }

        controller.music = AppDelegate.shared().musicsList?.musicRecords![indexPath.row]

        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func deleteMusicFromPlaylist(_ cell: RecentlyCollectionViewCell) {
        
        if let indexPath = self.collectionView.indexPath(for: cell) {
            let music = self.results[indexPath.row]
            if music.is_playlist == "1" {
                
                if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                    if let viewController = navi.viewControllers.first as? ViewController {
                        viewController.activeActivity(true)
                        
                        DJBitzAPI.shared.deletePlaylist(email: ReferenceHelper.sharedInstance.loginInfo![ReferenceHelper.key_login_email]!, music_id: music.id!) { (success, message) in
                            if success {
                                
                                viewController.activeActivity(false)
                                self.results.remove(at: indexPath.row)
                                self.collectionView.reloadData()
                                
                                
                            } else {
                                viewController.activeActivity(false)
                                self.showAlert(message!)
                            }
                        }
                        
                    }
                }
                
            }
        }
        
    }
    
    func showAlert(_ message:String) {
        let alert = UIAlertController(title: "ERROR", message:
            message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel,handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Close MediaPlayer
    func closeMediaPlayer() {
        self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 15, right: 0)
    }

}
