//
//  AppDelegate.swift
//  DJBITZ
//
//  Created by dung on 2/20/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase
import UserNotifications
import FirebaseMessaging
import MediaPlayer


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    public static func shared() -> AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    static let themeColor_enable = UIColor(red: 171.0/255.0, green: 38.0/255.0, blue: 250.0/255.0, alpha: 1.0)
    static let themeColor_disable = UIColor(red: 127.0/255.0, green: 127.0/255.0, blue: 127.0/255.0, alpha: 1.0)
    
    static let key_shuffle = "shuffle"
    static let key_repeat = "repeat"
    static let key_badge = "badge"
    
    //MARK:- MediaPlayer
    
    public var musicsList: MusicsList? = nil
    public var artistsList: ArtistsList? = nil
    public var genresList: GenresList? = nil
    
    var currentMusic: Music? = nil
    var currentArtist: ArtistInfomation? = nil

    var player: AVQueuePlayer?
    //var playerLooper: AVPlayerLooper?
    var observer: Any?
    var isReady = false
    var isStopped = true
    var sliderValue = Float(0.0)
    
    var isRepeat = false
    var isShuffled = false
    var currentSongIndex = Int(-1)
    
    public var prepareCallback: (() -> ())? = nil
    public var readyCallback: (() -> ())? = nil
    public var callback: ((_ time: CMTime) -> ())? = nil
    public var endedCallback: (() -> ())? = nil
    
    public var showLoader: (() -> ())? = nil
    public var hideLoader: (() -> ())? = nil
    
    var shuffledList = [Music]()
    
    var badgeCount: Int? = nil
    
    public var islocal = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        if let shuffled = UserDefaults.standard.value(forKey: AppDelegate.key_shuffle) as? Bool
        {
            self.isShuffled = shuffled
        }
        else {
            UserDefaults.standard.setValue(false, forKey: AppDelegate.key_shuffle)
            UserDefaults.standard.synchronize()
        }
        
        if let isRepeat = UserDefaults.standard.value(forKey: AppDelegate.key_repeat) as? Bool
        {
            self.isRepeat = isRepeat
        }
        else {
            UserDefaults.standard.setValue(false, forKey: AppDelegate.key_repeat)
            UserDefaults.standard.synchronize()
        }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default, options: .mixWithOthers)
            print("AVAudioSession Category Playback OK")
            do {
                try AVAudioSession.sharedInstance().setActive(true)
                print("AVAudioSession is Active")
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        FirebaseApp.configure()
        
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self
        // [END set_messaging_delegate]
        
        // For iOS 10 display notification (sent via APNS)
        UNUserNotificationCenter.current().delegate = self
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        
        application.registerForRemoteNotifications()
        
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        
        if let notification = launchOptions?[.remoteNotification] as? [String: AnyObject] {
                   do {
                    
                    
                    
                 //    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "liveStreamingNotification"), object: nil, userInfo: notification)
                 //    displayRemoteNotification1(notification)
                    if ReferenceHelper.sharedInstance.loginInfo != nil, ReferenceHelper.sharedInstance.userInfo != nil {
                              
                    if let token = notification["gcm.notification.token"] as? String, token != ""{
                        let roomName = notification["gcm.notification.channelName"] as? String ?? ""
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let navC = storyboard.instantiateInitialViewController() as? UINavigationController
                        let rootViewC1 = storyboard.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                        let rootViewC2 = storyboard.instantiateViewController(withIdentifier: "LiveViewController") as? LiveViewController
                        rootViewC2?.isFromNotification = true
                        rootViewC2?.isBroadcaster = false
                        rootViewC2?.strChannel = roomName
                        rootViewC2?.strUsername = notification["gcm.notification.name"] as? String ?? ""
                        let rootViewC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as? TabBarViewController
                        rootViewC?.selectedIndex = 0
                        navC?.viewControllers = [ rootViewC1!, rootViewC!, rootViewC2!]
                        rootViewC?.selectedIndex = 0
                        self.window!.rootViewController = navC
                        self.window!.makeKeyAndVisible()
                        }
                    }
                    else{
                        
                    }
                   } catch { }
               }
               
        
        self.setupAudioSession()
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        print("resign active ")
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        updatePlayerUI()
        
        /*
        if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            if let viewController = navi.viewControllers.first as? ViewController {
                if let currentNavi = viewController.tabbarController.selectedViewController as? UINavigationController {
                    if let currentMix = currentNavi.viewControllers.last as? CurrentMixViewController {
                        currentMix.music = self.currentMusic
                        currentMix.tableView.reloadData()
                    }
                    else {
                        if viewController.playerView.isHidden == false {
                            viewController.initUI()
                        }
                    }
                }
            }
        }
        */
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        //Commented this for test live audience on 12/6/20
//        self.badgeCount = 0
//        application.applicationIconBadgeNumber = self.badgeCount!
//        UserDefaults.standard.set(self.badgeCount, forKey: AppDelegate.key_badge)
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        print("terminated ")
        updateStatusAPI()
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func updateStatusAPI(){
                    DJBitzAPI.shared.updateLiveStatus( ) { (isSuccess, token, channelName) in
                                print(token)
                    }
                }
    
    
    func initSongData() {
        do {
            if self.player != nil {
                return
            }
            
            if self.shuffledList.count == 0 {
                return
            }
            
            if self.currentSongIndex > self.shuffledList.count - 1 {
                self.currentSongIndex = 0
            }
            
            if self.currentSongIndex < 0 {
                self.currentSongIndex = 0
            }
            
            self.currentMusic = self.shuffledList[self.currentSongIndex]
            
//            DJBitzAPI.shared.getMusicInfo(self.currentMusic!.id!) { (success, music, comments, error) in
//                //print("set music infomation: \(success)")
//
//                self.comments = comments
//                if self.commentCallback != nil {
//                    self.commentCallback!()
//                }
//            }
            
            isReady = false
            
            var playerItems = Array<AVPlayerItem>()
            
            for i in self.currentSongIndex..<self.shuffledList.count{
                var url: URL? = nil
                if islocal {
                    print(self.shuffledList[i].music!)
                    let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                    let mixPath = documentsURL.appendingPathComponent(String(format: "%@.mp3", self.shuffledList[i].name!))
                    if FileManager().fileExists(atPath: mixPath.path) {
                        print("Audio file exits")
                        url = mixPath
                    }
                } else {
                    url = URL(string: String(format: "%@%@", DJBitzAPI.mainURL, self.shuffledList[i].music!))
                }
                
                let playerItem = AVPlayerItem(url: url!)
                addObserverForRepeatSong(playerItem: playerItem)
                playerItems.append(playerItem)
            }
            
            self.player = AVQueuePlayer(items: playerItems)
            
            self.player?.actionAtItemEnd = self.isRepeat ? .pause : .advance
            
            let audioSession = AVAudioSession.sharedInstance()
            try audioSession.setCategory(.playback, mode: .default)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
            
            self.observer = self.player?.addPeriodicTimeObserver(forInterval: CMTimeMake(value: 1, timescale: 600), queue: DispatchQueue.main, using: { time in
                
                if self.player?.currentItem?.status == AVPlayerItem.Status.readyToPlay {
                    
                    self.isReady = true
                    self.isStopped = false
                    
                    if self.readyCallback != nil {
                        self.readyCallback!()
                    }
                    
                    self.player?.removeTimeObserver(AppDelegate.shared().observer as Any)
                    self.observer = nil
                    
                    self.observer = self.player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: Int32(NSEC_PER_SEC)), queue: nil) { time in
                        if self.callback != nil {
                            self.callback!(time)
                        }
                    }
                }
            })
            
            self.player?.play()
            
            if self.currentMusic != nil {
                self.setupLockScreen()
            }
            
        } catch {
            print(error)
        }
        
    }
    
    
    private func addObserverForRepeatSong(playerItem: AVPlayerItem) {
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: playerItem, queue: .main) { [weak self] _ in
            
            guard let s = self else {
                return
            }
            
            //print("s.isRepeat:\(s.isRepeat)")
            
            if s.isRepeat {
                s.player!.currentItem?.seek(to: CMTime.zero)
                s.updateNowPlayingInfo()
            }
            else{
                if s.currentSongIndex < s.shuffledList.count - 1 {
                    s.currentSongIndex += 1
                    s.currentMusic = s.shuffledList[s.currentSongIndex]
                    
                    let controller = CurrentMixViewController.sharedPlayer()
                    controller.music = s.currentMusic
                    //controller.currentSongIndex = s.currentSongIndex
                    if controller.view.superview != nil {
                        controller.prepareUI()
                    }
                    
                    if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                        if let viewController = navi.viewControllers.first as? ViewController {
                            viewController.updateTitleAndShowLoader()
                        }
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                        s.setupLockScreen()
                    })
                    
                    
                    // Update song info
//                    DJBitzAPI.shared.getMusicInfo(s.currentMusic!.id!) { (success, music, comments, error) in
//                        //print("set music infomation: \(success)")
//                        
//                        s.comments = comments
//                        if s.commentCallback != nil {
//                            s.commentCallback!()
//                        }
//                    }
                }
                
                
            }
            
            s.player?.play()
        }
    }
    
    
    func removePlayer() {
        if (self.player != nil) {
            self.player!.pause()
            if self.observer != nil {
                self.player?.removeTimeObserver(self.observer!)
                self.observer = nil
            }
            self.player = nil
            self.sliderValue = 0.0
        }
        
    }
    
    func playPrev() -> Bool {
        if self.currentSongIndex > 0 {
            self.currentSongIndex -= 1
            
            self.currentMusic = self.shuffledList[self.currentSongIndex]
            //self.player?.replaceCurrentItem(with: self.player?.items()[self.currentSongIndex])
            //self.player?.play()
            ////////////////////////////////
            if let player = self.player {
                player.pause()
                
                // Note: it is necessary to have seekToTime called twice in this method, once before and once after re-making the area. If it is not present before, the player will resume from the same spot in the next song when the previous song finishes playing; if it is not present after, the previous song will be played from the same spot that the current song was on.
                player.seek(to: CMTime.zero, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
                
                player.removeAllItems()
                for i in self.currentSongIndex..<self.shuffledList.count{
                    
                    var url: URL? = nil
                    if islocal {
                        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                        let mixPath = documentsURL.appendingPathComponent(String(format: "%@.mp3", self.shuffledList[i].name!))
                        if FileManager().fileExists(atPath: mixPath.path) {
                            print("Audio file exits")
                            url = mixPath
                        }
                    } else {
                        url = URL(string: String(format: "%@%@", DJBitzAPI.mainURL, self.shuffledList[i].music!))
                    }
                    let playerItem = AVPlayerItem(url: url!)
                    
                    addObserverForRepeatSong(playerItem: playerItem)
                    player.insert(playerItem, after: nil)
                }
                
                player.seek(to: CMTime.zero, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
                
                player.play()
                
            }
            
            return true
        }
        
        return false
    }
    
    func playNext() -> Bool{
        if self.currentSongIndex < self.shuffledList.count - 1 {
            
            self.currentSongIndex += 1
            
            self.currentMusic = self.shuffledList[self.currentSongIndex]
            
            self.player?.advanceToNextItem()
            
            return true
        }
        
        return false
    }
    
    func shuffle() {
        self.isShuffled = !self.isShuffled
        self.isRepeat = false
        UserDefaults.standard.setValue(self.isShuffled, forKey: AppDelegate.key_shuffle)
        UserDefaults.standard.setValue(self.isRepeat, forKey: AppDelegate.key_repeat)
        UserDefaults.standard.synchronize()
        
        self.getShuffledList()
        
        if self.isShuffled {
            if let player = self.player {
                
                for currentItem in player.items() {
                    if currentItem != player.currentItem {
                        player.remove(currentItem)
                    }
                }
                
                for music in self.shuffledList {
                    if !music.equalsTo(self.currentMusic!) {
                        var url: URL? = nil
                        if islocal {
                            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                            let mixPath = documentsURL.appendingPathComponent(String(format: "%@.mp3", music.name!))
                            if FileManager().fileExists(atPath: mixPath.path) {
                                print("Audio file exits")
                                url = mixPath
                            }
                        } else {
                            url = URL(string: String(format: "%@%@", DJBitzAPI.mainURL, music.music!))
                        }
                        let playerItem = AVPlayerItem(url: url!)
                        
                        if player.canInsert(playerItem, after: nil){
                            addObserverForRepeatSong(playerItem: playerItem)
                            player.insert(playerItem, after: nil)
                        }
                        else{
                            //print("ignore item")
                        }
                    }
                    else {
                        //print("ignore item")
                    }
                }
            }
        }
        else {
            if let player = self.player {
                
                for currentItem in player.items() {
                    if currentItem != player.currentItem {
                        player.remove(currentItem)
                    }
                }
                
                self.currentSongIndex = 0
                for i in 0..<self.shuffledList.count {
                    if self.currentMusic!.equalsTo(self.shuffledList[i]){
                        self.currentSongIndex = i
                        break
                    }
                }
                
                for i in self.currentSongIndex..<self.shuffledList.count {
                    let music = self.shuffledList[i]
                    if !music.equalsTo(self.currentMusic!) {
                        var url: URL? = nil
                        if islocal {
                            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                            let mixPath = documentsURL.appendingPathComponent(String(format: "%@.mp3", music.name!))
                            if FileManager().fileExists(atPath: mixPath.path) {
                                print("Audio file exits")
                                url = mixPath
                            }
                        } else {
                            url = URL(string: String(format: "%@%@", DJBitzAPI.mainURL, music.music!))
                        }
                        let playerItem = AVPlayerItem(url: url!)
                        
                        if player.canInsert(playerItem, after: nil){
                            addObserverForRepeatSong(playerItem: playerItem)
                            player.insert(playerItem, after: nil)
                        }
                        else{
                            //print("ignore item")
                        }
                    }
                    else {
                        //print("ignore item")
                    }
                }
            }
        }
    }
    
    func getShuffledList() {
        if let songList = self.musicsList?.musicRecords, songList.count > 0 {

            //print("BEFORE: \(self.currentSongIndex)")
            
            let currentSong = (self.shuffledList.count > self.currentSongIndex) ? self.shuffledList[self.currentSongIndex] : songList[self.currentSongIndex]

            //print("CURRENT SONG: \(currentSong.name)")
            
            if self.isShuffled {
                let tmpList = songList.shuffled()
                self.shuffledList.removeAll()
                self.shuffledList.append(currentSong)
                for music in tmpList {
                    if music.id != currentSong.id {
                        self.shuffledList.append(music)
                    }
                }
            }
            else {
                self.shuffledList = songList
            }
            
            
            
            self.currentSongIndex = self.shuffledList.firstIndex(where: { (music) -> Bool in
                return music.id == currentSong.id
            }) ?? 0
            
            //print("AFTER: \(self.currentSongIndex)")
        }
    }
    
    func setRepeat() {
        self.isRepeat = !self.isRepeat
        self.isShuffled = false
        UserDefaults.standard.setValue(self.isRepeat, forKey: AppDelegate.key_repeat)
        UserDefaults.standard.setValue(self.isShuffled, forKey: AppDelegate.key_shuffle)
        UserDefaults.standard.synchronize()
        
        self.player?.actionAtItemEnd = self.isRepeat ? .pause : .advance
        
    }
    
    func secondsToHoursMinutesSeconds (interval : Double) -> String {
        if interval.isNaN {
            return "00:00"
        }
        else {
            let interval = Int(interval)
            let seconds = interval % 60
            let minutes = (interval / 60) % 60
            let hours = (interval / 3600)
            
            return hours > 0 ? String(format: "%d:%02d:%02d", hours, minutes, seconds) : String(format: "%02d:%02d", minutes, seconds)
        }
        
    }
    
    func updateMusicsList(_ list: MusicsList) {
        self.musicsList = list
        self.getShuffledList()
    }
    
    
       func application(_ application: UIApplication,
                        didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        print("device token ----\(deviceToken)")
        let tokenParts = deviceToken.map { data -> String in
                   return String(format: "%02.2hhx", data)
               }
               let token = tokenParts.joined()
               print("device token ---\(token)")
        //   Messaging.messaging().apnsToken = deviceToken
       }
       
       
       func application(_ application: UIApplication,
                        didFailToRegisterForRemoteNotificationsWithError error: Error) {
       }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {

        print("APN recieved")
        print(userInfo)

        let state = application.applicationState
        
        if UserDefaults.standard.value(forKey: AppDelegate.key_badge) ==  nil {
            self.badgeCount = 1
        } else {
            self.badgeCount = UserDefaults.standard.value(forKey: AppDelegate.key_badge) as! Int + 1
        }
        
//          print("-----------Clicked --------------")
//        if let token = userInfo["gcm.notification.token"] as? String, token != ""{
//                   let roomName = userInfo["gcm.notification.channelName"] as? String ?? ""
//                 //  goToLive(roomName: roomName, token: token, isBroadcaster: false)
//
                 NotificationCenter.default.post(name: NSNotification.Name(rawValue: "liveStreamingNotification"), object: nil, userInfo: userInfo)
//               }
//
        UserDefaults.standard.set(self.badgeCount, forKey: AppDelegate.key_badge)
        
        switch state {

        case .inactive:
            print("Inactive")

        case .background:
            print("Background")
            if let token = userInfo["gcm.notification.token"] as? String, token != ""{

                        //NotificationCenter.default.post(name: Notification.Name("liveStreamingNotification"), object: nil, userInfo: userInfo)
                       
                   }
            // update badge count here
            application.applicationIconBadgeNumber = self.badgeCount!

        case .active:
            print("Active")

        }
        completionHandler(.noData)
    }
    
    
    //MARK:- Background Control
    func setupAudioSession(){
        UIApplication.shared.beginReceivingRemoteControlEvents()
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback)
            self.becomeFirstResponder()
            
            do {
                try AVAudioSession.sharedInstance().setActive(true)
                print("AVAudioSession is Active")
            } catch let error as NSError {
                print(error.localizedDescription)
                
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    func setupLockScreen(){
        let commandCenter = MPRemoteCommandCenter.shared()
        commandCenter.nextTrackCommand.isEnabled = true
        commandCenter.playCommand.addTarget { [unowned self] event in
            if self.player!.rate == 0.0 {
                self.player!.play()
                return .success
            }
            return .commandFailed
        }
        
        // Add handler for Pause Command
        commandCenter.pauseCommand.addTarget { [unowned self] event in
            if self.player!.rate == 1.0 {
                self.player!.pause()
                return .success
            }
            return .commandFailed
        }
        commandCenter.previousTrackCommand.removeTarget(self)
        commandCenter.previousTrackCommand.isEnabled = true
        commandCenter.previousTrackCommand.addTarget(self, action:#selector(bgControlPrev))
        commandCenter.nextTrackCommand.removeTarget(self)
        commandCenter.nextTrackCommand.addTarget(self, action:#selector(bgControlNext))
        
        var nowPlayingInfo = [String : Any]()
        nowPlayingInfo[MPMediaItemPropertyTitle] = self.currentMusic!.name!
        
        nowPlayingInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = self.player!.currentItem!.currentTime().seconds
        nowPlayingInfo[MPMediaItemPropertyPlaybackDuration] = self.player!.currentItem!.asset.duration.seconds
        nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackRate] = self.player!.rate
        
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
    }
    
    func updateNowPlayingInfo(){
        if var nowPlayingInfo = MPNowPlayingInfoCenter.default().nowPlayingInfo {
            nowPlayingInfo[MPMediaItemPropertyTitle] = self.currentMusic!.name!
            
            nowPlayingInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = self.player!.currentItem!.currentTime().seconds
            nowPlayingInfo[MPMediaItemPropertyPlaybackDuration] = self.player!.currentItem!.asset.duration.seconds
            nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackRate] = self.player!.rate
            
            MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
        }
        
    }
    
    @objc func bgControlPrev() -> MPRemoteCommandHandlerStatus {
        self.playPrev()
        if self.currentMusic != nil {
            updatePlayerUI()
            self.setupLockScreen()
        }
        return .success
    }
    @objc func bgControlNext() -> MPRemoteCommandHandlerStatus {
        self.playNext()
        if self.currentMusic != nil {
            updatePlayerUI()
            
            self.setupLockScreen()
        }
        return .success
    }
    
    func updatePlayerUI(){
        // Update player UI
        if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            if let viewController = navi.viewControllers.first as? ViewController {
                if let tabbarC = viewController.tabBarController as? TabBarViewController{
                if let currentNavi = tabbarC.selectedViewController as? UINavigationController {
                    if let currentMix = currentNavi.viewControllers.last as? CurrentMixViewController {
                        currentMix.music = self.currentMusic
                        currentMix.loadMusicInfo()
                        currentMix.tableView.reloadData()
                    }
                    else {
                        if viewController.playerView.isHidden == false {
                            viewController.initUI()
                        }
                    }
                }
            }
            }
        }
    }
}

//MARK:- UNUserNotificationCenterDelegate
// [START ios_10_message_handling]
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        //Messaging.messaging().appDidReceiveMessage(userInfo)
        /*
         // Print message ID.
         if let messageID = userInfo[gcmMessageIDKey] {
         print("Message ID: \(messageID)")
         }
         
         // Print full message.
         print(userInfo)
         */
        
       // displayRemoteNotification(userInfo)
        
        // Change this to your preferred presentation option
         completionHandler([.alert, .sound, .badge])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        // Print message ID.
        /*
         if let messageID = userInfo[gcmMessageIDKey] {
         print("Message ID: \(messageID)")
         }
         
         // Print full message.
         print(userInfo)
         */
        print("-----------Clicked ---11111-----------")
        if let token = userInfo["gcm.notification.token"] as? String, token != ""{
        // self.tabbarController.selectedIndex = 0
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
//            if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
//                if let viewController = navi.viewControllers.first as? ViewController {
//                    if let currentNavi = viewController.tabbarController.selectedViewController as? UINavigationController {
//                        if let currentMix = currentNavi.viewControllers.last as? LibraryViewController {
//                            currentMix.music = self.currentMusic
//                            currentMix.loadMusicInfo()
//                            currentMix.tableView.reloadData()
//                        }
//                        else {
//                            if viewController.playerView.isHidden == false {
//                                viewController.initUI()
//                            }
//                        }
//                    }
//                }
//            }
//
//            if  let conversationVC = storyboard.instantiateViewController(withIdentifier: "LiveViewController") as? LiveViewController,
//                   let tabBarController = self.window?.rootViewController as? UITabBarController,
//                   let navController = tabBarController.selectedViewController as? UINavigationController {
//
//                       // we can modify variable of the new view controller using notification data
//                       // (eg: title of notification)
//                     //  conversationVC.senderDisplayName = response.notification.request.content.title
//                       // you can access custom data of the push notification by using userInfo property
//                       // response.notification.request.content.userInfo
//                       navController.pushViewController(conversationVC, animated: true)
//               }

//
//
//                          let navC = storyboard.instantiateInitialViewController() as? UINavigationController
//                let rootViewC1 = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
//                      //    let rootViewC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as? TabBarViewController
//              navC?.viewControllers = [ rootViewC1!, rootViewC!]
//          //  rootViewC?.selectedIndex = 0
//                      self.window!.rootViewController = rootViewC1
//                      self.window!.makeKeyAndVisible()
//
            if ReferenceHelper.sharedInstance.loginInfo != nil, ReferenceHelper.sharedInstance.userInfo != nil {
                     
            if let token = userInfo["gcm.notification.token"] as? String, token != ""{
                                   let roomName = userInfo["gcm.notification.channelName"] as? String ?? ""
                                   let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                   let navC = storyboard.instantiateInitialViewController() as? UINavigationController
                                   let rootViewC1 = storyboard.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                                   let rootViewC2 = storyboard.instantiateViewController(withIdentifier: "LiveViewController") as? LiveViewController
                                   rootViewC2?.isFromNotification = true
                                   rootViewC2?.isBroadcaster = false
                                   rootViewC2?.strChannel = roomName
                                   rootViewC2?.strUsername = userInfo["gcm.notification.name"] as? String ?? ""
                                   let rootViewC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as? TabBarViewController
                                   rootViewC?.selectedIndex = 0
                                   navC?.viewControllers = [ rootViewC1!, rootViewC!, rootViewC2!]
                                   rootViewC?.selectedIndex = 0
                                   self.window!.rootViewController = navC
                                   self.window!.makeKeyAndVisible()
                               }
            }
            else{
               displayRemoteNotification(userInfo)
            }

            
        }
      //  displayRemoteNotification(userInfo)
        
        completionHandler()
    }
  
    // When app running in foreground, it displays alert when received notification
    private func displayRemoteNotification(_ userInfo: Dictionary<AnyHashable, Any>){
        if let apns = userInfo["aps"] as? Dictionary<String, Any?> {
            if let alert = apns["alert"] as? Dictionary<String, Any?> {
                let body = alert["body"] as? String
                let title = alert["title"] as? String
                
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: title, message: body, preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: {(action) in
                        // Do nothing
                    })
                    alertController.addAction(okAction)
                    
                    self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    private func displayRemoteNotification1(_ userInfo: Dictionary<AnyHashable, Any>){
           if let apns = userInfo["aps"] as? Dictionary<String, Any?> {
               if let alert = apns["alert"] as? Dictionary<String, Any?> {
                   let body = alert["body"] as? String
                   let title = alert["title"] as? String
                  let str =  userInfo["gcm.notification.token"] as? String
                   DispatchQueue.main.async {
                       let alertController = UIAlertController(title: str, message: body, preferredStyle: .alert)
                       let okAction = UIAlertAction(title: "Ok", style: .default, handler: {(action) in
                           // Do nothing
                       })
                       alertController.addAction(okAction)
                       
                       self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                   }
               }
           }
       }
}
// [END ios_10_message_handling]

//MARK:- MessagingDelegate
extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey: "firebase_token")
        if Messaging.messaging().fcmToken != nil {
            Messaging.messaging().subscribe(toTopic: "new-mix")
        }

        //let dataDict:[String: String] = ["token": fcmToken]
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}

