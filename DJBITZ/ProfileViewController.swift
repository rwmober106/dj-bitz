//
//  ProfileViewController.swift
//  DJBITZ
//
//  Created by MacLover on 8/24/20.
//  Copyright © 2020 BUGUNSOFT. All rights reserved.
//

import UIKit

class ProfileViewController: ActivityIndicatorViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet fileprivate weak var headerView: UIView!
    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var lblFullName: DJBitzTextField!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var btnDeleteAccount: UIButton!
    @IBOutlet var btnDjUser: UIButton!
    
    var viewcontroller: ViewController?
    var isDjUser = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imageTapped))

        imageView.addGestureRecognizer(tapGesture)
        imageView.isUserInteractionEnabled = true
        
        initUI()
        loadProfile()
        
    }
    
    @objc func imageTapped() {
        showAlert()
    }
    
    func initUI() {
        self.headerView.layer.shadowColor = UIColor.black.cgColor
        self.headerView.layer.shadowOpacity = 0.7
        self.headerView.layer.shadowOffset = CGSize(width: 0, height: 1)

        self.navigationController?.navigationBar.isHidden = true
        
        btnEdit.clipsToBounds = true
        btnEdit.layer.cornerRadius = btnEdit.layer.frame.height/2
        
        btnDeleteAccount.clipsToBounds = true
        btnDeleteAccount.layer.cornerRadius = btnEdit.layer.frame.height/2
        
        lblFullName.clipsToBounds = true
        lblFullName.layer.cornerRadius = 8
        
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = imageView.layer.frame.width/2
    }
    
    func loadProfile() {
        let fullName = ReferenceHelper.sharedInstance.userInfo?.username
        lblFullName.text = fullName
        if ReferenceHelper.sharedInstance.userInfo?.profile_avatar != nil {
            let profile_avatar = ReferenceHelper.sharedInstance.userInfo?.profile_avatar
            imageView.sd_setImage(with: URL(string: String(format: "%@%@", DJBitzAPI.mainURL, profile_avatar!)), completed: nil)
        } else {
            imageView.image = UIImage.init(named: "ic_avatar.png")
        }
        
        if ReferenceHelper.sharedInstance.userInfo?.isDjs == "1" {
            isDjUser = true            
        } else {
            isDjUser = false
        }
        btnDjUser.setImage(UIImage(named: isDjUser ? "ic_checkFilled" : "ic_uncheck"), for: .normal)
        
    }
    
    //Show alert to selected the media source type.
    private func showAlert() {

        let alert = UIAlertController(title: "Image Selection", message: "From where you want to pick this image?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .camera)
        }))
        alert.addAction(UIAlertAction(title: "Photo Album", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    //get image from source type
    private func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {

        //Check is source type available
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {

            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }
    
    func compressImage(image : UIImage) -> UIImage
    {
        var _actualImageHeight : CGFloat = image.size.height
        var _actualImageWidth : CGFloat = image.size.width
        let _maxHeight : CGFloat = 207.0
        let _maxWidth : CGFloat = 448.0
        var _imageRatio : CGFloat = _actualImageWidth / _actualImageHeight
        let _maxRatio: CGFloat = _maxWidth / _maxHeight
        let _imageCompressionQuality : CGFloat = 0.5 // makes the image get compressed to 50% of its actual size

        if _actualImageHeight > _maxHeight || _actualImageWidth > _maxWidth
        {
            if _imageRatio < _maxRatio
            {
                // Adjust thw width according to the _maxHeight
                _imageRatio = _maxHeight / _actualImageHeight
                _actualImageWidth = _imageRatio * _actualImageWidth
                _actualImageHeight = _maxHeight
            }
            else
            {
                if _imageRatio > _maxRatio
                {
                    // Adjust height according to _maxWidth
                    _imageRatio = _maxWidth / _actualImageWidth
                    _actualImageHeight = _imageRatio * _actualImageHeight
                    _actualImageWidth = _maxWidth
                }
                else
                {
                    _actualImageHeight = _maxHeight
                    _actualImageWidth = _maxWidth
                }

            }
        }
        let _compressedImage : CGRect = CGRect(x: 0.0 , y: 0.0 , width: _actualImageWidth , height: _actualImageHeight)
        UIGraphicsBeginImageContext(_compressedImage.size)
        image.draw(in: _compressedImage)
        let img: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        let imageData: NSData = img.jpegData(compressionQuality: _imageCompressionQuality)! as NSData
        UIGraphicsEndImageContext()
        return UIImage(data: imageData as Data)!
    }
    
    func showAlert(_ title:String, _ message:String) {
        let alert = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel,handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onButtonBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onButtonDJ(_ sender: Any) {
        isDjUser = !isDjUser
        btnDjUser.setImage(UIImage(named: isDjUser ? "ic_checkFilled" : "ic_uncheck"), for: .normal)
    }
    @IBAction func onButtonEdit(_ sender: Any) {
        
        let email = ReferenceHelper.sharedInstance.loginInfo![ReferenceHelper.key_login_email]
        let username = lblFullName.text
        if imageView.image == nil {
            imageView.image = UIImage.init(named: "ic_avatar.png")
        }
        let imageData = imageView.image!.jpegData(compressionQuality: 1.0)
        let base64encodeImage = imageData?.base64EncodedString(options: .lineLength64Characters)
        
        self.activeActivity(true)
        DJBitzAPI.shared.editProfile(isDjUser ? "1" : "2", email!, username!, base64encodeImage!) { (success, userInfo, message) in
            self.activeActivity(false)
            
            if success {
                ReferenceHelper.sharedInstance.userInfo = userInfo
            }
            else {
                self.showAlert("FAILED", message != nil ? message!:"Unknown")
            }
            
        }
        
    }
    @IBAction func onButtonDeleteAccount(_ sender: Any) {
        let email = ReferenceHelper.sharedInstance.loginInfo![ReferenceHelper.key_login_email]
        self.activeActivity(true)
        DJBitzAPI.shared.deleteAccount(email!) { (success, message) in
            self.activeActivity(false)
            if success {
                ReferenceHelper.sharedInstance.loginInfo = nil
                ReferenceHelper.sharedInstance.userInfo = nil
                UserDefaults.standard.removeObject(forKey: "firebase_token")
                if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                    if let viewController = navi.viewControllers.first as? ViewController {
                        if let loginController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
                            loginController.modalPresentationStyle = .fullScreen
                            viewController.present(loginController, animated: true, completion: nil)

                            if viewController.tabbar == nil{
                                return
                            }

                            viewController.tabbar.selectedItem = viewController.tabbar.items?.first
                            viewController.tabbarController.selectedIndex = 0
                            if AppDelegate.shared().player != nil {
                                viewController.onButtonClose(UIButton())
                            }
                        }
                    }
                }
            } else {
                self.showAlert("FAILED", message != nil ? message!:"Unknown")
            }

        }
    }
    
    //MARK:- UIImagePickerViewDelegate.
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        self.dismiss(animated: true) { [weak self] in

            guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
            //Setting image to your image view
            let resizedImage = self!.compressImage(image: image)
            self?.imageView.image = resizedImage
        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}
