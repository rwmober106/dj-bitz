//
//  LocalMix.swift
//  DJBITZ
//
//  Created by MacLover on 10/13/20.
//  Copyright © 2020 BUGUNSOFT. All rights reserved.
//

import UIKit
import Foundation

class LocalMix: NSObject, NSCoding {
    
    var name: String
    var dj: String
    var duration: String
    // Initializer
    init(name: String, dj: String, duration: String) {
        self.name = name
        self.dj = dj
        self.duration = duration
    }
    
    // MARK: NSCoding
    required convenience init?(coder decoder: NSCoder) {
        guard let name = decoder.decodeObject(forKey: "name") as? String,
            let dj = decoder.decodeObject(forKey: "dj") as? String,
            let duration = decoder.decodeObject(forKey: "duration") as? String
            else { return nil }
        self.init(name: name, dj: dj, duration: duration)
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.name, forKey: "name")
        coder.encode(self.dj, forKey: "dj")
        coder.encode(self.duration, forKey: "duration")
    }
    
}
