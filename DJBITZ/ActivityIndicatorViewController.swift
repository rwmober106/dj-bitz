//
//  ActivityIndicatorViewController.swift
//  CANNA-Mobile
//
//  Created by Le Van Thang on 12/28/18.
//  Copyright © 2018 BUGUNSOFT. All rights reserved.
//

import UIKit

class ActivityIndicatorViewController: UIViewController {
    var progressView: UIView!
    var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create activity indicator on the top of navigation controller
        self.progressView = UIView()
        self.progressView.backgroundColor = UIColor.init(white: 0.0, alpha: 0.4)
        
        self.activityIndicator = UIActivityIndicatorView()
        self.activityIndicator.style = .whiteLarge
        self.activityIndicator.startAnimating()
        
        self.progressView.addSubview(self.activityIndicator)
        
        var targetView: UIView?
        if let controller = self.navigationController {
            targetView = controller.view
        }
        else{
            targetView = self.view
        }
        
        
        if let targetView = targetView {
        targetView.addSubview(self.progressView)
        self.progressView.isHidden = true
            
        /*
        let widthConstraint = NSLayoutConstraint(item: self.progressView, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: targetView, attribute: NSLayoutConstraint.Attribute.width, multiplier: 1.0, constant: 0.0)
        let heightConstraint = NSLayoutConstraint(item: self.progressView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: targetView, attribute: NSLayoutConstraint.Attribute.height, multiplier: 1.0, constant: 0.0)
        let centerXConstraint = NSLayoutConstraint(item: self.progressView, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: targetView, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.0, constant: 0.0)
        let centerYConstraint = NSLayoutConstraint(item: self.progressView, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: targetView, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: 0.0)
        
        targetView.addConstraints([widthConstraint, heightConstraint, centerXConstraint, centerYConstraint])
        */
            
        }
    }
    
    public func activeActivity(_ show: Bool){
        if self.progressView != nil{
           self.progressView.isHidden = !show
        }
       
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.progressView.frame = self.view.bounds
        self.activityIndicator.center  = self.view.center
    }
}
