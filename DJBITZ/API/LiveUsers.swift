//
//  LiveUsers.swift
//  DJBITZ
//
//  Created by Rakinder on 27/06/20.
//  Copyright © 2020 BUGUNSOFT. All rights reserved.
//

import UIKit
import ObjectMapper

open class LiveUsers: Mappable {
    
    public var id: String?
    public var email: String?
    public var isLive: String?
    public var isDjs: String?
    public var live_token: String?
    public var channelName: String?
    public var username: String?
    public var profile_avatar: String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        id <- map["id"]
        email <- map["email"]
        isLive <- map["isLive"]
        isDjs <- map["isDjs"]
        live_token <- map["live_token"]
        channelName <- map["channelName"]
        username <- map["username"]
        profile_avatar <- map["profile_avatar"]
    }
}
