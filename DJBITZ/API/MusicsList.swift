//
//  MusicsList.swift
//  DJBITZ
//
//  Created by dung on 3/4/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import Foundation
import ObjectMapper

open class MusicsList: Mappable {
    
    public var searchText: Bool?
    public var musicRecords: [Music]?

    public init?(_ musicRecords: [Music], _ searchText:Bool? = false) {
        self.searchText = searchText
        self.musicRecords = musicRecords
    }
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        searchText <- map["searchText"]
        musicRecords <- map["musicRecords"]
    }
}
