//
//  ArtistInfomation.swift
//  DJBITZ
//
//  Created by dung on 3/4/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import Foundation
import ObjectMapper

open class ArtistInfomation: Mappable {
    
    public var id: String?
    public var name: String?
    public var avatar_url: String?
    public var email: String?
    public var mobile: String?
    public var profile_cover: String?
    public var description: String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        avatar_url <- map["avatar_url"]
        profile_cover <- map["profile_cover"]
        email <- map["email"]
        mobile <- map["mobile"]
        description <- map["description"]
        
        if avatar_url!.count > 0 {
            avatar_url = avatar_url!.replacingOccurrences(of: "\\", with: "")
        }
    }
}
