//
//  Music.swift
//  DJBITZ
//
//  Created by dung on 3/4/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import Foundation
import ObjectMapper

open class Music: NSObject, Mappable, NSCoding {
    
    public var id: String?
    public var name: String?
    public var desc: String?
    public var thumb: String?
    public var music: String?
    public var DJ: String?
    public var dj: String?
    public var genre: String?
    public var artist: String?
    public var createdBy: String?
    public var updatedBy: String?
    public var isDeleted: String?
    public var created_date: String?
    public var updated_date: String?
    public var duration: String?
    public var likes: String?
    public var is_liked: String?
    public var playCounts: String?
    public var comment_count: String?
    public var is_playlist: String?

    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        desc <- map["description"]
        thumb <- map["thumb"]
        music <- map["music"]
        DJ <- map["DJ"]
        dj <- map["dj"]
        genre <- map["genre"]
        artist <- map["artist"]
        createdBy <- map["createdBy"]
        updatedBy <- map["updatedBy"]
        isDeleted <- map["isDeleted"]
        created_date <- map["created_date"]
        updated_date <- map["updated_date"]
        duration <- map["duration"]
        likes <- map["likes"]
        is_liked <- map["is_liked"]
        playCounts <- map["playCounts"]
        comment_count <- map["comment_count"]
        is_playlist <- map["is_playlist"]

        if thumb!.count > 0 {
            thumb = thumb!.replacingOccurrences(of: "\\", with: "")
        }

        if music!.count > 0 {
            music = music!.replacingOccurrences(of: "\\", with: "")
        }
    }
    
    public func equalsTo(_ another: Music) -> Bool {
        if self.id != another.id {
            return false
        }

        return true
        
    }
    
    init(id: String, name: String, description: String, thumb: String, music: String, DJ: String, genre: String, created_date: String, duration: String, likes: String, is_liked: String, playCounts: String, comment_count: String) {
        self.id = id
        self.name = name
        self.desc = description
        self.thumb = thumb
        self.music = music
        self.DJ = DJ
//        self.dj = dj
        self.genre = genre
//        self.artist = artist
//        self.createdBy = createdBy
//        self.updatedBy = updatedBy
//        self.isDeleted = isDeleted
        self.is_liked = is_liked
        self.created_date = created_date
//        self.updated_date = updated_date
        self.likes = likes
        self.playCounts = playCounts
        self.comment_count = comment_count
        self.duration = duration
    }

    // MARK: NSCoding
    required convenience public init?(coder decoder: NSCoder) {
        guard
            let id = decoder.decodeObject(forKey: "id") as? String,
            let name = decoder.decodeObject(forKey: "name") as? String,
            let description = decoder.decodeObject(forKey: "description") as? String,

            let thumb = decoder.decodeObject(forKey: "thumb") as? String,
            let music = decoder.decodeObject(forKey: "music") as? String,
//            let dj = decoder.decodeObject(forKey: "dj") as? String,
            let DJ = decoder.decodeObject(forKey: "DJ") as? String,
            let genre = decoder.decodeObject(forKey: "genre") as? String,
//            let artist = decoder.decodeObject(forKey: "artist") as? String,
//            let createdBy = decoder.decodeObject(forKey: "createdBy") as? String,
//            let updatedBy = decoder.decodeObject(forKey: "updatedBy") as? String,
//            let isDeleted = decoder.decodeObject(forKey: "isDeleted") as? String,
            let created_date = decoder.decodeObject(forKey: "created_date") as? String,
//            let updated_date = decoder.decodeObject(forKey: "updated_date") as? String,
            let likes = decoder.decodeObject(forKey: "likes") as? String,
            let is_liked = decoder.decodeObject(forKey: "is_liked") as? String,
            let playCounts = decoder.decodeObject(forKey: "playCounts") as? String,
            let comment_count = decoder.decodeObject(forKey: "comment_count") as? String,
            let duration = decoder.decodeObject(forKey: "duration") as? String
            else { return nil }
        self.init(id: id, name: name, description: description, thumb: thumb, music: music, DJ: DJ, genre: genre, created_date: created_date, duration: duration, likes: likes, is_liked: is_liked, playCounts: playCounts, comment_count: comment_count)
    }

    public func encode(with coder: NSCoder) {
        coder.encode(self.id, forKey: "id")
        coder.encode(self.name, forKey: "name")
        coder.encode(self.duration, forKey: "duration")
        coder.encode(self.desc, forKey: "description")
        coder.encode(self.thumb, forKey: "thumb")
        coder.encode(self.music, forKey: "music")
        coder.encode(self.DJ, forKey: "DJ")
//        coder.encode(self.dj, forKey: "dj")
        coder.encode(self.genre, forKey: "genre")
//        coder.encode(self.artist, forKey: "artist")
//        coder.encode(self.createdBy, forKey: "createdBy")
//        coder.encode(self.updatedBy, forKey: "updatedBy")
//        coder.encode(self.isDeleted, forKey: "isDeleted")
        coder.encode(self.created_date, forKey: "created_date")
//        coder.encode(self.updated_date, forKey: "updated_date")
        coder.encode(self.duration, forKey: "duration")
        coder.encode(self.likes, forKey: "likes")
        coder.encode(self.is_liked, forKey: "is_liked")
        coder.encode(self.playCounts, forKey: "playCounts")
        coder.encode(self.comment_count, forKey: "comment_count")
    }
}
