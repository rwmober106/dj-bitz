//
//  DJBitzAPI.swift
//  DJBITZ
//
//  Created by dung on 3/4/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

public class DJBitzAPI {
    public static let shared = DJBitzAPI()
    public static let SUCCESS = 200
    //test url - http://ossdemo.website/dj_bitz/
    //Live url - http://18.235.241.120/
    public static let mainURL = "http://18.235.241.120/"
    public static let mainImageURL = "http://18.235.241.120/"
    
    
    // Blaze consumer user info
    public var consumerUserInfo: [String:Any?]? = nil
    
    /// The shared SessionManager instance for Alamofire.
    let alamofireManager: SessionManager = {
        
        // Build the session manager with the configured session.
        let manager = Alamofire.SessionManager(configuration: .default)
        
        return manager
    }()
    
    func getList(_ url:String, _ id:[String]? = nil, _ callback: @escaping (Bool, MusicsList?, String?) -> Void){
        if let url = URL(string: url) {
            do {
                // Build parameters required for getting token
                var parameters:[String:String] = ["uid":ReferenceHelper.sharedInstance.userInfo!.id!]
                if id != nil {
                    parameters[id![0]] = id![1]
                }
                
                // Perform request
                alamofireManager.request(url, method: .post, parameters: parameters)
                    .responseJSON { jsonResponse in
                        print(jsonResponse)
                        // Confirm response data is in a JSON Array format.
                        guard let json = jsonResponse.value as? [String : Any] else {
                            callback(false, nil, "JSON error: could not parse into dictionary.")
                            return
                        }
                        
                        guard !json.isEmpty else {
                            callback(false, nil, "No data was returned, no objects were found!")
                            return
                        }
                        
                        guard let results = json["result"] as? [String:Any] else {
                            callback(false, nil, "No data was returned, no objects were found!")
                            return
                        }
                        
                        // Map JSON Array to native type using Mappable protocol.
                        guard let object = Mapper<MusicsList>().map(JSON: results) else {
                            callback(false, nil, "No objects found!")
                            return
                        }
                        
                        // Complete with asynchronous callback                        
                        callback(true, object, nil)
                }
            }
            catch {
                
            }
        }
    }
    
    public func getMusicsList(byDJ id:String?, _ callback: @escaping (Bool, MusicsList?, String?) -> Void){
        let urlString = String(format: "%@index.php/getmusicswithdj", DJBitzAPI.mainURL)
        
        var info:[String]? = nil
        if id != nil {
            info = ["djId", id!]
        }
        
        self.getList(urlString, info) { (success, list, error) in
            callback(success, list, error)
        }
    }
    
    public func updateToken( _ callback: @escaping (Bool, String?) -> Void){
        let urlString = String(format: "%@authenticate.php/registerToken", DJBitzAPI.mainURL)
        if let url = URL(string: urlString) {
            do {
                
                // Build parameters required for getting token
                let token = UserDefaults.standard.object(forKey: "firebase_token") as? String ?? ""
                let parameters:[String:String] = ["token": token]

                // Perform request
                alamofireManager.request(url, method: .post, parameters: parameters)
                    .responseJSON { jsonResponse in
                     print(jsonResponse.value)
                        // Complete with asynchronous callback
                        callback(true, nil)
                }
            }
            catch {
                
            }
        }

    }
    
    //MARK: - Live Api
    public func getLiveStreamingToken(channel: String,id: String, _ callback: @escaping (Bool, LiveInfo?, String?) -> Void){
        print(id)
           let urlString = String(format: "%@GenerateToken/generate_token", DJBitzAPI.mainURL)
                 if let url = URL(string: urlString) {
               do {
                   
                   // Build parameters required for getting token
                   let parameters:[String:String] = ["userId": id,
                                                     "userName": ReferenceHelper.sharedInstance.userInfo?.username ?? "",
                                                     "channelName": channel]

                print(parameters)
                   // Perform request
                   alamofireManager.request(url, method: .post, parameters: parameters)
                       .responseJSON { jsonResponse in
                        print(jsonResponse.value)
                        
                           // Confirm response data is in a JSON Array format.
                           guard let json = jsonResponse.value as? [String : Any] else {
                               callback(false, nil, "JSON error: could not parse into dictionary.")
                               return
                           }
                           
                           guard !json.isEmpty else {
                               callback(false, nil, "No data was returned, no objects were found!")
                               return
                           }
                           
                           guard let results = json["AllData"] as? [String:Any] else {
                               callback(false, nil, "No data was returned, no objects were found!")
                               return
                           }
                           guard let object = Mapper<LiveInfo>().map(JSON: results) else {
                                                     callback(false, nil, "No objects found!")
                                                     return
                                                 }
                        
                           // Complete with asynchronous callback
                           callback(true, object, nil)
                   }
               }
               catch {
                   
               }
           }
       }
    
    public func updateLiveStatus( _ callback: @escaping (Bool, LiveInfo?, String?) -> Void){
              let urlString = String(format: "%@GenerateToken/updateLiveStatus", DJBitzAPI.mainURL)
                    if let url = URL(string: urlString) {
                  do {
                      
                      // Build parameters required for getting token
                      let parameters:[String:String] = ["liveStatus": "0",
                                                        "userId": "\(Helpers.sharedInstance.getFromDefaults(strKey: Consatnt.User_Defaults.kUserID))"]

                      // Perform request
                      alamofireManager.request(url, method: .post, parameters: parameters)
                          .responseJSON { jsonResponse in
                           print(jsonResponse.value)
                           
                              // Confirm response data is in a JSON Array format.
                              guard let json = jsonResponse.value as? [String : Any] else {
                                  callback(false, nil, "JSON error: could not parse into dictionary.")
                                  return
                              }
                              
                              guard !json.isEmpty else {
                                  callback(false, nil, "No data was returned, no objects were found!")
                                  return
                              }
                              
                              guard let results = json["AllData"] as? [String:Any] else {
                                  callback(false, nil, "No data was returned, no objects were found!")
                                  return
                              }
                              guard let object = Mapper<LiveInfo>().map(JSON: results) else {
                                                        callback(false, nil, "No objects found!")
                                                        return
                                                    }
                           
                              // Complete with asynchronous callback
                              callback(true, object, nil)
                      }
                  }
                  catch {
                      
                  }
              }
          }
    
    public func getAllLiveCustomers( _ callback: @escaping (Bool, [LiveUsers], String?) -> Void){
              let urlString = String(format: "%@GenerateToken/getAllLiveCustomers", DJBitzAPI.mainURL)
        let parameters:[String:String] = [ "userId": "\(Helpers.sharedInstance.getFromDefaults(strKey: Consatnt.User_Defaults.kUserID))"]
                    if let url = URL(string: urlString) {
                  do {
                      
                      // Build parameters required for getting token
                      // Perform request
                      alamofireManager.request(url, method: .post, parameters: parameters)
                          .responseJSON { jsonResponse in
                           print(jsonResponse.value)
                           
                              // Confirm response data is in a JSON Array format.
                              guard let json = jsonResponse.value as? [String : Any] else {
                                  callback(false, [], "JSON error: could not parse into dictionary.")
                                  return
                              }
                              
                              guard !json.isEmpty else {
                                  callback(false, [], "No data was returned, no objects were found!")
                                  return
                              }
                              
                              guard let results = json["customersList"] as? NSArray else {
                                  callback(false, [], "No data was returned, no objects were found!")
                                  return
                              }
                            
                            var liveUsers:[LiveUsers] = []
                                  if let cm = json["customersList"] as? [[String:Any]] {
                                      liveUsers = Mapper<LiveUsers>().mapArray(JSONArray: cm)
                                  }
                           
                              // Complete with asynchronous callback
                              callback(true, liveUsers, nil)
                      }
                  }
                  catch {
                      
                  }
              }
          }

    public func getMusicsList(byGenre id:String?, _ callback: @escaping (Bool, MusicsList?, String?) -> Void){
        let urlString = String(format: "%@index.php/getmusicswithgenre", DJBitzAPI.mainURL)
        
        var info:[String]? = nil
        if id != nil {
            info = ["genreId", id!]
        }

        self.getList(urlString, info) { (success, list, error) in
            callback(success, list, error)
        }
    }
    
    public func getTopMusicList(_ callback: @escaping (Bool, [Music]?, String?) -> Void){
        let urlString = String(format: "%@index.php/gettopmusiclist", DJBitzAPI.mainURL)
        if let url = URL(string: urlString) {
            do {
                
                // Build parameters required for getting token
                let parameters:[String:String] = ["uid":ReferenceHelper.sharedInstance.userInfo!.id!]

                // Perform request
                alamofireManager.request(url, method: .post, parameters: parameters)
                    .responseJSON { jsonResponse in
                        
                        // Confirm response data is in a JSON Array format.
                        guard let json = jsonResponse.value as? [String : Any] else {
                            callback(false, nil, "JSON error: could not parse into dictionary.")
                            return
                        }
                        
                        guard !json.isEmpty else {
                            callback(false, nil, "No data was returned, no objects were found!")
                            return
                        }
                        
                        guard let results = json["result"] as? [[String:Any]] else {
                            callback(false, nil, "No data was returned, no objects were found!")
                            return
                        }
                        
                        // Map JSON Array to native type using Mappable protocol.
                        let objects = Mapper<Music>().mapArray(JSONArray: results)
                        
                        // Complete with asynchronous callback
                        callback(true, objects, nil)
                }
            }
            catch {
                
            }
        }
    }

    public func getMusicsList(_ callback: @escaping (Bool, MusicsList?, String?) -> Void){
        
        let urlString = String(format: "%@index.php/getmusiclist", DJBitzAPI.mainURL)
        self.getList(urlString) { (success, list, error) in
            callback(success, list, error)
        }
    }
    
    public func getDJsList(_ callback: @escaping (Bool, ArtistsList?, String?) -> Void){
        
        if let url = URL(string: String(format: "%@index.php/getdjslist", DJBitzAPI.mainURL)) {
            do {
                let request = try URLRequest(url: url, method: .get)
                
                // Perform request
                alamofireManager.request(request)
                    .responseJSON { jsonResponse in
                        
                        // Confirm response data is in a JSON Array format.
                        guard let json = jsonResponse.value as? [String : Any] else {
                            callback(false, nil, "JSON error: could not parse into dictionary.")
                            return
                        }
                        
                        guard !json.isEmpty else {
                            callback(false, nil, "No data was returned, no objects were found!")
                            return
                        }
                        
                        guard let results = json["result"] as? [String:Any] else {
                            callback(false, nil, "No data was returned, no objects were found!")
                            return
                        }
                        
                        // Map JSON Array to native type using Mappable protocol.
                        guard let object = Mapper<ArtistsList>().map(JSON: results) else {
                            callback(false, nil, "No objects found!")
                            return
                        }
                        
                        // Complete with asynchronous callback
                        AppDelegate.shared().artistsList = object
                        
                        callback(true, object, nil)
                }
            }
            catch {
                
            }
        }
    }

    public func getGenresList(_ callback: @escaping (Bool, GenresList?, String?) -> Void){
        
        if let url = URL(string: String(format:"%@index.php/getgenreslist", DJBitzAPI.mainURL)) {
            do {
                let request = try URLRequest(url: url, method: .get)
                
                // Perform request
                alamofireManager.request(request)
                    .responseJSON { jsonResponse in
                        
                        // Confirm response data is in a JSON Array format.
                        guard let json = jsonResponse.value as? [String : Any] else {
                            callback(false, nil, "JSON error: could not parse into dictionary.")
                            return
                        }
                        
                        guard !json.isEmpty else {
                            callback(false, nil, "No data was returned, no objects were found!")
                            return
                        }
                        
                        guard let results = json["result"] as? [String:Any] else {
                            callback(false, nil, "No data was returned, no objects were found!")
                            return
                        }
                        
                        // Map JSON Array to native type using Mappable protocol.
                        guard let object = Mapper<GenresList>().map(JSON: results) else {
                            callback(false, nil, "No objects found!")
                            return
                        }
                        
                        // Complete with asynchronous callback
                        AppDelegate.shared().genresList = object

                        callback(true, object, nil)
                }
            }
            catch {
                
            }
        }
    }
    
    public func getMusicInfo(_ musicId:String, _ callback: @escaping (Bool, Music?, [Comment], String?) -> Void){
        let urlString = String(format:"%@index.php/getmusicinfo", DJBitzAPI.mainURL)
        if let url = URL(string: urlString) {
            do {
                // Build parameters required for getting token
                let parameters:[String:String] = ["uid":ReferenceHelper.sharedInstance.userInfo!.id!,
                                                  "mid":musicId]
                
                // Perform request
                alamofireManager.request(url, method: .post, parameters: parameters)
                    .responseJSON { jsonResponse in
                        
                        // Confirm response data is in a JSON Array format.
                        guard let json = jsonResponse.value as? [String : Any] else {
                            callback(false, nil, [], "JSON error: could not parse into dictionary.")
                            return
                        }
                        
                        guard !json.isEmpty else {
                            callback(false, nil, [], "No data was returned, no objects were found!")
                            return
                        }
                        
                        guard let results = json["result"] as? [[String:Any]] else {
                            callback(false, nil, [], "No data was returned, no objects were found!")
                            return
                        }
                        
                        if (results.count == 0){
                            callback(false, nil, [], "No data was returned, no objects were found!")
                            return
                        }
                        
                        var comments:[Comment] = []
                        if let cm = json["comments"] as? [[String:Any]] {
                            comments = Mapper<Comment>().mapArray(JSONArray: cm)
                        }

                        
                        
                        // Map JSON Array to native type using Mappable protocol.
                        guard let object = Mapper<Music>().map(JSON: results.first!) else {
                            callback(false, nil, [], "No objects found!")
                            return
                        }
                        
                        if let isLiked = json["is_liked"] as? Bool {
                            object.is_liked = isLiked ? "1":"0"
                        }

                        if let commentCount = json["comment_count"] as? Int {
                            object.comment_count = String(format: "%d", commentCount)
                        }

                        if let play_count = json["play_count"] as? [[String:String]], play_count.count > 0 {
                            object.playCounts = play_count.first!["count"]!
                        }
                        
                        // Complete with asynchronous callback
                        callback(true, object, comments, nil)
                }
            }
            catch {
                
            }
        }
    }

    public func DJSendrequest(_ email:String, _ firstName:String, _ lastName:String, _ DJPref:String, _ date:String, _ event:String, _ note:String, _ callback: @escaping (Bool) -> Void){
        
        // Perform request
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(email.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "sender_email")
            multipartFormData.append(firstName.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "first_name")
            multipartFormData.append(lastName.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "last_name")
            multipartFormData.append(DJPref.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "dj_pref")
            multipartFormData.append(date.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "event_date")
            multipartFormData.append(event.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "event_name")
            multipartFormData.append(note.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "note")
        }, to: String(format:"%@index.php/sendrequest", DJBitzAPI.mainURL), encodingCompletion: { result in
            print("result  = \(result)")
            switch result {
            case .success(request: _, streamingFromDisk: false, streamFileURL: nil):
                //Success code
                
                callback(true)

                break
                
            case .failure:
                callback(false)
                break
                
            case .success(let request, true, let streamFileURL):
                callback(true)

            case .success(let request, let streamingFromDisk, .some(_)):
                callback(true)

            }
        })
    }

    
    //MARK:- ACCOUNT API
    func register(isDJ: String ,userName: String, email:String, password:String,
                  then callback: @escaping (Bool, String?) -> Void) {
        
        // Build Request URL to get token
        guard let requestURL = URL(string: String(format: "%@index.php/register", DJBitzAPI.mainURL)) else {
            callback(false, "URL error")
            return
        }
        
        // Build parameters required for getting token
        let parameters = ["username":userName,
                          "email":email,
                          "password":password,
            "isDjs": isDJ]

        // Make request
        alamofireManager.request(requestURL,
                                 method: .post,
                                 parameters: parameters)
            // Handle reponse
            .responseJSON { responseJSON in
                
                guard
                    // Confirm HTTP response is success
                    responseJSON.result.isSuccess,
                    
                    // Confirm response body value is in JSON format
                    let json = responseJSON.result.value as? [String:Any]
                    else {
                        // Handle error
                        if let json = responseJSON.result.value as? [String:Any] {
                            if let msg = json["msg"] as? String {
                                callback(false, msg)
                                return
                            }
                            
                            callback(false, "Failed to register")
                            return
                        }
                        callback(false, "Unknown Error")
                        return
                }
                
                // Map JSON Array to native type using Mappable protocol.

                if let status = json["status"] as? String {
                    let msg = json["msg"] as? String
                    
                    if status == "failed" {
                        callback(false, msg)
                    }
                    else {
                        callback(true, msg)
                    }
                    return
                }
        }
    }
    
    func login(email: String,
               password: String,
               then callback: @escaping (Bool, UserInfo?, String?) -> Void) {
        
        // Build Request URL to get token
        guard let requestURL = URL(string: String(format: "%@index.php/login", DJBitzAPI.mainURL)) else {
            callback(false, nil, "URL error")
            return
        }
        let token = UserDefaults.standard.object(forKey: "firebase_token") as? String ?? ""
        print("token -----------------\(token)")
        // Build parameters required for getting token
        let parameters = ["email":email,
                          "password":password,
                          "FCM_token": token]

//        // Build headers required
        let headers: HTTPHeaders = ["Content-Type": "application/x-www-form-urlencoded"]
        
        // Make request
        alamofireManager.request(requestURL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers)
            // Handle reponse
            .responseJSON { responseJSON in
                print("Login response ")
                print(responseJSON)
                guard
                    // Confirm HTTP response is success
                    responseJSON.result.isSuccess,
                    
                    // Confirm response body value is in JSON format
                    let json = responseJSON.result.value as? [String:Any]
                    else {
                        // Handle error
                        if let json = responseJSON.result.value as? [String:Any] {
                            if let msg = json["msg"] as? String {
                                callback(false, nil, msg)
                                return
                            }
                            
                            callback(false, nil, "Failed to login")
                            return
                        }
                        callback(false, nil, "Unknown Error")
                        return
                }

                if let status = json["status"] as? String {
                    let msg = json["msg"] as? String
                    
                    if status == "failed" {
                        callback(false, nil, msg)
                    }
                    else {
                        if let userInfo = json["userInfo"] as? [String:Any] {
                            // Map JSON Array to native type using Mappable protocol.
                            guard let object = Mapper<UserInfo>().map(JSON: userInfo) else {
                                callback(false, nil, "No objects found!")
                                return
                            }
                            
                            // Save token to UserDefaults
                            //                ReferenceHelper.sharedInstance.token = [ReferenceHelper.LoginType.Devergo.rawValue, object.access_token!]
                            //                ReferenceHelper.sharedInstance.loginInfo = [ReferenceHelper.key_login_email:email, ReferenceHelper.key_login_password:password]
                            callback(true, object, msg)
                        }
                        else {
                            callback(false, nil, "Failed to get user info")
                        }
                        
                    }
                    return
                }
        }
    }

    func resetPassword(email: String, password: String,
               then callback: @escaping (Bool, String?) -> Void) {
        
        // Build Request URL to get token
        guard let requestURL = URL(string: String(format: "%@index.php/resetpassword", DJBitzAPI.mainURL)) else {
            callback(false, "URL error")
            return
        }
        
        // Build parameters required for getting token
        let parameters = ["registered_email":email,
                          "password":password]
        
        // Build headers required
        let headers: HTTPHeaders = ["Content-Type": "application/x-www-form-urlencoded"]
        
        // Make request
        alamofireManager.request(requestURL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers)
            // Handle reponse
            .responseJSON { responseJSON in
                
                guard
                    // Confirm HTTP response is success
                    responseJSON.result.isSuccess,
                    
                    // Confirm response body value is in JSON format
                    let json = responseJSON.result.value as? [String:Any]
                    else {
                        // Handle error
                        if let json = responseJSON.result.value as? [String:Any] {
                            if let msg = json["msg"] as? String {
                                callback(false, msg)
                                return
                            }
                            
                            callback(false, "Failed to reset password")
                            return
                        }
                        callback(false, "Unknown Error")
                        return
                }
                
                // Map JSON Array to native type using Mappable protocol.
                
                if let status = json["status"] as? String {
                    let msg = json["msg"] as? String
                    
                    if status == "failed" {
                        callback(false, msg)
                    }
                    else {
                        callback(true, msg)
                    }
                    return
                }
        }
    }

    
    //MARK:- Like
    func like(email:String, music_id:String,
                  then callback: @escaping (Bool, String?) -> Void) {
        
        // Build Request URL to get token
        guard let requestURL = URL(string: String(format: "%@index.php/like-music", DJBitzAPI.mainURL)) else {
            callback(false, "URL error")
            return
        }
        
        // Build parameters required for getting token
        let parameters = ["email":email,
                          "music_id":music_id]
        
        // Make request
        alamofireManager.request(requestURL,
                                 method: .post,
                                 parameters: parameters)
            // Handle reponse
            .responseJSON { responseJSON in
                
                guard
                    // Confirm HTTP response is success
                    responseJSON.result.isSuccess,
                    
                    // Confirm response body value is in JSON format
                    let json = responseJSON.result.value as? [String:Any]
                    else {
                        // Handle error
                        if let json = responseJSON.result.value as? [String:Any] {
                            if let msg = json["msg"] as? String {
                                callback(false, msg)
                                return
                            }
                            
                            callback(false, "Failed to like music")
                            return
                        }
                        callback(false, "Unknown Error")
                        return
                }
                
                // Map JSON Array to native type using Mappable protocol.
                
                if let status = json["status"] as? String {
                    let msg = json["msg"] as? String
                    
                    if status == "failed" {
                        callback(false, msg)
                    }
                    else {
                        callback(true, msg)
                    }
                    return
                }
        }
    }

    func dislike(email:String, music_id:String,
              then callback: @escaping (Bool, String?) -> Void) {
        
        // Build Request URL to get token
        guard let requestURL = URL(string: String(format: "%@index.php/dislike-music", DJBitzAPI.mainURL)) else {
            callback(false, "URL error")
            return
        }
        
        // Build parameters required for getting token
        let parameters = ["email":email,
                          "music_id":music_id]
        
        // Make request
        alamofireManager.request(requestURL,
                                 method: .post,
                                 parameters: parameters)
            // Handle reponse
            .responseJSON { responseJSON in
                
                guard
                    // Confirm HTTP response is success
                    responseJSON.result.isSuccess,
                    
                    // Confirm response body value is in JSON format
                    let json = responseJSON.result.value as? [String:Any]
                    else {
                        // Handle error
                        if let json = responseJSON.result.value as? [String:Any] {
                            if let msg = json["msg"] as? String {
                                callback(false, msg)
                                return
                            }
                            
                            callback(false, "Failed to dislike music")
                            return
                        }
                        callback(false, "Unknown Error")
                        return
                }
                
                // Map JSON Array to native type using Mappable protocol.
                
                if let status = json["status"] as? String {
                    let msg = json["msg"] as? String
                    
                    if status == "failed" {
                        callback(false, msg)
                    }
                    else {
                        callback(true, msg)
                    }
                    return
                }
        }
    }

    func likeMusic(email:String, music_id:String, _ isLiked:Bool,
                   then callback: @escaping (Bool, String?) -> Void) {
        if isLiked {
            self.dislike(email: email, music_id: music_id) { (success, message) in
                callback(success, message)
            }
        }
        else {
            self.like(email: email, music_id: music_id) { (success, message) in
                callback(success, message)
            }
        }
    }
    
    //MARK:- Comment
    func addComment(email:String, music_id:String, comment:String,
                 then callback: @escaping (Bool, Int?, String?) -> Void) {
        
        // Build Request URL to get token
        guard let requestURL = URL(string: String(format: "%@index.php/add-comment", DJBitzAPI.mainURL)) else {
            callback(false, nil, "URL error")
            return
        }
        
        // Build parameters required for getting token
        let parameters = ["email":email,
                          "music_id":music_id,
                          "comment":comment]
        
        // Make request
        alamofireManager.request(requestURL,
                                 method: .post,
                                 parameters: parameters)
            // Handle reponse
            .responseJSON { responseJSON in
                
                guard
                    // Confirm HTTP response is success
                    responseJSON.result.isSuccess,
                    
                    // Confirm response body value is in JSON format
                    let json = responseJSON.result.value as? [String:Any]
                    else {
                        // Handle error
                        if let json = responseJSON.result.value as? [String:Any] {
                            if let msg = json["msg"] as? String {
                                callback(false, nil, msg)
                                return
                            }
                            
                            callback(false, nil, "Failed to add comment")
                            return
                        }
                        callback(false, nil, "Unknown Error")
                        return
                }
                
                // Map JSON Array to native type using Mappable protocol.
                
                if let status = json["status"] as? String {
                    let msg = json["msg"] as? String
                    
                    if status == "failed" {
                        callback(false, nil, msg)
                    }
                    else {
                        callback(true,json["comment"] as? Int, msg)
                    }
                    return
                }
        }
    }

    func updateComment(email:String, comment_id:String, comment:String,
                    then callback: @escaping (Bool, String?) -> Void) {
        
        // Build Request URL to get token
        guard let requestURL = URL(string: String(format: "%@index.php/update-comment", DJBitzAPI.mainURL)) else {
            callback(false, "URL error")
            return
        }
        
        // Build parameters required for getting token
        let parameters = ["email":email,
                          "comment_id":comment_id,
                          "comment":comment]
        
        // Make request
        alamofireManager.request(requestURL,
                                 method: .post,
                                 parameters: parameters)
            // Handle reponse
            .responseJSON { responseJSON in
                
                guard
                    // Confirm HTTP response is success
                    responseJSON.result.isSuccess,
                    
                    // Confirm response body value is in JSON format
                    let json = responseJSON.result.value as? [String:Any]
                    else {
                        // Handle error
                        if let json = responseJSON.result.value as? [String:Any] {
                            if let msg = json["msg"] as? String {
                                callback(false, msg)
                                return
                            }
                            
                            callback(false, "Failed to update comment")
                            return
                        }
                        callback(false, "Unknown Error")
                        return
                }
                
                // Map JSON Array to native type using Mappable protocol.
                
                if let status = json["status"] as? String {
                    let msg = json["msg"] as? String
                    
                    if status == "failed" {
                        callback(false, msg)
                    }
                    else {
                        callback(true, msg)
                    }
                    return
                }
        }
    }

    func deleteComment(email:String, comment_id:String,
                       then callback: @escaping (Bool, String?) -> Void) {
        
        // Build Request URL to get token
        guard let requestURL = URL(string: String(format: "%@index.php/delete-comment", DJBitzAPI.mainURL)) else {
            callback(false, "URL error")
            return
        }
        
        // Build parameters required for getting token
        let parameters = ["email":email,
                                       "comment_id":comment_id]
        
        // Make request
        alamofireManager.request(requestURL,
                                 method: .post,
                                 parameters: parameters)
            // Handle reponse
            .responseJSON { responseJSON in
                
                guard
                    // Confirm HTTP response is success
                    responseJSON.result.isSuccess,
                    
                    // Confirm response body value is in JSON format
                    let json = responseJSON.result.value as? [String:Any]
                    else {
                        // Handle error
                        if let json = responseJSON.result.value as? [String:Any] {
                            if let msg = json["msg"] as? String {
                                callback(false, msg)
                                return
                            }
                            
                            callback(false, "Failed to delete comment")
                            return
                        }
                        callback(false, "Unknown Error")
                        return
                }
                
                // Map JSON Array to native type using Mappable protocol.
                
                if let status = json["status"] as? String {
                    let msg = json["msg"] as? String
                    
                    if status == "failed" {
                        callback(false, msg)
                    }
                    else {
                        callback(true, msg)
                    }
                    return
                }
        }
    }
    
    public func editProfile(_ isDJ: String, _ email:String, _ username:String, _ base64String:String, _ callback: @escaping (Bool, UserInfo?, String?) -> Void){
        
        let url = String(format:"%@index.php/setprofile", DJBitzAPI.mainURL)
       // Build Request URL to get token
        guard let requestURL = URL(string: url) else {
           callback(false, nil, "URL error")
           return
        }

        
        let parameters = ["email":email, "username":username, "base64string": base64String, "isDjs": isDJ]

        // Make request
        alamofireManager.request(requestURL,
                                 method: .post,
                                 parameters: parameters)
            // Handle reponse
            .responseJSON { responseJSON in
                
                guard
                    // Confirm HTTP response is success
                    responseJSON.result.isSuccess,
                    
                    // Confirm response body value is in JSON format
                    let json = responseJSON.result.value as? [String:Any]
                    else {
                        // Handle error
                        if let json = responseJSON.result.value as? [String:Any] {
                            if let msg = json["msg"] as? String {
                                callback(false, nil, msg)
                                return
                            }
                            
                            callback(false, nil, "Failed to upload image")
                            return
                        }
                        callback(false, nil, "Unknown Error")
                        return
                }
                
                // Map JSON Array to native type using Mappable protocol.
                
                if let status = json["status"] as? String {
                    let msg = json["msg"] as? String
                    
                    if status == "failed" {
                        callback(false, nil, msg)
                    }
                    else {
                        if let userInfo = json["userInfo"] as? [String:Any] {
                            // Map JSON Array to native type using Mappable protocol.
                            guard let object = Mapper<UserInfo>().map(JSON: userInfo) else {
                                callback(false, nil, "No objects found!")
                                return
                            }
                            
                            // Save token to UserDefaults
                            //                ReferenceHelper.sharedInstance.token = [ReferenceHelper.LoginType.Devergo.rawValue, object.access_token!]
                            //                ReferenceHelper.sharedInstance.loginInfo = [ReferenceHelper.key_login_email:email, ReferenceHelper.key_login_password:password]
                            callback(true, object, msg)
                            
                        }
                        else {
                            callback(false, nil, "Failed to get user info")
                        }
                        
                        
                    }
                    return
                }
        }
    
    }
    
    public func deleteAccount(_ email: String, _ callback: @escaping (Bool, String?) -> Void) {
        let url = String(format:"%@index.php/deletecustomer", DJBitzAPI.mainURL)
        // Build Request URL to get token
        guard let requestURL = URL(string: url) else {
            callback(false, "URL error")
            return
        }
        let parameters = ["email":email]
        
        // Make request
        alamofireManager.request(requestURL,
                                 method: .post,
                                 parameters: parameters)
            // Handle reponse
            .responseJSON { responseJSON in
                
                guard
                    // Confirm HTTP response is success
                    responseJSON.result.isSuccess,
                    
                    // Confirm response body value is in JSON format
                    let json = responseJSON.result.value as? [String:Any]
                    else {
                        // Handle error
                        if let json = responseJSON.result.value as? [String:Any] {
                            if let msg = json["msg"] as? String {
                                callback(false, msg)
                                return
                            }
                            
                            callback(false, "Failed to delete comment")
                            return
                        }
                        callback(false, "Unknown Error")
                        return
                }
                
                // Map JSON Array to native type using Mappable protocol.
                
                if let status = json["status"] as? String {
                    let msg = json["msg"] as? String
                    
                    if status == "failed" {
                        callback(false, msg)
                    }
                    else {
                        callback(true, msg)
                    }
                    return
                }
        }
        
    }
    
    func addPlaylist(email:String, music_id:String,
                  then callback: @escaping (Bool, String?) -> Void) {
        
        // Build Request URL to get token
        guard let requestURL = URL(string: String(format: "%@index.php/addplaylist", DJBitzAPI.mainURL)) else {
            callback(false, "URL error")
            return
        }
        
        // Build parameters required for getting token
        let parameters = ["email":email,
                          "music_id":music_id]
        
        // Make request
        alamofireManager.request(requestURL,
                                 method: .post,
                                 parameters: parameters)
            // Handle reponse
            .responseJSON { responseJSON in
                
                guard
                    // Confirm HTTP response is success
                    responseJSON.result.isSuccess,
                    
                    // Confirm response body value is in JSON format
                    let json = responseJSON.result.value as? [String:Any]
                    else {
                        // Handle error
                        if let json = responseJSON.result.value as? [String:Any] {
                            if let msg = json["msg"] as? String {
                                callback(false, msg)
                                return
                            }
                            
                            callback(false, "Failed to like music")
                            return
                        }
                        callback(false, "Unknown Error")
                        return
                }
                
                // Map JSON Array to native type using Mappable protocol.
                
                if let status = json["status"] as? String {
                    let msg = json["msg"] as? String
                    
                    if status == "failed" {
                        callback(false, msg)
                    }
                    else {
                        callback(true, msg)
                    }
                    return
                }
        }
    }
    
    func getPlaylist(_ url:String, _ email:String, _ callback: @escaping (Bool, MusicsList?, String?) -> Void){
        if let url = URL(string: url) {
            do {
                // Build parameters required for getting token
                let parameters = ["email":email]
                
                // Perform request
                alamofireManager.request(url, method: .post, parameters: parameters)
                    .responseJSON { jsonResponse in
                        print(jsonResponse)
                        // Confirm response data is in a JSON Array format.
                        guard let json = jsonResponse.value as? [String : Any] else {
                            callback(false, nil, "JSON error: could not parse into dictionary.")
                            return
                        }
                        
                        guard !json.isEmpty else {
                            callback(false, nil, "No data was returned, no objects were found!")
                            return
                        }
                        
                        guard let results = json["result"] as? [String:Any] else {
                            callback(false, nil, "No data was returned, no objects were found!")
                            return
                        }
                        
                        // Map JSON Array to native type using Mappable protocol.
                        guard let object = Mapper<MusicsList>().map(JSON: results) else {
                            callback(false, nil, "No objects found!")
                            return
                        }
                        
                        // Complete with asynchronous callback
                        callback(true, object, nil)
                }
            }
            catch {
                
            }
        }
    }
    
    func deletePlaylist(email:String, music_id:String,
                  then callback: @escaping (Bool, String?) -> Void) {
        
        // Build Request URL to get token
        guard let requestURL = URL(string: String(format: "%@index.php/removeplaylist", DJBitzAPI.mainURL)) else {
            callback(false, "URL error")
            return
        }
        
        // Build parameters required for getting token
        let parameters = ["email":email,
                          "music_id":music_id]
        
        // Make request
        alamofireManager.request(requestURL,
                                 method: .post,
                                 parameters: parameters)
            // Handle reponse
            .responseJSON { responseJSON in
                
                guard
                    // Confirm HTTP response is success
                    responseJSON.result.isSuccess,
                    
                    // Confirm response body value is in JSON format
                    let json = responseJSON.result.value as? [String:Any]
                    else {
                        // Handle error
                        if let json = responseJSON.result.value as? [String:Any] {
                            if let msg = json["msg"] as? String {
                                callback(false, msg)
                                return
                            }
                            
                            callback(false, "Failed to delete the music in the playlist")
                            return
                        }
                        callback(false, "Unknown Error")
                        return
                }
                
                // Map JSON Array to native type using Mappable protocol.
                
                if let status = json["status"] as? String {
                    let msg = json["msg"] as? String
                    
                    if status == "failed" {
                        callback(false, msg)
                    }
                    else {
                        callback(true, msg)
                    }
                    return
                }
        }
    }

}
