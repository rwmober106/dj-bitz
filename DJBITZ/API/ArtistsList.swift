//
//  ArtistsList.swift
//  DJBITZ
//
//  Created by dung on 3/4/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import Foundation
import ObjectMapper

open class ArtistsList: Mappable {
    
    public var searchText: Bool?
    public var djsRecords: [ArtistInfomation]?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        searchText <- map["searchText"]
        djsRecords <- map["djsRecords"]
    }
}

open class LiveInfo: Mappable {
    
    public var TokenWithUid: String?
    public var TokenWithUserAccount: String?
    public var channelName: String?
    public var userId: String?
    public required init?(map: Map) {
        
    }
    public func mapping(map: Map) {
        TokenWithUid <- map["TokenWithUid"]
        TokenWithUserAccount <- map["TokenWithUserAccount"]
        channelName <- map["channelName"]
        userId <- map["userId"]
    }
}
