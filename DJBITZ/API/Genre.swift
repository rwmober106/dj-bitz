//
//  Genre.swift
//  DJBITZ
//
//  Created by dung on 3/4/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import Foundation
import ObjectMapper

open class Genre: Mappable {
    
    public var id: String?
    public var name: String?
    public var thumb_img: String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        thumb_img <- map["thumb_img"]
        
        if thumb_img!.count > 0 {
            thumb_img = thumb_img!.replacingOccurrences(of: "\\", with: "")
        }        
    }
}
