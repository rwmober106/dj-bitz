//
//  GenresList.swift
//  DJBITZ
//
//  Created by dung on 3/4/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import Foundation
import ObjectMapper

open class GenresList: Mappable {
    
    public var searchText: Bool?
    public var genresRecords: [Genre]?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        searchText <- map["searchText"]
        genresRecords <- map["genresRecords"]
    }
}
