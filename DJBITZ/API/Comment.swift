//
//  Comment.swift
//  DJBITZ
//
//  Created by dung on 5/6/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import Foundation
import ObjectMapper

open class Comment: Mappable {
    
    public var id: String?
    public var username: String?
    public var user_id: String?
    public var music_id: String?
    public var comment: String?
    public var is_deleted: String?
    public var created_at: String?
    public var updated_at: String?
    public var profile_avatar: String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        id <- map["id"]
        username <- map["username"]
        user_id <- map["user_id"]
        music_id <- map["music_id"]
        comment <- map["comment"]
        is_deleted <- map["is_deleted"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        profile_avatar <- map["profile_avatar"]
    }
}
