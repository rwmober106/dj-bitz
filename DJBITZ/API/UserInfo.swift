//
//  UserInfo.swift
//  DJBITZ
//
//  Created by dung on 5/6/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import Foundation
import ObjectMapper

open class UserInfo: Mappable {
    
    public var id: String?
    public var username: String?
    public var email: String?
    public var password: String?
    public var isDeleted: String?
    public var created_at: String?
    public var updated_at: String?
    public var isDjs: String?
    public var profile_avatar: String?

    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        id <- map["id"]
        username <- map["username"]
        email <- map["email"]
        password <- map["password"]
        isDeleted <- map["isDeleted"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        isDjs <- map["isDjs"]
        profile_avatar <- map["profile_avatar"]
    }
}
