//
//  ArtistViewController.swift
//  DJBITZ
//
//  Created by dung on 2/20/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import UIKit

class ArtistViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet fileprivate weak var headerView: UIView!

    //MARK:- Life Circle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.headerView.layer.shadowColor = UIColor.black.cgColor
        self.headerView.layer.shadowOpacity = 0.7
        self.headerView.layer.shadowOffset = CGSize(width: 0, height: 1)

        self.navigationController?.navigationBar.isHidden = true
        
        self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 15, right: 0)

        // Do any additional setup after loading the view.
        
        if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            if let viewController = navi.viewControllers.first as? ViewController {
                viewController.activeActivity(true)
                DJBitzAPI.shared.getDJsList { (success, list, error) in
                    // Must call in main thread
                    DispatchQueue.main.async {
                        if (!success) {
                            //Show error alert
                            let alert = UIAlertController(title: "Error", message: "Error while loading data from server. Please check your network connection and try again later.", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                            self.present(alert, animated: true)
                            
                            return
                        }
                        
                        // Hide progress activity
                        viewController.activeActivity(false)
                        
                        // Reload contents
                        self.collectionView.reloadData()
                    }
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
        
        if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            if let viewController = navi.viewControllers.first as? ViewController {
                if AppDelegate.shared().player != nil {
                    viewController.initUI()
                    viewController.playerView.isHidden = false
                    viewController.closeMediaPlayerCallback = closeMediaPlayer

                    self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 60, right: 0)
                }
                else {
                    if viewController.playerView != nil{
                         viewController.playerView.isHidden = true
                    }
                   
                }
            }
        }

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:- UICollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard  let _ = AppDelegate.shared().artistsList, let records = AppDelegate.shared().artistsList!.djsRecords else {
            return 0
        }
        return records.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.bounds.width*0.8, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let artistCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArtistCollectionViewCell", for: indexPath) as! ArtistCollectionViewCell
        
        artistCell.setupCell(AppDelegate.shared().artistsList!.djsRecords![indexPath.row])
        
        return artistCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ArtistProfileViewController") as! ArtistProfileViewController
        
        controller.DJProfile = AppDelegate.shared().artistsList!.djsRecords![indexPath.row]
        
        self.navigationController?.pushViewController(controller, animated: true)

    }

    //MARK:- Close MediaPlayer
    func closeMediaPlayer() {
        self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 15, right: 0)
    }
}
