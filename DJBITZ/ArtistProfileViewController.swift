//
//  ArtistProfileViewController.swift
//  DJBITZ
//
//  Created by dung on 2/21/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import UIKit
import Alamofire

class ArtistProfileViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {


    @IBOutlet fileprivate weak var collectionView: UICollectionView!
    @IBOutlet fileprivate weak var backgroundImage: UIImageView!
    @IBOutlet fileprivate weak var headerView: UIView!
    @IBOutlet fileprivate weak var buttonBack: UIButton!
    @IBOutlet fileprivate weak var buttonSearch: UIButton!
    @IBOutlet fileprivate weak var labelTitle: UILabel!
    @IBOutlet fileprivate weak var labelNickName: UILabel!
    @IBOutlet fileprivate weak var labelRealName: UILabel!
    @IBOutlet fileprivate weak var labelMixes: UILabel!
    @IBOutlet fileprivate weak var labelListening: UILabel!
    @IBOutlet fileprivate weak var searchBar: UISearchBar!
    @IBOutlet fileprivate weak var avatar: UIImageView!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var downloadMixProgress: UIProgressView!
    
    
    var isSearching = false
    var DJProfile: ArtistInfomation? = nil
    var musicsList:MusicsList? = nil
    var results = [Music]()

    var openComment = false
    var openIndex:Int = -1
    
    private var currentTextField: UITextField?
    
    private var localMixes: [Music]? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true

        // Do any additional setup after loading the view.
        
        self.collectionView.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 0)

        self.searchBar.customizedSearchBar()
        
        self.activeSearchBar()
        
        self.labelNickName.text = String(format: "DJ %@", (DJProfile?.name?.components(separatedBy: " ").first)!)
        self.labelRealName.text = DJProfile?.name
        self.labelMixes.text = "0 Mix"
        self.labelListening.text = "0 listening"
        let description = DJProfile?.description
        if description != nil {
            lblDescription.text = description
        }
        if let path = DJProfile?.profile_cover {
            //self.backgroundImage.sd_setImage(with: URL(string: String(format: "%@%@", DJBitzAPI.mainURL, path)), completed: nil)
        }
        
        self.avatar.layer.cornerRadius = self.avatar.frame.width/2
        self.avatar.clipsToBounds = true
        self.avatar.layer.shadowOffset = CGSize(width: 1, height: 1)
        //self.avatar.layer.borderWidth = 3
        //self.avatar.layer.borderColor = UIColor.white.cgColor
        if let avatarPath = DJProfile?.profile_cover {
            //self.avatar.sd_setImage(with: URL(string: String(format: "%@%@", DJBitzAPI.mainURL, avatarPath)), completed: nil)
            self.avatar.sd_setImage(with: URL(string: String(format: "%@%@", DJBitzAPI.mainURL, avatarPath)), completed: nil)
        }
        
        if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            if let viewController = navi.viewControllers.first as? ViewController {
                viewController.activeActivity(true)
                
                DJBitzAPI.shared.getMusicsList(byDJ: DJProfile?.id) { (success, list, error) in
                    // Must call in main thread
                    DispatchQueue.main.async {
                        if (!success) {
                            //Show error alert
                            let alert = UIAlertController(title: "Error", message: "Error while loading data from server. Please check your network connection and try again later.", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                            self.present(alert, animated: true)
                            
                            return
                        }

                        // Hide progress activity
                        viewController.activeActivity(false)
                        
                        // Reload contents
                        self.musicsList = list
                        if self.musicsList != nil {
                            self.results = (self.musicsList?.musicRecords)!.filter({($0.music?.count)! > 0})
                            self.labelMixes.text = self.results.count > 1 ? String(format: "%d Mixes", self.results.count) : String(format: "%d Mix", self.results.count)
                            self.collectionView.reloadData()
                        }
                    }
                }                
            }
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(contactInputWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(contactInputWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
        
        if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            if let viewController = navi.viewControllers.first as? ViewController {
                if AppDelegate.shared().player != nil {
                    viewController.initUI()
                    viewController.playerView.isHidden = false
                    viewController.closeMediaPlayerCallback = closeMediaPlayer
                    
                    self.collectionView.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 60, right: 0)
                }
                else {
                    viewController.playerView.isHidden = true
                }
            }
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }

    //MARK:- Action Handle
    @IBAction func onButtonBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func onButtonSearch(_ sender: UIButton) {
        self.isSearching = !self.isSearching
        self.activeSearchBar()
    }

    func activeSearchBar() {
        if self.isSearching {
            self.searchBar.isHidden = false
        }
        else {
            self.searchBar.isHidden = true
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:- UISearchBarDelegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let _ = self.musicsList, let ms = self.musicsList?.musicRecords?.filter({($0.music?.count)! > 0}) else {
            return
        }
        
        if searchText.count > 0 {
            self.results = ms.filter({($0.name?.contains(searchText))!})
        }
        else {
            self.results = ms
        }
        
        self.collectionView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.isSearching = false
        self.searchBar.isHidden = true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }

    //MARK:- UICollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.results.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.bounds.width*0.9, height: (openComment && openIndex == indexPath.row) ? 180: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let recentlyCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as! RecentlyCollectionViewCell
        
        recentlyCell.setupCell(self.results[indexPath.row])
        recentlyCell.callback = self.comment
        recentlyCell.complete = self.downloadMusic
        recentlyCell.textFieldDidBeginEditing = self.textFieldDidBeginEditing

        return recentlyCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        // Plays audio from remote url
        AppDelegate.shared().islocal = false
        
        
        let newList = MusicsList.init(self.results, self.musicsList!.searchText)
        AppDelegate.shared().currentSongIndex = indexPath.row
        AppDelegate.shared().updateMusicsList(newList!)

        let controller = CurrentMixViewController.sharedPlayer()
        if let _ = controller.view.superview {
            controller.navigationController?.popViewController(animated: false)
        }

        controller.music = AppDelegate.shared().musicsList?.musicRecords![indexPath.row]

        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:-
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.currentTextField = textField
    }

    func downloadMusic(_ cell: RecentlyCollectionViewCell) {
        if let indexPath = self.collectionView.indexPath(for: cell) {
            let music = self.results[indexPath.row]
            let url = String(format: "%@%@", DJBitzAPI.mainURL, music.music!)
            let mixName = music.name
            let imgUrl = String(format: "%@%@", DJBitzAPI.mainURL, music.thumb!)
            let dj = music.DJ
            let duration = music.duration
            
            // Downloads mix and thumb
            self.downloadMixProgress.isHidden = false
            guard let requestURL = URL(string: url) else {
                return
            }
            
            let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                documentsURL.appendPathComponent(String(format: "%@.mp3", mixName!))
                return (documentsURL, [.removePreviousFile])
            }
            
            if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                if let viewController = navi.viewControllers.first as? ViewController {
                    viewController.activeActivity(true)
                    alamofireManager.download(requestURL, to: destination).downloadProgress { (progress) in
                        print("download progress: ", progress.fractionCompleted*100)
                        DispatchQueue.main.async {
                            self.downloadMixProgress.setProgress(Float(progress.fractionCompleted), animated: true)
                        }
                        
                    }.response { (res) in
                        viewController.activeActivity(false)
                        self.downloadMixProgress.isHidden = true
                        self.downloadMixProgress.setProgress(0.0, animated: false)
                        if let error  = res.error {
                            print("download error: %@", error.localizedDescription)
                        } else {
                            print("download success")
                            if let mixPath = res.destinationURL?.path {
                                print("mixPath: %@", mixPath)
                            }
                            
                            guard let requestURL1 = URL(string: imgUrl) else {
                                return
                            }
                            
                            let imgDestination: DownloadRequest.DownloadFileDestination = { _, _ in
                                var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                                documentsURL.appendPathComponent(String(format: "%@.jpg", mixName!))
                                return (documentsURL, [.removePreviousFile])
                            }
                            
                            viewController.activeActivity(true)
                            alamofireManager.download(requestURL1, to: imgDestination).downloadProgress { (progress) in
                                print(progress.fractionCompleted)
                            }.response { (res) in
                                viewController.activeActivity(false)
                                if let error  = res.error {
                                    print("download error: %@", error.localizedDescription)
                                } else {
                                    print("download success")
                                    
                                    //Save local mixes
                                                                
                                    let userdefaults = UserDefaults.standard
                                    if let localMixData = userdefaults.object(forKey: "localmixes") {
                                        let localmixes = NSKeyedUnarchiver.unarchiveObject(with: localMixData as! Data) as! [Music]
                                        self.localMixes = localmixes
                                    } else {
                                        self.localMixes = [Music]()
                                    }
                                    self.localMixes?.append(music)
                                    
                                    userdefaults.setValue(NSKeyedArchiver.archivedData(withRootObject: self.localMixes!), forKey: "localmixes")
                                    
                                    userdefaults.set(music.id, forKey: music.id!)
                                    
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func addPlaylist(_ cell: RecentlyCollectionViewCell) {
        if let indexPath = self.collectionView.indexPath(for: cell) {
            let music = self.results[indexPath.row]
            if music.is_playlist == "0" {
                
                if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                    if let viewController = navi.viewControllers.first as? ViewController {
                        viewController.activeActivity(true)
                        
                        DJBitzAPI.shared.addPlaylist(email: ReferenceHelper.sharedInstance.loginInfo![ReferenceHelper.key_login_email]!, music_id: music.id!) { (success, message) in
                            if success {
                                
                                DJBitzAPI.shared.getMusicsList(byDJ: self.DJProfile?.id) { (success, list, error) in
                                    // Must call in main thread
                                    DispatchQueue.main.async {
                                        if (!success) {
                                            //Show error alert
                                            let alert = UIAlertController(title: "Error", message: "Error while loading data from server. Please check your network connection and try again later.", preferredStyle: .alert)
                                            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                                            self.present(alert, animated: true)
                                            
                                            return
                                        }

                                        // Hide progress activity
                                        viewController.activeActivity(false)
                                        
                                        // Reload contents
                                        self.musicsList = list
                                        if self.musicsList != nil {
                                            self.results = (self.musicsList?.musicRecords)!.filter({($0.music?.count)! > 0})
                                            self.labelMixes.text = self.results.count > 1 ? String(format: "%d Mixes", self.results.count) : String(format: "%d Mix", self.results.count)
                                            self.collectionView.reloadData()
                                        }
                                    }
                                }
                                
                            } else {
                                viewController.activeActivity(false)
                                self.showAlert(message!)
                            }
                        }
                        
                    }
                }
                
            }
        }
    }
    
    func comment(_ cell:RecentlyCollectionViewCell) {
        
        var needReloadCells:[IndexPath] = []
        
        if self.collectionView.cellForItem(at: IndexPath(row: openIndex, section: 0)) != nil {
            needReloadCells.append(IndexPath(row: openIndex, section: 0))
        }
        
        if let indexpath = self.collectionView.indexPath(for: cell) {
            
            if openIndex != indexpath.row {
                openIndex = indexpath.row
                openComment = true
                
                needReloadCells.append(IndexPath(row: openIndex, section: 0))
            }
            else {
                openIndex = -1
                openComment = false
            }
        }
        
        if needReloadCells.count > 0 {
            UIView.animate(withDuration: 0.8, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.9, options: UIView.AnimationOptions.curveEaseInOut, animations: {
                self.collectionView.reloadItems(at: needReloadCells)
            }, completion: { success in
                print("success")
            })
        }
        
    }
    
    func showAlert(_ message:String) {
        let alert = UIAlertController(title: "ERROR", message:
            message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel,handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

    //MARK:- Close MediaPlayer
    func closeMediaPlayer() {
        self.collectionView.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 0)
    }
    
    //MARK:-
    @objc func contactInputWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            //print("show keyboard: keyboardHeight: \(keyboardHeight)")
            
            let superview = self.currentTextField!.superview
            let textfieldFrame = superview!.convert(self.currentTextField!.frame, to: self.view)
            
            //print("\(textfieldFrame)")
            /*
             let y = keyboardHeight - (self.view.frame.height - textfieldFrame.origin.y - textfieldFrame.height)
             if y > 0 {
             
             UIView.animate(withDuration: 0.2) {
             self.inputConstraint.constant = y
             
             self.view.layoutIfNeeded()
             }
             }
             */
            
            if keyboardHeight > self.view.frame.height - textfieldFrame.origin.y - textfieldFrame.size.height{
                let offset = textfieldFrame.origin.y + textfieldFrame.size.height - (self.view.frame.height - keyboardHeight) + 5
                //print("invisible:\(offset)")
                
                UIView.animate(withDuration: 0.2) {
                    //self.inputConstraint.constant = offset + 100
                    var offsetCurrent = self.collectionView.contentOffset
                    offsetCurrent.y += offset
                    self.collectionView.contentOffset = offsetCurrent
                    self.view.layoutIfNeeded()
                }
            }
            else{
                //print("visible")
            }
        }
    }
    
    @objc func contactInputWillHide(_ notification: Notification) {
        if self.collectionView.contentOffset.y > self.collectionView.contentSize.height - self.collectionView.frame.height {
            var offsetCurrent = self.collectionView.contentOffset
            offsetCurrent.y = self.collectionView.contentSize.height - self.collectionView.frame.height
            self.collectionView.setContentOffset(offsetCurrent, animated: true)
            self.view.layoutIfNeeded()
        }
    }

}

