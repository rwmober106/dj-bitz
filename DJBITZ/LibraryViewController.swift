//
//  LibraryViewController.swift
//  DJBITZ
//
//  Created by dung on 2/20/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import UIKit
import AgoraRtcKit
import FirebaseCrashlytics
import Alamofire

class LibraryViewController: ActivityIndicatorViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private lazy var agoraKit: AgoraRtcEngineKit = {
          let engine = AgoraRtcEngineKit.sharedEngine(withAppId: Consatnt.LiveStrteamingKeyCenter.AppId, delegate: nil)
             engine.setLogFilter(AgoraLogFilter.info.rawValue)
           // engine.setLogFile(FileCenter.logFilePath())
             return engine
         }()
         
    @IBOutlet weak var constTblVTop: NSLayoutConstraint!
    @IBOutlet weak var lblNoLiveUser: UILabel!
    
    @IBOutlet weak var btnLive: UIButton!
    @IBOutlet weak var lblLive: UILabel!
    @IBOutlet fileprivate weak var collectionView: UICollectionView!
    @IBOutlet fileprivate weak var headerView: UIView!
    @IBOutlet fileprivate weak var buttonRecently: UIButton!
    @IBOutlet fileprivate weak var buttonTop10: UIButton!
    @IBOutlet fileprivate weak var buttonGenres: UIButton!
    @IBOutlet fileprivate weak var labelRecently: UILabel!
    @IBOutlet fileprivate weak var labelTop10: UILabel!
    @IBOutlet fileprivate weak var labelGenres: UILabel!

    var firstLogin = true

    var collectionType = Int(0)

    var musicsList:MusicsList? = nil
    var musics:[Music]? = nil
    var liveUsers:[LiveUsers] = [LiveUsers]()
    var viewController:ViewController? = nil
    
    var openComment = false
    var openIndex:Int = -1
    
    @IBOutlet weak var btnGoLIve: UIButton!
    @IBOutlet weak var viewLive: UIView!
    @IBOutlet weak var tblVLive: UITableView!
    private var currentTextField: UITextField?
    private var settings = Settings()
    var isFromNotification = Bool()
    
    @IBOutlet var downloadMixProgress: UIProgressView!
    var localMixes: [Music]? = nil
    
    
    //MARK:- Life Circle
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNoLiveUser.isHidden = true
        viewLive.isHidden = true
        btnGoLIve.layer.cornerRadius = 17.5
        self.navigationController?.navigationBar.isHidden = true
         tblVLive.register(UINib(nibName: "LiveUserTableViewCell", bundle: nil), forCellReuseIdentifier: "LiveUserTableViewCell")
        // Do any additional setup after loading the view.
        
        self.headerView.layer.shadowColor = UIColor.black.cgColor
        self.headerView.layer.shadowOpacity = 0.7
        self.headerView.layer.shadowOffset = CGSize(width: 0, height: 1)
        
        if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            if let viewController = navi.viewControllers.first as? ViewController {
                self.viewController = viewController
                
            }
        }
        
//        self.showAlert("From background Notification ")
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("liveStreamingNotification"), object: nil)
    }
    
    
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        
         //self.showAlert("background notification")
        
        print(notification.userInfo)
        getAllLiveCustomersAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
        if  self.viewController?.tabbar != nil{
              self.viewController?.tabbar.isHidden = false
        }
        if self.tabBarController != nil{
              self.tabBarController?.tabBar.isHidden = false
        }
        updateStatusAPI()
        self.getAllLiveCustomersAPI()
      
       // self.viewController?.constTabBarHeight.constant = 78
      
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(contactInputWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(contactInputWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
        
        
        /////////Auto Login//////////
        
        if firstLogin {
            if let info = ReferenceHelper.sharedInstance.loginInfo {
                
                if ReferenceHelper.sharedInstance.userInfo != nil {
                  //  self.activeActivity(true)
              self.viewController!.activeActivity(true)
                    DJBitzAPI.shared.getMusicsList { (success, list, error) in
                        // Must call in main thread
                        DispatchQueue.main.async {
                            // Hide progress activity
                        self.viewController!.activeActivity(false)
                            
                            if (!success) {
                                self.showAlert(error!)
                                return
                            }
                            
                            // Reload contents
                            self.musicsList = list
                            self.musics = (self.musicsList?.musicRecords)!.filter({($0.music?.count)! > 0})
                            
                            self.collectionView.reloadData()
                            self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 15, right: 0)
                        }
                    }

                    return
                }
                
              self.viewController!.activeActivity(true)
                DJBitzAPI.shared.login(email: info[ReferenceHelper.key_login_email]!, password: info[ReferenceHelper.key_login_password]!) { (success, userInfo, message) in
                    if success {
                        self.firstLogin = false

                        ReferenceHelper.sharedInstance.userInfo = userInfo

                        if ReferenceHelper.sharedInstance.userInfo != nil {
                            DJBitzAPI.shared.getMusicsList { (success, list, error) in
                                // Must call in main thread
                                DispatchQueue.main.async {
                                    // Hide progress activity
                                   self.viewController!.activeActivity(false)
                                    
                                    if (!success) {
                                        self.showAlert(error!)
                                        return
                                    }
                                    
                                    // Reload contents
                                    self.musicsList = list
                                    self.musics = (self.musicsList?.musicRecords)!.filter({($0.music?.count)! > 0})
                                    
                                    self.collectionView.reloadData()
                                    self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 15, right: 0)
                                }
                            }
                        }
                    }
                    else{
                      self.viewController!.activeActivity(false)
                        
                        if let loginController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
                            self.present(loginController, animated: false, completion: nil)
                        }
                    }
                }
            }
            else {
                if let loginController = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
                    self.present(loginController, animated: false, completion: nil)
                }
            }
        }

        /////////////////////////////
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
        if AppDelegate.shared().player != nil {
            if self.viewController != nil {
                self.viewController!.initUI()
                self.viewController!.playerView.isHidden = false
                self.viewController?.closeMediaPlayerCallback = closeMediaPlayer
                
                if self.collectionType == 2 {
                    self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 25, bottom: 60, right: 25)
                }
                else {
                    self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 60, right: 0)
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(animated)
      //  NotificationCenter.default.removeObserver(self)
    }
    
    
    @IBAction func goLiveClicked(_ sender: Any) {
        
        openLiveView()
    }
    
    //MARK:- Action Handle
    @IBAction func onButtonRecently(_ sender: UIButton) {
        viewLive.isHidden = true
        collectionType = 0
        self.openIndex = -1
        
        self.labelRecently.textColor = AppDelegate.themeColor_enable
        self.labelTop10.textColor = AppDelegate.themeColor_disable
        self.labelGenres.textColor = AppDelegate.themeColor_disable
        self.lblLive.textColor = AppDelegate.themeColor_disable

       self.viewController!.activeActivity(true)
        DJBitzAPI.shared.getMusicsList { (success, list, error) in
            // Must call in main thread
            DispatchQueue.main.async {
                // Hide progress activity
              self.viewController!.activeActivity(false)
                
                if (!success) {
                    let alert = UIAlertController(title: "Error", message: "Error while loading data from server. Please check your network connection and try again later.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                    
                    return
                }

                // Reload contents
                self.musicsList = list
                self.musics = (self.musicsList?.musicRecords)!.filter({($0.music?.count)! > 0})
                self.collectionView.reloadData()
                
                if AppDelegate.shared().player != nil {
                    self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 60, right: 0)
                }
                else {
                    self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 15, right: 0)
                }
            }
        }

    }
    
    
    @IBAction func liveClicked(_ sender: Any) {
        
        getAllLiveCustomersAPI()
        btnGoLIve.isHidden =  ReferenceHelper.sharedInstance.userInfo?.isDjs == "1" ? false : true
        constTblVTop.constant =  ReferenceHelper.sharedInstance.userInfo?.isDjs == "1" ? 60 : 0

        viewLive.isHidden = false
        lblLive.textColor =  AppDelegate.themeColor_enable
        self.labelRecently.textColor = AppDelegate.themeColor_disable
        self.labelTop10.textColor = AppDelegate.themeColor_disable
        self.labelGenres.textColor = AppDelegate.themeColor_disable
    }
    
    func openLiveView(){

        if  self.viewController?.tabbar != nil {
            self.viewController?.tabbar.isHidden = true
        }
        if self.tabBarController != nil {
            self.tabBarController?.tabBar.isHidden = true
        }
        let StoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = StoryBoard.instantiateViewController(withIdentifier: "LiveInfoViewController") as? LiveInfoViewController
//        vc?.modalPresentationStyle = .fullScreen
//        self.present(vc!, animated: true, completion: nil)
        UIView.animate(withDuration: 0.2, delay: 0.0, options: .transitionFlipFromTop, animations: {() -> Void in

            self.addChild(vc!)
            self.view.addSubview((vc?.view)!)
            vc?.didMove(toParent: self)
            vc?.view.layoutIfNeeded()
            UIApplication.shared.keyWindow?.addSubview((vc?.view)!)

        }, completion: {(_ finished: Bool) -> Void in
           vc?.view.layoutIfNeeded()
        })
    }
    

    @IBAction func onButtonTop10(_ sender: UIButton) {
        viewLive.isHidden = true
        collectionType = 1
        self.openIndex = -1

        self.labelRecently.textColor = AppDelegate.themeColor_disable
        self.labelTop10.textColor = AppDelegate.themeColor_enable
        self.labelGenres.textColor = AppDelegate.themeColor_disable
        self.lblLive.textColor = AppDelegate.themeColor_disable
        
        self.viewController!.activeActivity(true)
        DJBitzAPI.shared.getTopMusicList { (success, list, error) in
            // Must call in main thread
            DispatchQueue.main.async {
                // Hide progress activity
                self.viewController!.activeActivity(false)
                
                if (!success) {
                    let alert = UIAlertController(title: "Error", message: "Error while loading data from server. Please check your network connection and try again later.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                    
                    return
                }

                // Reload contents
                self.musics = list!.filter({($0.music?.count)! > 0})
                self.collectionView.reloadData()
                
                if AppDelegate.shared().player != nil {
                    self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 60, right: 0)
                }
                else {
                    self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 15, right: 0)
                }
            }
        }
    }
    //MARK: - API Implementatiom
    func getAllLiveCustomersAPI(){
        DJBitzAPI.shared.getAllLiveCustomers { (success, list, error) in
            // Must call in main thread
            DispatchQueue.main.async {
                if success{
                    self.liveUsers = list
                    self.tblVLive.reloadData()
                    self.lblNoLiveUser.isHidden = true
                }
                else{
                    self.liveUsers = [LiveUsers]()
                    self.tblVLive.reloadData()
                    self.lblNoLiveUser.isHidden = false
                }
            }
        }
    }
    
    
    func updateStatusAPI(){
        
        DJBitzAPI.shared.updateLiveStatus( ) { (isSuccess, token, channelName) in
            print(token)
        }
    }
      
    @IBAction func onButtonGenres(_ sender: UIButton) {
        viewLive.isHidden = true
        collectionType = 2
        self.openIndex = -1

        self.labelRecently.textColor = AppDelegate.themeColor_disable
        self.labelTop10.textColor = AppDelegate.themeColor_disable
        self.labelGenres.textColor = AppDelegate.themeColor_enable
         self.lblLive.textColor = AppDelegate.themeColor_disable

      self.viewController!.activeActivity(true)
        
        DJBitzAPI.shared.getGenresList { (success, list, error) in
            // Must call in main thread
            DispatchQueue.main.async {
                // Hide progress activity
              self.viewController!.activeActivity(false)
                
                if (!success) {
                    let alert = UIAlertController(title: "Error", message: "Error while loading data from server. Please check your network connection and try again later.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                    
                    return
                }

                // Reload contents
                self.collectionView.reloadData()
                if AppDelegate.shared().player != nil {
                    self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 25, bottom: 60, right: 25)
                }
                else {
                    self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 25, bottom: 15, right: 25)
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:- UICollectionView
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return collectionType == 2 ? 30:10
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionType == 2 {
            guard  let _ = AppDelegate.shared().genresList, let records = AppDelegate.shared().genresList!.genresRecords else {
                return 0
            }
            return records.count
        }
        else {
            guard  let _ = self.musics else {
                return 0
            }
            
            print("\(self.musics!.count)")
            
            return self.musics!.count
        }        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionType == 2 {
            return CGSize(width: (self.view.bounds.width - 60)/2, height: 170)
        }
        
        return CGSize(width: self.view.bounds.width*0.9, height: (openComment && openIndex == indexPath.row) ? 180: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionType == 2 {
            let genreCell = collectionView.dequeueReusableCell(withReuseIdentifier: "GenresCollectionViewCell", for: indexPath) as! GenresCollectionViewCell
            
            genreCell.setupCell(AppDelegate.shared().genresList!.genresRecords![indexPath.row])
            
            return genreCell
        }
        else {
            let recentlyCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecentlyCollectionViewCell", for: indexPath) as! RecentlyCollectionViewCell
            
            recentlyCell.setupCell(self.musics![indexPath.row])
            recentlyCell.callback = self.comment
            recentlyCell.didFavourite = self.favouriteMusic
            recentlyCell.complete = self.downloadMusic
            recentlyCell.didAddPlaylist = self.addPlaylist
            recentlyCell.textFieldDidBeginEditing = self.textFieldDidBeginEditing
            
            return recentlyCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // Plays audio from remote url
        AppDelegate.shared().islocal = false
        
        if collectionType == 2 {
        self.viewController!.activeActivity(true)
            let genre = AppDelegate.shared().genresList!.genresRecords![indexPath.row]
            
            DJBitzAPI.shared.getMusicsList(byGenre: genre.id) { (success, list, error) in
                // Must call in main thread
                DispatchQueue.main.async {
                    // Hide progress activity
                 self.viewController!.activeActivity(false)
                    
                    if (!success) {
                        //Show error alert
                        self.showAlert(error!)
                        
                        return
                    }
                    
                    // Open Details Page
                    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "ChosenGenreViewController") as! ChosenGenreViewController
                    
                    controller.genre = genre
                    controller.chosenMusicsList = list
                    
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }
        }
        else {
            var searchText = false
            if self.musicsList != nil, self.musicsList!.searchText != nil {
                searchText = self.musicsList!.searchText!
            }
            
            let newList = MusicsList.init(self.musics!, searchText)
            AppDelegate.shared().currentSongIndex = indexPath.row
            AppDelegate.shared().updateMusicsList(newList!)
            
            //let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            //let controller = storyboard.instantiateViewController(withIdentifier: "CurrentMixViewController") as! CurrentMixViewController
            
            let controller = CurrentMixViewController.sharedPlayer()
            if let _ = controller.view.superview {
                controller.navigationController?.popViewController(animated: false)
            }
            
            controller.music = AppDelegate.shared().musicsList?.musicRecords![indexPath.row]
            //controller.currentSongIndex = indexPath.row
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    //MARK:-
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.currentTextField = textField
    }
    
    func downloadMusic(_ cell: RecentlyCollectionViewCell) {
        if let indexPath = self.collectionView.indexPath(for: cell) {
            let music = self.musics![indexPath.row]
            let url = String(format: "%@%@", DJBitzAPI.mainURL, music.music!)
            let mixName = music.name
            let imgUrl = String(format: "%@%@", DJBitzAPI.mainURL, music.thumb!)
            
            // Downloads mix and thumb
            self.downloadMixProgress.isHidden = false
            guard let requestURL = URL(string: url) else {
                return
            }
            
            let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                documentsURL.appendPathComponent(String(format: "%@.mp3", mixName!))
                return (documentsURL, [.removePreviousFile])
            }
            
            self.viewController?.activeActivity(true)
            alamofireManager.download(requestURL, to: destination).downloadProgress { (progress) in
                print("download progress: ", progress.fractionCompleted*100)
                DispatchQueue.main.async {
                    self.downloadMixProgress.setProgress(Float(progress.fractionCompleted), animated: true)
                }
                
            }.response { (res) in
                self.viewController!.activeActivity(false)
                self.downloadMixProgress.isHidden = true
                self.downloadMixProgress.setProgress(0.0, animated: false)
                if let error  = res.error {
                    print("download error: %@", error.localizedDescription)
                } else {
                    print("download success")
                    if let mixPath = res.destinationURL?.path {
                        print("mixPath: %@", mixPath)
                        music.music = mixPath
                    }
                    
                    guard let requestURL1 = URL(string: imgUrl) else {
                        return
                    }
                    
                    let imgDestination: DownloadRequest.DownloadFileDestination = { _, _ in
                        var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                        documentsURL.appendPathComponent(String(format: "%@.jpg", mixName!))
                        return (documentsURL, [.removePreviousFile])
                    }
                    
                    self.viewController?.activeActivity(true)
                    alamofireManager.download(requestURL1, to: imgDestination).downloadProgress { (progress) in
                        print(progress.fractionCompleted)
                    }.response { (res) in
                        self.viewController!.activeActivity(false)
                        if let error  = res.error {
                            print("download error: %@", error.localizedDescription)
                        } else {
                            print("download success")
                            
                            if let imgPath = res.destinationURL?.path {
                                print("imgPath: %@", imgPath)
                                music.thumb = imgPath
                            }
                            
                            //Save local mixes
                                                        
                            let userdefaults = UserDefaults.standard
                            if let localMixData = userdefaults.object(forKey: "localmixes") {
                                let localmixes = NSKeyedUnarchiver.unarchiveObject(with: localMixData as! Data) as! [Music]
                                self.localMixes = localmixes
                            } else {
                                self.localMixes = [Music]()
                            }
                            self.localMixes?.append(music)
                            
                            userdefaults.setValue(NSKeyedArchiver.archivedData(withRootObject: self.localMixes!), forKey: "localmixes")
                            
                            userdefaults.set(music.id, forKey: music.id!)
                            
                        }
                        
                    }
                    
                    
                }
                
                
            }
            
            
        }
    }

    func addPlaylist(_ cell: RecentlyCollectionViewCell) {
        if let indexPath = self.collectionView.indexPath(for: cell) {
            let music = self.musics![indexPath.row]
                self.viewController!.activeActivity(true)
                DJBitzAPI.shared.addPlaylist(email: ReferenceHelper.sharedInstance.loginInfo![ReferenceHelper.key_login_email]!, music_id: music.id!) { (success, message) in
                    if success {
                        if self.collectionType == 1 {
                            DJBitzAPI.shared.getTopMusicList { (success, list, error) in
                                // Must call in main thread
                                DispatchQueue.main.async {
                                    // Hide progress activity
                                self.viewController!.activeActivity(false)
                                    
                                    if (!success) {
                                        self.showAlert(error!)
                                        return
                                    }
                                    
                                    // Reload contents
                                    self.musics = list!.filter({($0.music?.count)! > 0})
                                    self.collectionView.reloadData()
                                    
                                    if AppDelegate.shared().player != nil {
                                        self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 60, right: 0)
                                    }
                                    else {
                                        self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 15, right: 0)
                                    }
                                }
                            }
                        } else {
                            DJBitzAPI.shared.getMusicsList { (success, list, error) in
                                // Must call in main thread
                                DispatchQueue.main.async {
                                    // Hide progress activity
                                   self.viewController!.activeActivity(false)
                                    
                                    if (!success) {
                                        self.showAlert(error!)
                                        return
                                    }
                                    
                                    // Reload contents
                                    self.musicsList = list
                                    self.musics = (self.musicsList?.musicRecords)!.filter({($0.music?.count)! > 0})
                                    
                                    self.collectionView.reloadData()
                                    self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 15, right: 0)
                                }
                            }
                        }
                    }
                    else {
                        self.viewController!.activeActivity(false)
                        self.showAlert(message!)
                    }
                }
        }
    }
    
    func favouriteMusic(_ cell:RecentlyCollectionViewCell) {
        if let indexPath = self.collectionView.indexPath(for: cell) {
            let music = self.musics![indexPath.row]
            if let liked = music.is_liked {
                self.viewController?.activeActivity(true)
                DJBitzAPI.shared.likeMusic(email: ReferenceHelper.sharedInstance.loginInfo![ReferenceHelper.key_login_email]!, music_id: music.id!, liked == "1" ? true:false) { (success, message) in
                    if success {
                        if self.collectionType == 1 {
                            DJBitzAPI.shared.getTopMusicList { (success, list, error) in
                                // Must call in main thread
                                DispatchQueue.main.async {
                                    // Hide progress activity
                                self.viewController!.activeActivity(false)
                                    
                                    if (!success) {
                                        self.showAlert(error!)
                                        return
                                    }
                                    
                                    // Reload contents
                                    self.musics = list!.filter({($0.music?.count)! > 0})
                                    self.collectionView.reloadData()
                                    
                                    if AppDelegate.shared().player != nil {
                                        self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 60, right: 0)
                                    }
                                    else {
                                        self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 15, right: 0)
                                    }
                                }
                            }
                        }
                        else {
                            DJBitzAPI.shared.getMusicsList { (success, list, error) in
                                // Must call in main thread
                                DispatchQueue.main.async {
                                    // Hide progress activity
                                   self.viewController!.activeActivity(false)
                                    
                                    if (!success) {
                                        self.showAlert(error!)
                                        return
                                    }
                                    
                                    // Reload contents
                                    self.musicsList = list
                                    self.musics = (self.musicsList?.musicRecords)!.filter({($0.music?.count)! > 0})
                                    
                                    self.collectionView.reloadData()
                                    self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 15, right: 0)
                                }
                            }
                        }
                    }
                    else {
                        //self.viewController!.activeActivity(false)
                        self.showAlert(message!)
                    }
                }
            }
        }
    }
    
    func comment(_ cell:RecentlyCollectionViewCell) {
        
        var needReloadCells:[IndexPath] = []
        
        if self.collectionView.cellForItem(at: IndexPath(row: openIndex, section: 0)) != nil {
            needReloadCells.append(IndexPath(row: openIndex, section: 0))
        }
        
        if let indexpath = self.collectionView.indexPath(for: cell) {
            
            if openIndex != indexpath.row {
                openIndex = indexpath.row
                openComment = true
                
                needReloadCells.append(IndexPath(row: openIndex, section: 0))
            }
            else {
                openIndex = -1
                openComment = false
            }
        }
        
        if needReloadCells.count > 0 {
            UIView.animate(withDuration: 0.8, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.9, options: UIView.AnimationOptions.curveEaseInOut, animations: {
                self.collectionView.reloadItems(at: needReloadCells)
            }, completion: { success in
                print("success")
            })
        }

    }
    
    func showAlert(_ message:String) {
        let alert = UIAlertController(title: "ERROR", message:
            message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel,handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

    //MARK:- Close MediaPlayer
    func closeMediaPlayer() {
        if self.collectionType == 2 {
            self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 25, bottom: 15, right: 25)
        }
        else {
            self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 15, right: 0)
        }
    }
    
    //MARK:-
    @objc func contactInputWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            //print("show keyboard: keyboardHeight: \(keyboardHeight)")
            guard (currentTextField as? UITextField) != nil else{
                return
            }
            let superview = self.currentTextField!.superview
            let textfieldFrame = superview!.convert(self.currentTextField!.frame, to: self.view)
            
            //print("\(textfieldFrame)")
            /*
             let y = keyboardHeight - (self.view.frame.height - textfieldFrame.origin.y - textfieldFrame.height)
             if y > 0 {
             
             UIView.animate(withDuration: 0.2) {
             self.inputConstraint.constant = y
             
             self.view.layoutIfNeeded()
             }
             }
             */
            
            if keyboardHeight > self.view.frame.height - textfieldFrame.origin.y - textfieldFrame.size.height{
                let offset = textfieldFrame.origin.y + textfieldFrame.size.height - (self.view.frame.height - keyboardHeight) + 5
                //print("invisible:\(offset)")
                
                UIView.animate(withDuration: 0.2) {
                    //self.inputConstraint.constant = offset + 100
                    var offsetCurrent = self.collectionView.contentOffset
                    offsetCurrent.y += offset
                    self.collectionView.contentOffset = offsetCurrent
                    self.view.layoutIfNeeded()
                }
            }
            else{
                //print("visible")
            }
        }
    }
    
    @objc func contactInputWillHide(_ notification: Notification) {
        if self.collectionView.contentOffset.y > self.collectionView.contentSize.height - self.collectionView.frame.height {
            var offsetCurrent = self.collectionView.contentOffset
            offsetCurrent.y = self.collectionView.contentSize.height - self.collectionView.frame.height
            self.collectionView.setContentOffset(offsetCurrent, animated: true)
            self.view.layoutIfNeeded()
        }
    }
}

extension LibraryViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return liveUsers.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LiveUserTableViewCell", for: indexPath) as! LiveUserTableViewCell
        cell.delegate = self
        cell.index = indexPath.row
        let liveUser = liveUsers[indexPath.row]
        if liveUser.profile_avatar != nil {
            cell.imgVUser.sd_setImage(with: URL(string: String(format: "%@%@", DJBitzAPI.mainURL, liveUser.profile_avatar!)), completed: nil)
        }
        cell.lblUserName.text = liveUser.username
        cell.lblEmail.text = liveUser.email
        return cell
    }
 
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
}

extension LibraryViewController: liveJoinDelegate{
    func delegateJoin(int: Int) {
        if  self.viewController?.tabbar != nil{
              self.viewController?.tabbar.isHidden = true
        }
        if self.tabBarController != nil{
              self.tabBarController?.tabBar.isHidden = true
        }
        let liveUser = liveUsers[int]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LiveViewController") as! LiveViewController
        vc.isFromNotification = true
        vc.isBroadcaster = false
        vc.strChannel = liveUser.channelName ?? ""
        vc.strUsername = liveUser.username ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
