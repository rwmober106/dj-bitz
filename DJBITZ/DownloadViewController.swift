//
//  DownloadViewController.swift
//  DJBITZ
//
//  Created by MacLover on 10/6/20.
//  Copyright © 2020 BUGUNSOFT. All rights reserved.
//

import UIKit

class DownloadViewController: UIViewController,
    UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout {

    @IBOutlet var headerView: UIView!
    @IBOutlet var collectionView: UICollectionView!
    
    private var mixes: [Music]? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        
        self.headerView.layer.shadowColor = UIColor.black.cgColor
        self.headerView.layer.shadowOpacity = 0.7
        self.headerView.layer.shadowOffset = CGSize(width: 0, height: 1)
        
        self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 15, right: 0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Plays audio and show avatar on local directory
        AppDelegate.shared().islocal = true
        
        // Gets mixes stored on the local directory
        if let mixData = UserDefaults.standard.object(forKey: "localmixes") {
            mixes  = NSKeyedUnarchiver.unarchiveObject(with: mixData as! Data) as? [Music]
        } else {
            mixes = [Music]()
        }
        self.collectionView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            if let viewController = navi.viewControllers.first as? ViewController {
                if AppDelegate.shared().player != nil {
                    viewController.initUI()
                    viewController.playerView.isHidden = false
                }
                else {
                    viewController.playerView.isHidden = true
                }
            }
        }
        
    }
    
    //Mark: UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (mixes?.count)!
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let localMixCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DownloadCollectionViewCell", for: indexPath) as! RecentlyCollectionViewCell
        
        let mix = mixes![indexPath.row]
        localMixCell.setupCell(mix)
        localMixCell.didDelete = deleteMusic
        
        return localMixCell
    }
    
    //Mark: UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let searchText = false
        let newList = MusicsList.init(self.mixes!, searchText)
        AppDelegate.shared().currentSongIndex = indexPath.row
        AppDelegate.shared().updateMusicsList(newList!)
        
        // Plays audio on local directory
        AppDelegate.shared().islocal = true
        
        let controller = CurrentMixViewController.sharedPlayer()
        if let _ = controller.view.superview {
            controller.navigationController?.popViewController(animated: false)
        }

        controller.music = AppDelegate.shared().musicsList?.musicRecords![indexPath.row]

        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.bounds.width*0.9, height: 60)
    }
    
    func deleteMusic(_ cell: RecentlyCollectionViewCell) {
        if let indexPath = self.collectionView.indexPath(for: cell) {
            
            let music = self.mixes![indexPath.row]
            
            let userdefaults = UserDefaults.standard
            let fileManager = FileManager.default
            
            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            documentsURL.appendPathComponent(String(format: "%@.mp3", music.name!))
            print(documentsURL)
           do {
               try fileManager.removeItem(at: documentsURL)
           } catch let error {
               self.showAlert(error.localizedDescription)
               return
           }
            
            var documentsURL1 = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            documentsURL1.appendPathComponent(String(format: "%@.jpg", music.name!))
            do {
                try fileManager.removeItem(at: documentsURL1)
            } catch let error {
                self.showAlert(error.localizedDescription)
                return
            }
            
            self.mixes!.remove(at: indexPath.row)
            
            userdefaults.setValue(NSKeyedArchiver.archivedData(withRootObject: self.mixes!), forKey: "localmixes")
            userdefaults.removeObject(forKey: music.id!)
            
            self.collectionView.reloadData()
            
        }
    }
    
    func showAlert(_ message:String) {
        let alert = UIAlertController(title: "ERROR", message:
            message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel,handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

}
