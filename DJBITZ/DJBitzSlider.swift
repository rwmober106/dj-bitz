//
//  DJBitzSlider.swift
//  DJBITZ
//
//  Created by dung on 2/21/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import UIKit

class DJBitzSlider: UISlider {

    
    @IBInspectable var trackHeight: CGFloat = 10
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(origin: bounds.origin, size: CGSize(width: bounds.width, height: trackHeight))
    }

//    override func thumbRect(forBounds bounds: CGRect, trackRect rect: CGRect, value: Float) -> CGRect
//    {
//        let unadjustedThumbrect = super.thumbRect(forBounds: bounds, trackRect: rect, value: value)
//        let thumbOffsetToApplyOnEachSide:CGFloat = unadjustedThumbrect.size.width / 2.0
//        let minOffsetToAdd = thumbOffsetToApplyOnEachSide
//        let maxOffsetToAdd = -thumbOffsetToApplyOnEachSide
//        let offsetForValue = minOffsetToAdd + (maxOffsetToAdd - minOffsetToAdd) * CGFloat(value / (self.maximumValue - self.minimumValue))
//        var origin = unadjustedThumbrect.origin
//        origin.x += offsetForValue
//        return CGRect(origin: origin, size: unadjustedThumbrect.size)
//    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    func customizedImage(_ type: Int) {
        
        var past_duration = ""
        var all_duration = ""
        var slider_thumb = ""
        
        switch type {
        case 0:
            past_duration = "past_duration"
            all_duration = "all_duration"
            slider_thumb = "slider_thumb"
            break
        case 1:
            past_duration = "past_duration1"
            all_duration = "all_duration1"
            slider_thumb = "slider_thumb1"
            break
        default:
            break
        }
        self.setMinimumTrackImage(UIImage(named: past_duration), for: .normal)
        self.setMaximumTrackImage(UIImage(named: all_duration), for: .normal)
        self.setThumbImage(UIImage(named: slider_thumb), for: .normal)
    }

}
