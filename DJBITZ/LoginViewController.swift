//
//  LoginViewController.swift
//  DJBITZ
//
//  Created by dung on 5/6/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import UIKit

class LoginViewController: ActivityIndicatorViewController, UITextFieldDelegate {

    @IBOutlet fileprivate weak var labelLogin:UILabel!

    @IBOutlet fileprivate weak var tfUserName:UITextField!
    @IBOutlet fileprivate weak var tfEmail:UITextField!
    @IBOutlet fileprivate weak var tfPassword:UITextField!
    @IBOutlet fileprivate weak var tfConfirm:UITextField!

    @IBOutlet fileprivate weak var buttonForgot:UIButton!
    @IBOutlet fileprivate weak var buttonLogin:UIButton!
    @IBOutlet fileprivate weak var buttonSignup:UIButton!

    @IBOutlet fileprivate weak var stackFullname:UIStackView!
    @IBOutlet fileprivate weak var stackConfirm:UIStackView!
    @IBOutlet fileprivate weak var viewForgot:UIView!

    @IBOutlet weak var viewDjUser: UIView!
    @IBOutlet fileprivate weak var spaceConstraint:NSLayoutConstraint!

    @IBOutlet weak var btnDjUser: UIButton!
    var isDjUser = Bool()
    enum LoginMode:Int {
        case login
        case register
        case forgot
    }
    
    var loginMode:LoginMode = .login
    var currentTextfield:UITextField? = nil
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setPlaceHolderColor(tfUserName, string: "Full name", color: UIColor.gray)
        self.setPlaceHolderColor(tfEmail, string: "Email", color: UIColor.gray)
        self.setPlaceHolderColor(tfPassword, string: "Password", color: UIColor.gray)
        self.setPlaceHolderColor(tfConfirm, string: "Confirm password", color: UIColor.gray)

        self.buttonLogin.layer.cornerRadius = self.buttonLogin.frame.height/2
        self.buttonLogin.layer.borderWidth = 1
        self.buttonLogin.layer.borderColor = UIColor(red: 171.0/255.0, green: 38.0/255.0, blue: 250.0/255.0, alpha: 1.0).cgColor
        
        self.buttonForgot.setAttributedTitle(self.underlineTitle("Forgot password?", color: .white, fontSize: 14), for: .normal)

        self.viewForMode()
    }

    //MARK:-
    func viewForMode() {
        self.viewForgot.isHidden = loginMode == .login ? false:true
         self.viewDjUser.isHidden = loginMode == .login ? true:false
        self.stackFullname.isHidden = loginMode == .register ? false:true
        self.stackConfirm.isHidden = loginMode == .login ? true:false
        self.spaceConstraint.constant = loginMode == .login ? 10:30
        
        self.buttonSignup.setAttributedTitle(self.underlineTitle(loginMode == .login ? "Sign up":"Log in", color: .white, fontSize: 15), for: .normal)

        var title = "Log in"
        var button = "Log in"
        switch loginMode {
        case .login:
            title = "LOG IN"
            button = "Log in"
            break
        case .register:
            title = "Registration"
            button = "Sign up"
            break
        case .forgot:
            title = "Reset password"
            button = "Reset"
            break
        }

        self.labelLogin.attributedText = self.underlineTitle(title, color: UIColor(red: 171.0/255.0, green: 38.0/255.0, blue: 250.0/255.0, alpha: 1.0), fontSize: 15)
        self.buttonLogin.setTitle(button, for: .normal)
    }
    
    //MARK:- Supports
    func setPlaceHolderColor(_ textfield:UITextField, string:String, color:UIColor) {
        textfield.attributedPlaceholder = NSAttributedString(string: string,
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
    }
    
    func underlineTitle(_ string:String, color:UIColor, fontSize:CGFloat) -> NSAttributedString {
        let attributes: [NSAttributedString.Key: Any] = [
            .underlineStyle: NSUnderlineStyle.single.rawValue,
            .font: UIFont(name: "MuseoSansCyrl-300", size: fontSize) as Any,
            .foregroundColor: color
        ]
        
        return NSAttributedString(string: string, attributes: attributes)
    }
    
    @IBAction func djUserClicked(_ sender: Any) {
        isDjUser = !isDjUser
        btnDjUser.setImage(UIImage(named: isDjUser ? "ic_checkFilled" : "ic_uncheck"), for: .normal)
    }
    
    
    func showAlert(_ title:String, _ message:String) {
        let alert = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel,handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:- IBAction
    @IBAction func onButtonLogin(_ sender:UIButton) {
        
        self.currentTextfield?.resignFirstResponder()
        
        let fullName = tfUserName.text
        let email = tfEmail.text
        let password = tfPassword.text
        let repassword = tfConfirm.text

        switch loginMode {
        case .login:
            if email!.count == 0 {
                self.showAlert("ERROR", "Some information are missing. Please fill the information and try again later.")
                return
            }

            guard Helpers.sharedInstance.isValidEmail(email!) else {
                self.showAlert("ERROR", "Email address does not match the format. Please check and try again later.")
                return
            }
            
            self.activeActivity(true)
            DJBitzAPI.shared.login(email: email!, password: password!) { (success, userInfo, message) in
                self.activeActivity(false)
                if success {
                    self.tokenUpdateAPI()
                    UserDefaults.standard.set(userInfo?.username, forKey: Consatnt.User_Defaults.kUsername)
                    UserDefaults.standard.set(userInfo?.id, forKey: Consatnt.User_Defaults.kUserID)
                    ReferenceHelper.sharedInstance.loginInfo = [ReferenceHelper.key_login_email:email!, ReferenceHelper.key_login_password:password!]
                    
                    ReferenceHelper.sharedInstance.userInfo = userInfo
                    
                    self.dismiss(animated: true, completion: nil)
                }
                else {
                    self.showAlert("FAILED", message != nil ? message!:"Unknown")
                }
            }
            
            break
        case .register:
            if fullName!.count == 0 || email!.count == 0 || password!.count == 0 {
                self.showAlert("ERROR", "Some information are missing. Please fill the information and try again later.")
                return
            }

            guard Helpers.sharedInstance.isValidEmail(email!) else {
                self.showAlert("ERROR", "Email address does not match the format. Please check and try again later.")
                return
            }

            if password != repassword {
                self.showAlert("ERROR", "Repassword is not same as password. Please check and try again later.")
                return
            }

            self.activeActivity(true)
            DJBitzAPI.shared.register(isDJ: isDjUser ? "1" : "2", userName: fullName!, email: email!, password: password!) { (success, message) in
                if success {
                    DJBitzAPI.shared.login(email: email!, password: password!) { (success, userInfo, message) in
                        self.activeActivity(false)
                        if success {
                            self.tokenUpdateAPI()
                            UserDefaults.standard.set(userInfo?.username, forKey: Consatnt.User_Defaults.kUsername)
                            UserDefaults.standard.set(userInfo?.id, forKey: Consatnt.User_Defaults.kUserID)
                            ReferenceHelper.sharedInstance.loginInfo = [ReferenceHelper.key_login_email:email!, ReferenceHelper.key_login_password:password!]
                            
                            ReferenceHelper.sharedInstance.userInfo = userInfo
                            
                            self.dismiss(animated: true, completion: nil)
                        }
                        else {
                            self.showAlert("FAILED", message != nil ? message!:"Unknown")
                        }
                    }
                }
                else {
                    self.activeActivity(false)
                    self.showAlert("FAILED", message != nil ? message!:"Unknown")
                }
            }
            
            break
        case .forgot:
            if email!.count == 0 || password!.count == 0 {
                self.showAlert("ERROR", "Some information are missing. Please fill the information and try again later.")
                return
            }
            
            guard Helpers.sharedInstance.isValidEmail(email!) else {
                self.showAlert("ERROR", "Email address does not match the format. Please check and try again later.")
                return
            }
            
            if password != repassword {
                self.showAlert("ERROR", "Repassword is not same as password. Please check and try again later.")
                return
            }
            
            self.activeActivity(true)
            DJBitzAPI.shared.resetPassword(email: email!, password: password!) { (success, message) in
                self.activeActivity(false)
                
                if success {
                    self.showAlert("SUCCESSED", message != nil ? message!:"Unknown")
                }
                else {
                    self.showAlert("FAILED", message != nil ? message!:"Unknown")
                }
            }
            break
        }
    }
    
    
    func tokenUpdateAPI(){
//        DJBitzAPI.shared.updateToken { (isSuccess, token) in
//            print("token ----\(token)")
//        }
    }
    
    @IBAction func onButtonForgot(_ sender:UIButton) {
        loginMode = .forgot
        self.viewForMode()
    }
    
    @IBAction func onButtonSignup(_ sender:UIButton) {
        if loginMode == .login {
            loginMode = .register
        }
        else {
            loginMode = .login
        }
        
        self.viewForMode()
    }
    
    //MARK:- UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.currentTextfield = textField
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
