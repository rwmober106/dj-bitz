//
//  CurrentMixViewController.swift
//  DJBITZ
//
//  Created by dung on 2/21/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import UIKit
import AVFoundation

class CurrentMixViewController: ActivityIndicatorViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {

    public static let shared = CurrentMixViewController()

    @IBOutlet fileprivate weak var headerView: UIView!
    @IBOutlet fileprivate weak var buttonBack: UIButton!
    @IBOutlet fileprivate weak var labelTitle: UILabel!
    @IBOutlet fileprivate weak var backgroundImage: UIImageView!
    
    @IBOutlet fileprivate weak var slider: DJBitzSlider!
    @IBOutlet fileprivate weak var labelTimePlayed: UILabel!
    @IBOutlet fileprivate weak var labelTimeLeft: UILabel!
    @IBOutlet fileprivate weak var buttonPlay: UIButton!

    
    @IBOutlet fileprivate weak var activity: UIActivityIndicatorView!
    
    @IBOutlet var tableView: UITableView!
    
    var viewController:ViewController? = nil
    
    var music: Music? = nil
    var artist: ArtistInfomation? = nil
    // var currentSongIndex = Int(0)
    private var artistList: ArtistsList? = nil
    
    private static var sharedInstance: CurrentMixViewController? = nil
    class func sharedPlayer()-> CurrentMixViewController {
        if sharedInstance == nil {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CurrentMixViewController") as! CurrentMixViewController
            
            sharedInstance = controller
        }
        
        return sharedInstance!
    }
    
    var comments:[Comment] = []
    var commentHeight:[CGFloat] = []
    
    private var currentTextField: UITextField?

    //MARK:- Life Circle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        
        // Do any additional setup after loading the view.
        
        self.slider.customizedImage(1)
        
        self.slider.value = 0.0
        self.labelTimePlayed.text = ""
        self.labelTimeLeft.text = ""
        
        self.calculateCellHeight()
        
        if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            if let viewController = navi.viewControllers.first as? ViewController {
                self.viewController = viewController
            }
        }
        
        self.tableView.reloadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let _ = ReferenceHelper.sharedInstance.userInfo, let music = self.music, let id = music.id else {
            self.navigationController?.popViewController(animated: false)
            return
        }
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(contactInputWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(contactInputWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
        
        self.viewController!.playerView.isHidden = true
        self.slider.value = self.viewController!.slider.value
        
        var continuous = false
        if AppDelegate.shared().currentMusic != nil, AppDelegate.shared().currentMusic?.id == id {
            continuous = true
        }
        
        if !continuous {
            AppDelegate.shared().removePlayer()
            
            self.slider.value = 0.0
            self.labelTimePlayed.text = ""
            self.labelTimeLeft.text = ""
            
            self.comments = []
            self.commentHeight = []
            self.tableView.reloadData()
        }
        else {
            let duration : CMTime = (AppDelegate.shared().player?.currentItem != nil) ? AppDelegate.shared().player!.currentItem!.asset.duration : CMTime.zero
            
            let seconds : Float64 = CMTimeGetSeconds(duration)
            self.slider.maximumValue = Float(seconds)
            self.slider.isContinuous = true
            
            self.labelTimePlayed.text = "0"
            if AppDelegate.shared().player?.currentItem != nil {
                self.labelTimeLeft.text = AppDelegate.shared().secondsToHoursMinutesSeconds(interval: Double(CMTimeGetSeconds(AppDelegate.shared().player!.currentItem!.asset.duration)))
            }
            
            if AppDelegate.shared().player?.rate == 1
            {
                self.buttonPlay.setBackgroundImage(UIImage(named: "ic_pause"), for: .normal)
                self.buttonPlay.setBackgroundImage(UIImage(named: "ic_pause"), for: .selected)
            } else {
                self.buttonPlay.setBackgroundImage(UIImage(named: "ic_play"), for: .normal)
                self.buttonPlay.setBackgroundImage(UIImage(named: "ic_play"), for: .selected)
            }
            
            AppDelegate.shared().callback = self.updateUI
        }
        
        loadMusicInfo()
        /*
        self.viewController!.activeActivity(true)
        var total:Int = 0
        var loaded:Int = 0

        total += 1
        
        var DJName:String? = nil
        if let DJ = music.DJ {
            DJName = DJ
        }

        DJBitzAPI.shared.getMusicInfo(id) { (success, music, comments, message) in
            if success {
                self.music = music
                self.music!.DJ = DJName
                self.comments = comments
                
                if AppDelegate.shared().player == nil {
                    self.defaultUI()
                    
                    total += 1
                    DJBitzAPI.shared.getDJsList({ (success, artistList, error) in
                        loaded += 1
                        if loaded >= total {
                            self.viewController!.activeActivity(false)
                        }
                        
                        self.artistList = artistList
                        
                        DispatchQueue.main.async {
                            if (!success) {
                                //Show error alert
                                self.showAlert(error!)
                                return
                            }
                            
                            if let artists = artistList?.djsRecords?.filter({$0.id == self.music?.dj}), artists.count > 0 {
                                AppDelegate.shared().currentArtist = artists.first
                            }
                            
                            self.activity.startAnimating()
                            self.buttonPlay.alpha = 0.0
                            self.initNewMusic()
                            self.tableView.reloadData()
                        }
                    })
                }
                
                self.calculateCellHeight()
                self.tableView.reloadData()
                
                loaded += 1

                if loaded >= total {
                    self.viewController!.activeActivity(false)
                }

            }
            else{
                loaded += 1

                if loaded >= total {
                    self.viewController!.activeActivity(false)
                }
            }
        }

        
        
        if total == 0 {
            self.viewController!.activeActivity(false)
        }
        */
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)

        AppDelegate.shared().sliderValue = self.slider.value
        self.comments = []
    }
    
    func loadMusicInfo(){
        if let music = self.music, let id = music.id {
            self.viewController!.activeActivity(true)
            var total:Int = 0
            var loaded:Int = 0
            
            total += 1
            
            var DJName:String? = nil
            if let DJ = music.DJ {
                DJName = DJ
            }
            
            DJBitzAPI.shared.getMusicInfo(id) { (success, music, comments, message) in
                if success {
                    self.music = music
                    self.music!.DJ = DJName
                    self.comments = comments
                    
                    if AppDelegate.shared().player == nil {
                        self.defaultUI()
                    }
                    
                    
                    total += 1
                    DJBitzAPI.shared.getDJsList({ (success, artistList, error) in
                        loaded += 1
                        if loaded >= total {
                            self.viewController!.activeActivity(false)
                        }
                        
                        self.artistList = artistList
                        
                        DispatchQueue.main.async {
                            if (!success) {
                                //Show error alert
                                self.showAlert(error!)
                                return
                            }
                            
                            if let artists = artistList?.djsRecords?.filter({$0.id == self.music?.dj}), artists.count > 0 {
                                AppDelegate.shared().currentArtist = artists.first
                            }
                            
                            self.activity.startAnimating()
                            self.buttonPlay.alpha = 0.0
                            self.initNewMusic()
                            self.tableView.reloadData()
                        }
                    })
                    
                    self.calculateCellHeight()
                    self.tableView.reloadData()
                    
                    loaded += 1
                    
                    if loaded >= total {
                        self.viewController!.activeActivity(false)
                    }
                    
                }
                else{
                    loaded += 1
                    
                    if loaded >= total {
                        self.viewController!.activeActivity(false)
                    }
                }
            }       
            
            
            if total == 0 {
                self.viewController!.activeActivity(false)
            }
        }
    }
    //MARK:- Action Handle
    @IBAction func onButtonBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onButtonPlay(_ sender: UIButton) {
        guard let player = AppDelegate.shared().player else {
            
            self.defaultUI()
            self.prepareUI()
            self.initNewMusic()
            
            return
        }
        
        if AppDelegate.shared().isStopped {
            AppDelegate.shared().isStopped = false
            player.seek(to: CMTime.zero)
            player.play()
        }
        else {
            DispatchQueue.main.async {
                if player.rate == 0
                {
                    player.play()
                    sender.setBackgroundImage(UIImage(named: "ic_pause"), for: .normal)
//                    sender.setBackgroundImage(UIImage(named: "ic_pause"), for: .selected)
                } else {
                    player.pause()
                    sender.setBackgroundImage(UIImage(named: "ic_play"), for: .normal)
//                    sender.setBackgroundImage(UIImage(named: "ic_play"), for: .selected)
                }
                self.view.layoutIfNeeded()
            }
        }
    }

    @IBAction func onButtonPrev(_ sender: UIButton) {
        if AppDelegate.shared().playPrev(){
            self.music = AppDelegate.shared().currentMusic
            self.prepareUI()
        }
    }
    
    @IBAction func onButtonNext(_ sender: UIButton) {
        if AppDelegate.shared().playNext() {
            self.music = AppDelegate.shared().currentMusic
            self.prepareUI()
        }
    }

    @IBAction func sliderValueChanged(_ sender: UISlider) {
        
        if let player = AppDelegate.shared().player?.currentItem {
            
            self.showLoader()
            
            let seconds : Int64 = Int64(sender.value)
            let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
            
            player.seek(to: targetTime)
            AppDelegate.shared().player?.play()
            AppDelegate.shared().updateNowPlayingInfo()
//            if player.rate == 0
//            {
//                player.play()
//            }
        }
    }

    //MARK:- MediaPlayer
    func initNewMusic() {
        AppDelegate.shared().currentMusic = self.music
        // Must set currentSongIndex here
        AppDelegate.shared().currentSongIndex = AppDelegate.shared().shuffledList.firstIndex(where: { (music) -> Bool in
            return music.id == AppDelegate.shared().currentMusic?.id
        }) ?? 0
        
        AppDelegate.shared().initSongData()
        AppDelegate.shared().prepareCallback = self.prepareUI
        AppDelegate.shared().readyCallback = self.readyUI
        AppDelegate.shared().callback = self.updateUI
        AppDelegate.shared().endedCallback = self.endMusic
        AppDelegate.shared().showLoader = self.showLoader
        AppDelegate.shared().hideLoader = self.hideLoader
    }
    
    func showLoader() {
        self.buttonPlay.alpha = 0.0
        self.activity.startAnimating()
        AppDelegate.shared().player?.pause()
}
    
    func hideLoader() {
        self.buttonPlay.alpha = 1.0
        self.activity.stopAnimating()
        AppDelegate.shared().player?.play()
    }

    func prepareUI() {
        showLoader()
        self.slider.value = 0.0
        self.slider.isContinuous = true
        self.labelTimeLeft.text = ""
        self.labelTimePlayed.text = ""
        
        let duration : CMTime = (AppDelegate.shared().player?.currentItem != nil) ? AppDelegate.shared().player!.currentItem!.asset.duration : CMTime.zero
        
        let seconds : Float64 = CMTimeGetSeconds(duration)
        self.slider.maximumValue = Float(seconds)

        var DJName:String? = nil
        if let music = self.music, let DJ = music.DJ {
            DJName = DJ
        }

        self.viewController!.activeActivity(true)
        DJBitzAPI.shared.getMusicInfo(self.music!.id!) { (success, music, comments, message) in
            self.viewController!.activeActivity(false)

            if success {
                self.music = music
                self.music!.DJ = DJName
                self.comments = comments
                self.calculateCellHeight()
                
                if let artists = self.artistList?.djsRecords?.filter({$0.id == self.music?.dj}), artists.count > 0 {
                    AppDelegate.shared().currentArtist = artists.first
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func readyUI() {
        let duration : CMTime = AppDelegate.shared().player!.currentItem!.asset.duration
        let seconds : Float64 = CMTimeGetSeconds(duration)
        self.slider.maximumValue = Float(seconds)
        self.slider.isContinuous = true
        
        self.labelTimePlayed.text = "0"
        self.labelTimeLeft.text = AppDelegate.shared().secondsToHoursMinutesSeconds(interval: Double(CMTimeGetSeconds(AppDelegate.shared().player!.currentItem!.asset.duration)))
        
        if let _ = AppDelegate.shared().player?.currentItem?.isPlaybackLikelyToKeepUp {
            hideLoader()
            self.buttonPlay.setBackgroundImage(UIImage(named: "ic_pause"), for: .normal)
        }
    }
    
    func updateUI(_ time: CMTime) {
        if !self.slider.isTracking {
            
            self.slider.value = Float(CMTimeGetSeconds(time))
            
            self.labelTimePlayed.text = AppDelegate.shared().secondsToHoursMinutesSeconds(interval: Double(CMTimeGetSeconds(time)))
            
            if let _ = AppDelegate.shared().player?.currentItem?.isPlaybackLikelyToKeepUp {
                if self.buttonPlay.alpha < 1 {
                    self.buttonPlay.alpha = 1.0
                    self.activity.stopAnimating()
                }
                
                self.buttonPlay.setBackgroundImage(UIImage(named: "ic_pause"), for: .normal)
            }
            
            if AppDelegate.shared().player?.currentItem != nil {
                self.labelTimeLeft.text = AppDelegate.shared().secondsToHoursMinutesSeconds(interval: Double(CMTimeGetSeconds(AppDelegate.shared().player!.currentItem!.asset.duration) - CMTimeGetSeconds(time)))
            }
        }
    }
    
    func defaultUI() {
        self.labelTimePlayed.text = ""
        self.labelTimeLeft.text = ""

    }
    
    func endMusic() {
        self.buttonPlay.setBackgroundImage(UIImage(named: "ic_play"), for: .normal)
        self.buttonPlay.setBackgroundImage(UIImage(named: "ic_play"), for: .selected)
    }
    
    //MARK:- UITableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 250
        }
        
        return commentHeight[indexPath.row] + 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }

        return commentHeight.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ControlTableViewCell") as! CommentTableViewCell
            cell.music = self.music
            
            cell.labelSong.text = self.music?.name
            cell.labelArtist.text = self.music?.DJ
            if AppDelegate.shared().currentArtist != nil {
                cell.avatarImage.sd_setImage(with: URL(string: String(format: "%@%@", DJBitzAPI.mainURL, AppDelegate.shared().currentArtist!.avatar_url!)), completed: nil)
            }

            cell.labelListenCount.text = "0"
            if let listenCount = self.music?.playCounts {
                cell.labelListenCount.text = listenCount
            }

            cell.labelLikeCount.text = "0"
            if let likeCount = self.music?.likes {
                cell.labelLikeCount.text = likeCount
            }
            
            cell.buttonLike.setImage(UIImage(named: "ic_favourite"), for: .normal)
            if let liked = self.music?.is_liked {
                cell.buttonLike.setImage(UIImage(named: liked == "1" ? "ic_favourite_filled":"ic_favourite"), for: .normal)
            }
            
            cell.callback = reloadComments
            cell.didFavourite = self.favouriteMusic
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell") as! CommentTableViewCell
        if let comment = self.comments[indexPath.row].comment {
            cell.labelComment.text = comment
        }
        
        if let name = self.comments[indexPath.row].username {
            cell.labelName.text = name
        }
        let profile_avatar = self.comments[indexPath.row].profile_avatar
        if profile_avatar != nil {
            cell.avatar.sd_setImage(with: URL(string: String(format: "%@%@", DJBitzAPI.mainURL, profile_avatar!)), completed: nil)
        }
        
        if let id = self.comments[indexPath.row].user_id, let userID = ReferenceHelper.sharedInstance.userInfo?.id, id == userID {
            cell.buttonRemove.isHidden = false
            cell.viewRemove.isHidden = false
        }
        else {
            cell.buttonRemove.isHidden = true
            cell.viewRemove.isHidden = true
        }
        
        cell.removeComment = self.removeComment
        
        return cell
    }

    
    //MARK:-
    func favouriteMusic() {
        if let music = self.music {
            if let liked = music.is_liked {
                self.activeActivity(true)
                DJBitzAPI.shared.likeMusic(email: ReferenceHelper.sharedInstance.loginInfo![ReferenceHelper.key_login_email]!, music_id: music.id!, liked == "1" ? true:false) { (success, message) in
                    if success {
                        DJBitzAPI.shared.getMusicInfo(music.id!) { (success, music, comments, error) in
                            //print("set music infomation: \(success)")
                            self.activeActivity(false)
                            if success {
                                self.music = music!
                                self.tableView.reloadData()
                            }
                            else {
                                self.showAlert(message!)
                            }
                        }
                    }
                    else {
                        self.activeActivity(false)
                        self.showAlert(message!)
                    }
                }
            }
        }
    }

    func removeComment(_ cell:CommentTableViewCell) {
        if let indexPath = self.tableView.indexPath(for: cell) {
            let comment = self.comments[indexPath.row]
            
            let alert = UIAlertController.init(title: "Remove Comment", message: "This action cannot be undone. Are you sure you want to remove this comment?", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
            
            alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (UIAlertAction) in
                self.viewController!.activeActivity(true)
                DJBitzAPI.shared.deleteComment(email: ReferenceHelper.sharedInstance.loginInfo![ReferenceHelper.key_login_email]!, comment_id: comment.id!) { (success, message) in
                    
                    if success {
                        DJBitzAPI.shared.getMusicInfo(self.music!.id!, { (success, music, comments, message) in
                            self.viewController!.activeActivity(false)
                            if success {
                                self.music = music
                                self.comments = comments
                                self.calculateCellHeight()
                                self.tableView.reloadData()
                            }
                            else {
                                self.showAlert(message!)
                            }
                        })
                    }
                    else {
                        self.viewController!.activeActivity(false)
                        self.showAlert(message!)
                    }
                }
            }))
            
            self.viewController!.present(alert, animated: true, completion: nil)
        }
    }

    func reloadComments(_ comments:[Comment]) {
        if let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? CommentTableViewCell {
            cell.tfComment.text = ""
        }
        
        self.comments = comments
        self.calculateCellHeight()
        self.tableView.reloadData()
    }
    
    func calculateCellHeight() {
        self.commentHeight.removeAll()
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell") as! CommentTableViewCell

        for comment in self.comments {
            var string = ""
            if let cm = comment.comment {
                string = cm
            }
            
            self.commentHeight.append(self.heightForView(text: string, font: cell.labelComment.font, width: cell.labelComment.bounds.width))
        }
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.height) > 70 ? ceil(boundingBox.height):50
    }

    func showAlert(_ message:String) {
        let alert = UIAlertController(title: "ERROR", message:
            message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel,handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: - UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.currentTextField = textField
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
                
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:-
    @objc func contactInputWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            //print("show keyboard: keyboardHeight: \(keyboardHeight)")
            
            let superview = self.currentTextField!.superview
            let textfieldFrame = superview!.convert(self.currentTextField!.frame, to: self.view)
            
            //print("\(textfieldFrame)")
            /*
             let y = keyboardHeight - (self.view.frame.height - textfieldFrame.origin.y - textfieldFrame.height)
             if y > 0 {
             
             UIView.animate(withDuration: 0.2) {
             self.inputConstraint.constant = y
             
             self.view.layoutIfNeeded()
             }
             }
             */
            
            if keyboardHeight > self.view.frame.height - textfieldFrame.origin.y - textfieldFrame.size.height{
                let offset = textfieldFrame.origin.y + textfieldFrame.size.height - (self.view.frame.height - keyboardHeight) + 5
                //print("invisible:\(offset)")
                
                UIView.animate(withDuration: 0.2) {
                    //self.inputConstraint.constant = offset + 100
                    var offsetCurrent = self.tableView.contentOffset
                    offsetCurrent.y += offset
                    self.tableView.contentOffset = offsetCurrent
                    self.view.layoutIfNeeded()
                }
            }
            else{
                //print("visible")
            }
        }
    }
    
    @objc func contactInputWillHide(_ notification: Notification) {
        if self.tableView.contentOffset.y > self.tableView.contentSize.height - self.tableView.frame.height {
            var offsetCurrent = self.tableView.contentOffset
            offsetCurrent.y = self.tableView.contentSize.height - self.tableView.frame.height
            self.tableView.setContentOffset(offsetCurrent, animated: true)
            self.view.layoutIfNeeded()
        }
    }

}
