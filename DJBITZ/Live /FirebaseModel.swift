//
//  FirebaseModel.swift
//  DJBITZ
//
//  Created by Rakinder on 08/06/20.
//  Copyright © 2020 BUGUNSOFT. All rights reserved.
//

import UIKit
import Firebase

struct FirebaseData {
    static let Comments = "Comments"
    static let LiveCount = "LiveCount"
    static let Like = "Like"
    static let Time = "Time"
}

struct Comments {
    var username = ""
    var comment = ""
    var id = ""
    var isLike = false
    var profile_avatar = ""
    init(name: String, comment: String, id: String, isLike: Bool, profile_avatar: String) {
        self.username = name
        self.comment = comment
        self.id = id
        self.isLike = isLike
        self.profile_avatar = profile_avatar
    }
    init(snapshot: DataSnapshot) {
        let snapshotValue = snapshot.value as! [String:AnyObject]
        username = snapshotValue["username"] as? String ?? ""
        comment = snapshotValue["comment"] as? String ?? ""
        id = snapshotValue["user_id"] as? String ?? ""
        isLike = snapshotValue["isLike"] as? Bool ?? false
        profile_avatar = snapshotValue["profile_avatar"] as? String ?? ""
    }
}

struct Like {
    var username = ""
    var like = ""
    var id = ""
    
    init(name: String, like: String, id: String) {
        self.username = name
        self.like = like
        self.id = id
    }
    init(snapshot: DataSnapshot) {
        
        let snapshotValue = snapshot.value as! [String:AnyObject]
        username = snapshotValue["username"] as? String ?? ""
        like = snapshotValue["like"] as? String ?? ""
        id = snapshotValue["user_id"] as? String ?? ""
    }
}

struct LiveCount {
    var id = ""
    
    init( id: String) {
        self.id = id
    }
    init(snapshot: DataSnapshot) {
        let snapshotValue = snapshot.value as! [String:AnyObject]
        id = snapshotValue["user_id"] as? String ?? ""
    }
}

struct TimeCount {
    var id = ""
    
    init( id: String) {
        self.id = id
    }
    init(snapshot: DataSnapshot) {
        if let snapshotValue = snapshot.value as? [String:AnyObject]{
             id = snapshotValue["time"] as? String ?? ""
        }
        else{
            id = "Just Now"
        }
       
    }
}
