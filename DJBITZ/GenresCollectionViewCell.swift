//
//  GenresCollectionViewCell.swift
//  DJBITZ
//
//  Created by dung on 2/20/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import UIKit

class GenresCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet fileprivate weak var viewContainer: UIView!
    @IBOutlet fileprivate weak var avatar: UIImageView!
    @IBOutlet fileprivate weak var labelTitle: UILabel!
    @IBOutlet fileprivate weak var buttonPlay: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.avatar.layer.cornerRadius = 5
        self.avatar.clipsToBounds = true
        
        self.buttonPlay.layer.cornerRadius = self.buttonPlay.frame.height/2
        self.buttonPlay.clipsToBounds = true
        self.buttonPlay.isHidden = true
    }
    
    //MARK:- Support
    func setupCell(_ data: Genre) {
        let thumbImage = data.thumb_img
        var gotImage = false
        
        if thumbImage!.count > 0 {
            if let thumbURL = URL(string: String(format: "%@%@", DJBitzAPI.mainImageURL, thumbImage!)) {
                self.avatar!.sd_setImage(with: thumbURL, completed: nil)
                gotImage = true
            }
        }
        
        if !gotImage {
            self.avatar.image = UIImage(named: "images")
        }
        self.labelTitle.text = data.name
    }
    
    //MARK:- Action Handle
    @IBAction func onButtonPlay(_ sender: UIButton) {
        
    }
}
