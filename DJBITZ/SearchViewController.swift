//
//  SearchViewController.swift
//  DJBITZ
//
//  Created by dung on 2/20/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {

    @IBOutlet fileprivate weak var collectionView: UICollectionView!
    @IBOutlet fileprivate weak var headerView: UIView!
    @IBOutlet fileprivate weak var searchBar: UISearchBar!

    var musicsList: MusicsList? = nil
    var results = [Music]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true

        self.headerView.layer.shadowColor = UIColor.black.cgColor
        self.headerView.layer.shadowOpacity = 0.7
        self.headerView.layer.shadowOffset = CGSize(width: 0, height: 1)

        self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 15, right: 0)

        // Do any additional setup after loading the view.
        
        self.searchBar.customizedSearchBar()
        
        if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            if let viewController = navi.viewControllers.first as? ViewController {
                viewController.activeActivity(true)
                DJBitzAPI.shared.getMusicsList { (success, list, error) in
                    // Must call in main thread
                    DispatchQueue.main.async {
                        // Hide progress activity
                        viewController.activeActivity(false)
                        
                        if (!success) {
                            let alert = UIAlertController(title: "Error", message: "Error while loading data from server. Please check your network connection and try again later.", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                            self.present(alert, animated: true)
                            
                            return
                        }

                        //reload data
                        self.musicsList = list
//                        self.results = (self.musicsList?.musicRecords)!.filter({($0.music?.count)! > 0})
                        self.collectionView.reloadData()
                    }
                }
            }
        }

        
        
//        if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
//            textfield.textColor = UIColor.white
//            if let backgroundview = textfield.subviews.first {
//                // Background color
//                backgroundview.backgroundColor = UIColor(red: 69.0/255.0, green: 69.0/255.0, blue: 69.0/255.0, alpha: 1.0)
//                // Rounded corner
//                backgroundview.layer.cornerRadius = 18;
//                backgroundview.clipsToBounds = true;
//            }
//
//            //            if let clearButton = textfield.value(forKey: "clearButton") as? UIButton {
//            //                clearButton.setImage(UIImage(named: "ic_close"), for: .normal)
//            //                clearButton.tintColor = .white
//            //            }
//        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
        
        if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            if let viewController = navi.viewControllers.first as? ViewController {
                if AppDelegate.shared().player != nil {
                    viewController.initUI()
                    viewController.playerView.isHidden = false
                    viewController.closeMediaPlayerCallback = closeMediaPlayer
                    
                    self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 60, right: 0)
                }
                else {
                    if viewController.playerView != nil{
                         viewController.playerView.isHidden = true
                    }
                   
                }
            }
        }

        
//        for subView in self.searchBar.subviews  {
//            for subsubView in subView.subviews  {
//
//                subsubView.backgroundColor = UIColor.clear
//                if let textField = subsubView as? UITextField {
//                    var bounds: CGRect
//                    bounds = textField.frame
//                    bounds.size.height = 28
//                    textField.bounds = bounds
//
//                    textField.backgroundColor = UIColor(red: 69.0/255.0, green: 69.0/255.0, blue: 69.0/255.0, alpha: 1.0)
//                }
//            }
//        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:- UICollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return results.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.bounds.width*0.8, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let recentlyCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchCollectionViewCell", for: indexPath) as! RecentlyCollectionViewCell
        
        recentlyCell.setupCell(results[indexPath.row])

        return recentlyCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        // Plays audio from remote url
        AppDelegate.shared().islocal = false
        
        let newList = MusicsList.init(self.results, self.musicsList?.searchText)
        AppDelegate.shared().currentSongIndex = indexPath.row
        AppDelegate.shared().updateMusicsList(newList!)
        
        let controller = CurrentMixViewController.sharedPlayer()
        if let _ = controller.view.superview {
            controller.navigationController?.popViewController(animated: false)
        }

        controller.music = AppDelegate.shared().musicsList?.musicRecords![indexPath.row]

        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    //MARK:- UISearchBarDelegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let _ = self.musicsList, let ms = self.musicsList?.musicRecords?.filter({($0.music?.count)! > 0}) else {
            return
        }
        
        if searchText.count > 0 {            
            self.results = ms.filter({($0.name?.contains(searchText))!})
        }
        else {
            self.results = []
        }
        
        self.collectionView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
    
    //MARK:- Close MediaPlayer
    func closeMediaPlayer() {
        self.collectionView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 15, right: 0)
    }
}

extension UISearchBar {
    func customizedSearchBar() {
        if let textfield = self.value(forKey: "searchField") as? UITextField {
            textfield.textColor = UIColor.white
            if let backgroundview = textfield.subviews.first {
                // Background color
                backgroundview.backgroundColor = UIColor(red: 69.0/255.0, green: 69.0/255.0, blue: 69.0/255.0, alpha: 1.0)
                // Rounded corner
                backgroundview.layer.cornerRadius = 18;
                backgroundview.clipsToBounds = true;
            }
        }
    }
}
