//
//  RecentlyCollectionViewCell.swift
//  DJBITZ
//
//  Created by dung on 2/20/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import UIKit
import SDWebImage

class RecentlyCollectionViewCell: UICollectionViewCell, UITextFieldDelegate {
    
    @IBOutlet fileprivate weak var avatar: UIImageView!
    @IBOutlet fileprivate weak var labelArtist: UILabel!
    @IBOutlet fileprivate weak var labelSong: UILabel!
    @IBOutlet fileprivate weak var HQView: UIView!
    @IBOutlet fileprivate weak var labelTime: UILabel!
    @IBOutlet fileprivate weak var buttonPlay: UIButton!

    @IBOutlet fileprivate weak var buttonLike: UIButton!
    
    @IBOutlet fileprivate weak var labelListen: UILabel!
    @IBOutlet fileprivate weak var labelLike: UILabel!
    @IBOutlet fileprivate weak var labelComment: UILabel!

    @IBOutlet fileprivate weak var tfComment: UITextField!
    @IBOutlet fileprivate weak var buttonSend: UIButton!
    @IBOutlet var buttonDownload: UIButton!
    @IBOutlet var buttonPlaylist: UIButton!
    @IBOutlet var buttonDelete: UIButton!
    
    
    
    public var callback: ((_ cell: RecentlyCollectionViewCell) -> ())? = nil
    public var didFavourite: ((_ cell: RecentlyCollectionViewCell) -> ())? = nil
    public var textFieldDidBeginEditing: ((_ textfield:UITextField) -> ())? = nil
    public var complete: ((_ cell: RecentlyCollectionViewCell) -> ())? = nil
    public var didAddPlaylist: ((_ cell: RecentlyCollectionViewCell) -> ())? = nil
    public var didDelete: ((_ cell: RecentlyCollectionViewCell) -> ())? = nil
    

    var music:Music? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.avatar.layer.cornerRadius = 5
        self.avatar.clipsToBounds = true

        self.buttonPlay.layer.cornerRadius = self.buttonPlay.frame.height/2
        self.buttonPlay.clipsToBounds = true
        self.buttonPlay.isHidden = true
        
        self.HQView.layer.cornerRadius = 3
        self.HQView.clipsToBounds = true
        
        if self.buttonSend != nil {
            self.buttonSend.layer.cornerRadius = self.buttonSend.frame.height/2
            self.buttonSend.clipsToBounds = true
        }
    }
    
    //MARK:- Support
    func setupLocalMixCell(_ data: LocalMix) {
        if self.labelArtist != nil {
            self.labelArtist.text = data.dj
        }
        self.labelSong.text = data.name
        
        let duration = data.duration
        let times = duration.components(separatedBy: ":")
        if times.count > 1 {
            if times.first == "00" {
                self.labelTime.text = String(format: "%@:%@", times[1], times[2])
            }
            else {
                self.labelTime.text = String(format: "%d:%@:%@", Int(times.first!)!, times[1], times[2])
            }
        }
        else {
            self.labelTime.text = ""
        }
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let thumbPath = documentsURL.appendingPathComponent(String(format: "%@.jpg", data.name))
        print(thumbPath.path)
        if FileManager().fileExists(atPath: thumbPath.path) {
            print("The file already exists at the path")
            self.avatar!.image = UIImage(contentsOfFile: thumbPath.path)
        }
        
        
    }
    
    
    func setupCell(_ data: Music) {
        
        if !AppDelegate.shared().islocal {
            if self.buttonDownload != nil {
                self.buttonDownload.isEnabled = true
                if let musicId = UserDefaults.standard.object(forKey: data.id!) as? String {
                    if data.id == musicId {
                        self.buttonDownload.isEnabled = false
                        self.buttonDownload.layoutIfNeeded()
                    }
                }
            }
        }
        
        self.music = data
        
        if AppDelegate.shared().islocal {
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let imgPath = documentsURL.appendingPathComponent(String(format: "%@.jpg", data.name!))
            if FileManager().fileExists(atPath: imgPath.path) {
                print("Audio file exits")
                self.avatar!.image = UIImage(contentsOfFile: imgPath.path)
            }
        } else {
            let thumbImage = data.thumb
            if thumbImage!.count > 0 {
                if let thumbURL = URL(string: String(format: "%@%@", DJBitzAPI.mainImageURL, thumbImage!)) {
                    self.avatar!.sd_setImage(with: thumbURL, completed: nil)
                }
            }
        }
        
        
        if self.labelArtist != nil {
            self.labelArtist.text = data.DJ
        }
        self.labelSong.text = data.name
        
        guard let duration = data.duration else {
            self.labelTime.text = ""
            
            return
        }
        let times = duration.components(separatedBy: ":")
        if times.count > 1 {
            if times.first == "00" {
                self.labelTime.text = String(format: "%@:%@", times[1], times[2])
            }
            else {
                self.labelTime.text = String(format: "%d:%@:%@", Int(times.first!)!, times[1], times[2])
            }
        }
        else {
            self.labelTime.text = ""
        }
        
        if self.buttonLike != nil {
            self.buttonLike.setImage(UIImage(named: "ic_favourite"), for: .normal)
            if let liked = data.is_liked {
                self.buttonLike.setImage(UIImage(named: liked == "1" ? "ic_favourite_filled":"ic_favourite"), for: .normal)
            }
        }
        
        if self.buttonPlaylist != nil {
            self.buttonPlaylist.isEnabled = true
            if data.is_playlist == "1" {
                self.buttonPlaylist.isEnabled = false
            }
        }
        
        if labelListen != nil {
            labelListen.text = "0"
            if let listenCount = data.playCounts {
                labelListen.text = listenCount
            }
        }
        
        if labelLike != nil {
            labelLike.text = "0"
            if let likeCount = data.likes {
                labelLike.text = likeCount
            }
        }
        
        if labelComment != nil {
            labelComment.text = "0"
            if let commentCount = data.comment_count {
                labelComment.text = commentCount
            }
        }
    }
    
    func showAlert(_ title:String, _ message:String) {
        let alert = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel,handler: nil))
        
        if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            if let viewController = navi.viewControllers.first as? ViewController {
                viewController.present(alert, animated: true, completion: nil)
            }
        }
    }

    //MARK:- Action Handle
    @IBAction func onButtonPlay(_ sender: UIButton) {
        
    }

    @IBAction func onButtonFavourite(_ sender: UIButton) {
        if didFavourite != nil {
            didFavourite!(self)
        }
    }

    @IBAction func onButtonComment(_ sender: UIButton) {
        if callback != nil {
            self.callback!(self)
        }
    }
    
    @IBAction func onDownload(_ sender: Any) {
        if complete != nil {
            self.complete!(self)
            buttonDownload.isEnabled = false
            buttonDownload.layoutIfNeeded()
        }
    }
    
    @IBAction func onPlaylist(_ sender: UIButton) {
        if didAddPlaylist != nil {
            didAddPlaylist!(self)
        }
    }
    
    @IBAction func onDelete(_ sender: Any) {
        
        if didDelete != nil {
            didDelete!(self)
        }
        
    }
    
    
    @IBAction func onButtonSend(_ sender: UIButton) {
        tfComment.resignFirstResponder()
        
        if let comment = self.tfComment.text, comment.count > 0 {
            if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                if let viewController = navi.viewControllers.first as? ViewController {
                    viewController.activeActivity(true)
                    DJBitzAPI.shared.addComment(email: ReferenceHelper.sharedInstance.loginInfo![ReferenceHelper.key_login_email]!, music_id: self.music!.id!, comment: comment) { (success, comment_id, message) in
                        viewController.activeActivity(false)
                        
                        self.showAlert(success ? "SUCCESSED":"FAILED", message!)
                    }
                }
            }
        }
    }

    //MARK:- UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if self.textFieldDidBeginEditing != nil {
            self.textFieldDidBeginEditing!(textField)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
}
