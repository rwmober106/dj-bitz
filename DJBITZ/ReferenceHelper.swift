//
//  ReferenceHelper.swift
//  Devergo
//
//  Created by dung on 3/14/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import Foundation

class ReferenceHelper {

    static let sharedInstance = ReferenceHelper()
    
    var userInfo:UserInfo? = nil

    //MARK:- Login Token
    static let key_login_token = "loginToken"

    var token:[String]? {
        get {
            if let token = UserDefaults.standard.object(forKey: ReferenceHelper.key_login_token) as? [String] {
                return token
            }
            return nil
        }
        set {
            UserDefaults.standard.setValue(token, forKey: ReferenceHelper.key_login_token)
            UserDefaults.standard.synchronize()
        }
    }
    

    //MARK:- Login Info
    static let key_login_info = "loginInfo"
    static let key_login_email = "loginEmail"
    static let key_login_password = "loginPassword"

    var loginInfo:[String:String]? {
        set (newValue){
            if newValue != nil {
                UserDefaults.standard.setValue(newValue![ReferenceHelper.key_login_email], forKey: ReferenceHelper.key_login_info)
                UserDefaults.standard.synchronize()
                KeychainService.savePassword(token: newValue![ReferenceHelper.key_login_password]! as NSString)
            }
            else {
                UserDefaults.standard.setValue(nil, forKey: ReferenceHelper.key_login_info)
                UserDefaults.standard.synchronize()
                KeychainService.savePassword(token: "")
            }
        }
        get {
            var info:[String:String] = [:]
            if let email = UserDefaults.standard.object(forKey: ReferenceHelper.key_login_info) as? String, let password = KeychainService.loadPassword() {
                info[ReferenceHelper.key_login_email] = email
                info[ReferenceHelper.key_login_password] = password as String
                return info
            }
            return nil
        }
    }
}
