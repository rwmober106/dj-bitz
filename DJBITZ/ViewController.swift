//
//  ViewController.swift
//  DJBITZ
//
//  Created by dung on 2/20/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: ActivityIndicatorViewController {

    @IBOutlet weak var constTabBarHeight: NSLayoutConstraint!
    @IBOutlet var tabbar: UITabBar!
    var tabbarController: UITabBarController!

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var slider: DJBitzSlider!
    //@IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var playButton: UIButton!
    @IBOutlet fileprivate weak var closeButton: UIButton!
    @IBOutlet fileprivate weak var activity: UIActivityIndicatorView!
    @IBOutlet fileprivate weak var buttonLabel: UIButton!
    
    public var closeMediaPlayerCallback: (() -> ())? = nil
    
        var isFromNotification = Bool()
    //MARK:- Life Circle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        
        // Do any additional setup after loading the view, typically from a nib.
        
        self.tabbar.unselectedItemTintColor = UIColor.white

        self.slider.customizedImage(0)
        self.tabbarController.selectedIndex = 0
        self.tabbar.selectedItem = self.tabbar.items?.first
        
        self.playerView.isHidden = true
        
        /*
        if let splashController = storyboard?.instantiateViewController(withIdentifier: "SplashViewController") as? SplashViewController {
            self.present(splashController, animated: false, completion: nil)
        }
        */
        
        
        if ReferenceHelper.sharedInstance.loginInfo == nil, ReferenceHelper.sharedInstance.userInfo == nil {
            if let loginController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
                loginController.modalPresentationStyle = .fullScreen
                self.present(loginController, animated: false, completion: nil)
            }
        }
        //  NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("liveStreamingNotification"), object: nil)
        if isFromNotification{
            NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("liveStreamingNotification"), object: nil)
        }
    }
    
     @objc func methodOfReceivedNotification(notification: Notification) {
        NotificationCenter.default.post(name: Notification.Name("liveStreamingNotification1"), object: nil, userInfo: notification.userInfo)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: UITabBarController.classForCoder()){
            self.tabbarController = (segue.destination as! UITabBarController)
        }
    }
    
    //MARK:- Action Handle
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        if let player = AppDelegate.shared().player?.currentItem {
            let seconds : Int64 = Int64(sender.value)
            let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
            
            self.showLoader()

            player.seek(to: targetTime)
            AppDelegate.shared().player?.play()
            
//            player.seek(to: targetTime)
//
//            if player.rate == 0
//            {
//                player.play()
//            }
        }
    }
    
    @IBAction func onButtonPlay(_ sender: UIButton) {
//        DispatchQueue.main.async {
            if AppDelegate.shared().player?.timeControlStatus == .paused
            {
                AppDelegate.shared().player!.play()
                self.playButton.setBackgroundImage(UIImage(named: "ic_pause1"), for: .normal)
                self.playButton.setBackgroundImage(UIImage(named: "ic_pause1"), for: .selected)
            } else {
                AppDelegate.shared().player!.pause()
                self.playButton.setBackgroundImage(UIImage(named: "ic_play1"), for: .normal)
                self.playButton.setBackgroundImage(UIImage(named: "ic_play1"), for: .selected)
            }
//        }
    }

    @IBAction func onButtonClose(_ sender: UIButton) {
        AppDelegate.shared().removePlayer()
        self.playerView.isHidden = true
        
        if self.closeMediaPlayerCallback != nil {
            self.closeMediaPlayerCallback!()
        }
    }
    
    @IBAction func onButtonRestore(_ sender: UIButton) {
        let controller = CurrentMixViewController.sharedPlayer()
        //controller.music = AppDelegate.shared().musicsList?.musicRecords![indexPath.row]
        //controller.currentSongIndex = indexPath.row
        //self.navigationController?.pushViewController(controller, animated: true)
        if let nav = self.tabbarController.selectedViewController as? UINavigationController {
            nav.pushViewController(controller, animated: true)
        }
    }
    
    //MARK:- MediaPlayer
    func initUI() {
        
        if let music = AppDelegate.shared().currentMusic {
            self.buttonLabel.setTitle(String(format: "%@ - %@", music.DJ!, music.name!), for: .normal)
        }

        if AppDelegate.shared().isReady {
            hideLoader()
        }
        else {
            showLoader()
        }
        
        let duration : CMTime = AppDelegate.shared().player?.currentItem != nil ? AppDelegate.shared().player!.currentItem!.asset.duration : CMTime.zero
        let seconds : Float64 = CMTimeGetSeconds(duration)
        self.slider.maximumValue = Float(seconds)
        self.slider.isContinuous = true
        self.slider.value = AppDelegate.shared().sliderValue
        
        AppDelegate.shared().prepareCallback = prepareToPlay
        AppDelegate.shared().readyCallback = getReady
        AppDelegate.shared().callback = updateSlider
        AppDelegate.shared().endedCallback = closePlayerView
        AppDelegate.shared().showLoader = self.showLoader
        AppDelegate.shared().hideLoader = self.hideLoader
        
        
        if AppDelegate.shared().player?.rate == 1
        {
            self.playButton.setBackgroundImage(UIImage(named: "ic_pause1"), for: .normal)
            self.playButton.setBackgroundImage(UIImage(named: "ic_pause1"), for: .selected)
        } else {
            self.playButton.setBackgroundImage(UIImage(named: "ic_play1"), for: .normal)
            self.playButton.setBackgroundImage(UIImage(named: "ic_play1"), for: .selected)
        }

    }
    
    func updateTitleAndShowLoader(){
        if let music = AppDelegate.shared().currentMusic {
            self.buttonLabel.setTitle(String(format: "%@ - %@", music.DJ!, music.name!), for: .normal)
        }
        
        showLoader()
    }
    
    func showLoader() {
        self.playButton.alpha = 0.0
        self.activity.startAnimating()
//        AppDelegate.shared().player?.pause()
    }
    
    func hideLoader() {
        self.playButton.alpha = 1.0
        self.activity.stopAnimating()
//        AppDelegate.shared().player?.play()
    }
    
    func prepareToPlay() {
        showLoader()
        if (AppDelegate.shared().currentSongIndex >= 0) && (AppDelegate.shared().currentSongIndex < AppDelegate.shared().shuffledList.count){
            if let music = AppDelegate.shared().shuffledList[AppDelegate.shared().currentSongIndex] as? Music {
                self.buttonLabel.setTitle(String(format: "%@ - %@", music.DJ!, music.name!), for: .normal)
            }
        }
        self.slider.value = 0.0
    }
    
    func getReady() {
        hideLoader()
        let duration : CMTime = AppDelegate.shared().player!.currentItem!.asset.duration
        let seconds : Float64 = CMTimeGetSeconds(duration)
        self.slider.maximumValue = Float(seconds)
    }
    
    func updateSlider(_ time: CMTime) {
        self.slider.value = Float(CMTimeGetSeconds(time))
        
        if let _ = AppDelegate.shared().player?.currentItem?.isPlaybackLikelyToKeepUp {
            if self.playButton.alpha < 1 {
                self.playButton.alpha = 1.0
                self.activity.stopAnimating()
            }
            
            self.hideLoader()
        }

    }
    
    func closePlayerView() {
        AppDelegate.shared().removePlayer()
        self.playerView.isHidden = true
    }
}

//MARK:- UITabBar
extension ViewController: UITabBarDelegate {
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        let index = (tabBar.items?.firstIndex(of: item))!
        self.tabbarController.selectedIndex = index
        
    }
}

