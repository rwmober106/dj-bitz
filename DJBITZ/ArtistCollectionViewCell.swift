//
//  ArtistCollectionViewCell.swift
//  DJBITZ
//
//  Created by dung on 2/20/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import UIKit

class ArtistCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet fileprivate weak var avatar: UIImageView!
    @IBOutlet fileprivate weak var labelArtist: UILabel!
    @IBOutlet fileprivate weak var labelJenres: UILabel!
    @IBOutlet fileprivate weak var buttonGo: UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.avatar.layer.cornerRadius = 5
        self.avatar.clipsToBounds = true        
    }
    
    //MARK:- Support
    func setupCell(_ data: ArtistInfomation) {
        let thumbImage = data.avatar_url
        if thumbImage!.count > 0 {
            if let thumbURL = URL(string: String(format: "%@%@", DJBitzAPI.mainImageURL, thumbImage!)) {
                self.avatar!.sd_setImage(with: thumbURL, completed: nil)
            }
        }
        
        self.labelArtist.text = data.name
        if data.description != nil {
            self.labelJenres.text = data.description
        }
        
    }
    
    //MARK:- Action Handle
    @IBAction func onButtonGo(_ sender: UIButton) {
        
    }
}
