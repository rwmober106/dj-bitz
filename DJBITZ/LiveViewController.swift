//
//  LiveViewController.swift
//  DJBITZ
//
//  Created by Rakinder on 04/06/20.
//  Copyright © 2020 BUGUNSOFT. All rights reserved.
//

import UIKit
import AgoraRtcKit
import Firebase
import Alamofire
import ObjectMapper

protocol LiveVCDataSource: NSObjectProtocol {
    func liveVCNeedAgoraKit() -> AgoraRtcEngineKit
    func liveVCNeedSettings() -> Settings
}

let alamofireManager: SessionManager = {
    
    // Build the session manager with the configured session.
    let manager = Alamofire.SessionManager(configuration: .default)
    return manager
}()

class LiveViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: - Intialize the Agora Kit
    private lazy var agoraKit: AgoraRtcEngineKit = {
        let engine = AgoraRtcEngineKit.sharedEngine(withAppId: Consatnt.LiveStrteamingKeyCenter.AppId, delegate: self)
        engine.setLogFilter(AgoraLogFilter.info.rawValue)
        return engine
    }()
    
    private var settings = Settings()
    
    //MARK: - Outlet Declaration
    @IBOutlet weak var lblLiveCount: UILabel!
    @IBOutlet weak var viewLike: UIView!
    @IBOutlet weak var constViewCommentBottom: NSLayoutConstraint!
    @IBOutlet weak var tblVComments: UITableView!
    @IBOutlet weak var constViewCommentHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var imgVUser: UIImageView!
    @IBOutlet weak var viewOnlineCount: UIView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var txtFComment: UITextField!
    @IBOutlet weak var viewComment: UIView!
    @IBOutlet weak var btnSendComment: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var broadcastersView: AGEVideoContainer!
    
    @IBOutlet weak var videoMuteButton: UIButton!
    @IBOutlet weak var audioMuteButton: UIButton!
    @IBOutlet weak var beautyEffectButton: UIButton!
    @IBOutlet var sessionButtons: [UIButton]!
    
    //When came from notification then det this data
    var isFromNotification = Bool()
    var strChannel = String()
    var comments: [Comments] = [Comments]()
    var liveCount: [LiveCount] = [LiveCount]()
    var userID_Get = Int()
    var token = String()
    var strUsername = String()
    var timer: Timer?
    var ref: DatabaseReference!
    var counter = 0
    let imagesArray = [#imageLiteral(resourceName: "red"), #imageLiteral(resourceName: "purple"), #imageLiteral(resourceName: "red"), #imageLiteral(resourceName: "purple"),#imageLiteral(resourceName: "red"), #imageLiteral(resourceName: "purple"), #imageLiteral(resourceName: "red"), #imageLiteral(resourceName: "purple"),#imageLiteral(resourceName: "red"), #imageLiteral(resourceName: "purple"), #imageLiteral(resourceName: "red"), #imageLiteral(resourceName: "purple"),#imageLiteral(resourceName: "red"), #imageLiteral(resourceName: "purple"), #imageLiteral(resourceName: "red"), #imageLiteral(resourceName: "purple")]
    var fbAnimationView: FBAnimationView!
    
    var isBroadcaster = Bool() // to get from last view to check join live as broadcaster or audience
    private let maxVideoSession = 40
      var dataSource: LiveVCDataSource?
    var strAutoIDforLiveCount = String()
    
    //Beauty effect
    private let beautyOptions: AgoraBeautyOptions = {
        let options = AgoraBeautyOptions()
        options.lighteningContrastLevel = .normal
        options.lighteningLevel = 0.7
        options.smoothnessLevel = 0.5
        options.rednessLevel = 0.1
        return options
    }()
    
    private var isMutedVideo = false {
        didSet {
            // mute local video
            agoraKit.muteLocalVideoStream(isMutedVideo)
            videoMuteButton.isSelected = isMutedVideo
        }
    }
    
    //MARK: - View Lifecycle
       override func viewDidLoad() {
           super.viewDidLoad()
           self.tabBarController?.tabBar.isHidden = true
           initUI()
       }
       
       override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           self.navigationController?.navigationBar.isHidden = true
           NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
           NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
       }
       
       override func viewWillDisappear(_ animated: Bool) {
           super.viewWillDisappear(animated)
         updateStatusAPI()
           NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
           NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
       }
    
    //MARK: - Common Functions
    func initUI(){
        lblUsername.text = strUsername
        // add the animation at like click
        viewLike.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapped)))
        setupFBAnimationView()
        
        tblVComments.transform = CGAffineTransform(rotationAngle: (-.pi))
        tblVComments.register(UINib(nibName: "LiveCommentsTableViewCell", bundle: nil), forCellReuseIdentifier: "LiveCommentsTableViewCell")
        
        //Image view and like button corner radius
        imgVUser.layer.borderColor = UIColor(red: 171/255, green: 38/255, blue: 250/255, alpha: 1.0).cgColor
        imgVUser.layer.borderWidth = 1.0
        imgVUser.layer.cornerRadius = imgVUser.frame.size.height/2
        
        if ReferenceHelper.sharedInstance.userInfo?.profile_avatar != nil {
            let profile_avatar = ReferenceHelper.sharedInstance.userInfo?.profile_avatar
            imgVUser.sd_setImage(with: URL(string: String(format: "%@%@", DJBitzAPI.mainURL, profile_avatar!)), completed: nil)
        }
        
        btnLike.layer.cornerRadius = btnLike.frame.size.height/2
        btnSendComment.layer.cornerRadius = btnSendComment.frame.size.height/2
        
        viewOnlineCount.layer.cornerRadius = 7.0
        viewOnlineCount.layer.borderColor = UIColor.white.cgColor
        viewOnlineCount.layer.borderWidth = 1.0
        viewComment.layer.cornerRadius = viewComment.frame.size.height/2
        
        guard let sessionButtons = sessionButtons else {
            return
        }
        for item in sessionButtons {
            item.isHidden = true
        }
        apiImplementation()
  
    }
    
    func dynamicHeight(font: UIFont, width: CGFloat,str: String) -> CGFloat{
        
        let constraintRect = CGSize(width: width, height: CGFloat(MAXFLOAT))
        let boundingBox =  str.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [kCTFontAttributeName as NSAttributedString.Key: font], context: nil)
        return boundingBox.height
    }
    
    
    //MARK: - API Implementation
    func apiImplementation(){
        if isFromNotification{
              DJBitzAPI.shared.getLiveStreamingToken(channel: strChannel, id: "\(Helpers.sharedInstance.getFromDefaults(strKey: Consatnt.User_Defaults.kUserID))") { (isSuccess, token, channelName) in
                  
                  self.settings.roomName = token?.channelName
                  self.token = token?.TokenWithUid ?? ""
                  self.userID_Get = Int(Helpers.sharedInstance.getFromDefaults(strKey: Consatnt.User_Defaults.kUserID)) ?? 0
                  
                  if self.isBroadcaster{
                      self.settings.role = .broadcaster
                  }
                  else{
                      self.settings.role = .audience
                  }
                  
                  self.updateButtonsVisiablity()
                    self.loadAgoraKit()
                  
                  self.ref = Database.database().reference()
                  self.likeObserverFirebase()
                  self.commentObserverFirebase()
                  self.liveCountObserverFirebase()
                  self.timeObserverFirebase()
                  self.addLiveCount()
              }
          }
    }
    
    func updateStatusAPI(){
                 DJBitzAPI.shared.updateLiveStatus( ) { (isSuccess, token, channelName) in

                 }
             }

    //MARK: - Firebase Methods
    func likeObserverFirebase(){ // Like handling in Firebase
        ref.child(FirebaseData.Like).child(settings.roomName ?? "").observe(.value, with: { snapshot in
            if let value = snapshot.value as? NSDictionary{
                           if let username = value["username"] as? String{
                              print("\(username) like your live ")
                           }
                       }
            self.likeAnimate()
            self.remove(child: FirebaseData.Like)
        })
    }
    
    func timeObserverFirebase(){ // Update time when user start live
        ref.child(FirebaseData.Time).child(settings.roomName ?? "").observe(.value, with: { snapshot in
            if let value = snapshot.value as? NSDictionary{
                if let time = value["time"] as? String{
                    self.lblTime.text = time
                }
            }
        })
    }
    
    func commentObserverFirebase(){
        /*
         Get the Comment data from Firebase
         Save the live data in model and show in the comment table
         */
        ref.child(FirebaseData.Comments).child(settings.roomName ?? "").observe(.value, with: { snapshot in
            var plc: [Comments] = []
            for eachPlace in (snapshot.children){
                let place = Comments(snapshot: eachPlace as! DataSnapshot)
                    plc.append(place)
                
            }
            self.comments = plc.reversed()
            self.tblVComments.reloadData()
            if self.comments.count > 0{
                let index = IndexPath(row: 0, section: 0)
                self.tblVComments.scrollToRow(at: index, at: .top, animated: true)
            }
        })
    }
    
    func addLiveCount(){ // Only audience user will count as user in live not broadcaster
        let isHidden = self.settings.role == .broadcaster
        
        if !isHidden{ // audience
            var isAlreadyJoined = Bool()
            let userID =  Helpers.sharedInstance.getFromDefaults(strKey: Consatnt.User_Defaults.kUserID)
            for i in liveCount{
                if i.id == userID{
                    isAlreadyJoined = true
                }
                
            }
            print(isAlreadyJoined)
            if let strChannel =   UserDefaults.standard.value(forKey: "channelName") as? String{
                if settings.roomName == strChannel{
                    isAlreadyJoined = true
                }
            }
            if !isAlreadyJoined{
                isAlreadyJoined = true
                let data = ["user_id": userID] as [String : Any]
            
              let value =   self.ref.child(FirebaseData.LiveCount).child(self.settings.roomName ?? "").childByAutoId()
                strAutoIDforLiveCount =   value.key ?? ""
                UserDefaults.standard.set(strAutoIDforLiveCount, forKey: "livecountValue")
                        UserDefaults.standard.synchronize()
                       UserDefaults.standard.set(self.settings.roomName, forKey: "channelName")
                       UserDefaults.standard.synchronize()
                value.setValue(data)
            }
        }
    }
    
    func liveCountObserverFirebase(){
        /*
         Get the live data from Firebase
         Save the live data in model
         */
        ref.child(FirebaseData.LiveCount).child(settings.roomName ?? "").observe(.value, with: { snapshot in
            var live: [LiveCount] = []
            for eachPlace in (snapshot.children){
                let place = LiveCount(snapshot: eachPlace as! DataSnapshot)
                live.append(place)
            }
            
            /*
             Update the count of live to every user
             */
            self.liveCount = live
            let strThousand = String(format:"%.2f", live.count/1000)
            self.lblLiveCount.text = live.count >= 1000 ? "\(strThousand)k" : "\(live.count)"
        //    self.addLiveCount()
            /*
             If the user is audience, then add the user id in firebase
             Add after check it already contain or not
             */
        })
       // addLiveCount()
    }
    
    func remove(child: String) {
        guard let ref = ref else{
            return
        }
        ref.child(child).child(settings.roomName ?? "").removeValue { (error, ref) in
            print(error as Any)
        }
    }
    
    //MARK: - Animation Like View
    func setupFBAnimationView() {
        fbAnimationView = FBAnimationView()
        fbAnimationView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(fbAnimationView)
        if #available(iOS 11.0, *) {
            
            [fbAnimationView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: -50),
             fbAnimationView.rightAnchor.constraint(equalTo: self.view.rightAnchor),
             fbAnimationView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -60),
             fbAnimationView.heightAnchor.constraint(equalToConstant: 250)].forEach { ($0.isActive = true)  }
        }
    }
    
    @objc func handleTapped() {
        let username = Helpers.sharedInstance.getFromDefaults(strKey: Consatnt.User_Defaults.kUsername)
        let userid = Helpers.sharedInstance.getFromDefaults(strKey: Consatnt.User_Defaults.kUserID)
        let profile_avatar = ReferenceHelper.sharedInstance.userInfo?.profile_avatar ?? ""
        let data = ["username": username,
                    "user_id": userid,
                    "like": "1"] as [String : Any]
        ref.child(FirebaseData.Like).child(settings.roomName ?? "").childByAutoId().setValue(data)

                  let dataLike = ["username": username,
                              "user_id": userid,
                              "isLike" : true,
                              "profile_avatar" : profile_avatar,
                              "comment": ""] as [String : Any]
                  ref.child(FirebaseData.Comments).child(settings.roomName ?? "").childByAutoId().setValue(dataLike)
    }
    
    func likeAnimate(){
        let randomIndex = Int(arc4random_uniform(UInt32(imagesArray.count)))
        let randomImage = imagesArray[randomIndex]
        fbAnimationView.animate(icon: randomImage)
    }
    
    @IBAction func likeClicked(_ sender: Any) {
        
    }
    
    //MARK:- Keyboard Methods
    @objc func keyboardWillShow(_ notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.constViewCommentBottom.constant = keyboardFrame.size.height
        })
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification) {
        self.constViewCommentBottom.constant = 0
    }
    
    //MARK: - Text Field Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
              if self.tabBarController != nil{
                    self.tabBarController?.tabBar.isHidden = true
              }
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - Button Actions
      private var isMutedAudio = false {
          didSet {
              // mute local audio
              agoraKit.muteLocalAudioStream(isMutedAudio)
              audioMuteButton.isSelected = isMutedAudio
          }
      }
      
      private var isBeautyOn = false {
          didSet {
              // improve local render view
              agoraKit.setBeautyEffectOptions(isBeautyOn,
                                              options: isBeautyOn ? beautyOptions : nil)
              beautyEffectButton.isSelected = isBeautyOn
          }
      }
      
      private var isSwitchCamera = false {
          didSet {
              agoraKit.switchCamera()
          }
      }
      
      private var videoSessions = [VideoSession]() {
          didSet {
              //  placeholderView.isHidden = (videoSessions.count == 0 ? false : true)
              // update render view layout
              updateBroadcastersView()
          }
      }
      
    @IBAction func doSwitchCameraPressed(_ sender: UIButton) {
        isSwitchCamera.toggle()
    }
    
    @IBAction func doBeautyPressed(_ sender: UIButton) {
        isBeautyOn.toggle()
    }
    
    @IBAction func doMuteVideoPressed(_ sender: UIButton) {
        isMutedVideo.toggle()
    }
    
    @IBAction func doMuteAudioPressed(_ sender: UIButton) {
        isMutedAudio.toggle()
    }
    
    @IBAction func doLeavePressed(_ sender: UIButton) {
        timer?.invalidate()
        leaveChannel()
    }
    
    @IBAction func sendCommentClicked(_ sender: Any) {
        if txtFComment.text != ""{
            let username = Helpers.sharedInstance.getFromDefaults(strKey: Consatnt.User_Defaults.kUsername)
            let profile_avatar = ReferenceHelper.sharedInstance.userInfo?.profile_avatar ?? ""
            let userid = Helpers.sharedInstance.getFromDefaults(strKey: Consatnt.User_Defaults.kUserID)
            let data = ["username": username,
                        "user_id": userid,
                        "isLike" : false,
                        "profile_avatar" : profile_avatar,
                        "comment": txtFComment.text as Any] as [String : Any]
            ref.child(FirebaseData.Comments).child(settings.roomName ?? "").childByAutoId().setValue(data)
            txtFComment.text = ""
            txtFComment.resignFirstResponder()
        }
    }
    
    func showAlert(_ title:String, _ message:String) {
        let alert = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel,handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

private extension LiveViewController {
    func updateBroadcastersView() {
        // video views layout
        if videoSessions.count == maxVideoSession {
            broadcastersView.reload(level: 0, animated: true)
        } else {
            var rank: Int
            var row: Int
            
            if videoSessions.count == 0 {
                return
            } else if videoSessions.count == 1 {
                rank = 1
                row = 1
            } else if videoSessions.count == 2 {
                rank = 1
                row = 2
            } else {
                rank = 2
                row = Int(ceil(Double(videoSessions.count) / Double(rank)))
            }
            
            let itemWidth = CGFloat(1.0) / CGFloat(rank)
            let itemHeight = CGFloat(1.0) / CGFloat(row)
            let itemSize = CGSize(width: itemWidth, height: itemHeight)
            let layout = AGEVideoLayout(level: 0)
                .itemSize(.scale(itemSize))
            
            broadcastersView
                .listCount { [unowned self] (_) -> Int in
                    return self.videoSessions.count
            }.listItem { [unowned self] (index) -> UIView in
                return self.videoSessions[index.item].hostingView
            }
            broadcastersView.setLayouts([layout], animated: true)
        }
    }
    
    func updateButtonsVisiablity() {
        guard let sessionButtons = sessionButtons else {
            return
        }
        let isHidden = settings.role == .audience
        if !isHidden{ // fro broadcaster add the timer
            addTimeAtLive()
        }
        
        for item in sessionButtons {
            item.layer.cornerRadius = item.frame.size.height/2
            item.isHidden = isHidden
        }
    }
    
    func addTimeAtLive(){
        DispatchQueue.main.async {

            self.timer = Timer.scheduledTimer(withTimeInterval: 60, repeats: true) { timer in
                self.counter =  self.counter + 1
                
                let minValue = self.counter < 60 ? "min" : "hour"
                let timeValue = Int(self.counter % 60) < 1 ? self.counter : Int(self.counter % 60)
                let data = ["time": "\(timeValue) \(minValue)"] as [String : Any]
                self.ref.child(FirebaseData.Time).child(self.settings.roomName ?? "").setValue(data)
                
            }
        }
    }
    
    func minutesToHoursMinutes (minutes : Int) -> (hours : Int , leftMinutes : Int) {
        return (minutes / 60, (minutes % 60))
    }
    
    func setIdleTimerActive(_ active: Bool) {
        UIApplication.shared.isIdleTimerDisabled = !active
    }
}

private extension LiveViewController {
    func getSession(of uid: UInt) -> VideoSession? {
        for session in videoSessions {
            if session.uid == uid {
                return session
            }
        }
        return nil
    }
    
    func videoSession(of uid: UInt) -> VideoSession {
        if let fetchedSession = getSession(of: uid) {
            return fetchedSession
        } else {
            let newSession = VideoSession(uid: uid)
            videoSessions.append(newSession)
            return newSession
        }
    }
}

//MARK: - Agora Media SDK
private extension LiveViewController {
    func loadAgoraKit() {
        guard let channelId = settings.roomName else {
            return
        }
        
        setIdleTimerActive(false)
        
        // Step 1, set delegate to inform the app on AgoraRtcEngineKit events
        agoraKit.delegate = self
        // Step 2, set live broadcasting mode
        // for details: https://docs.agora.io/cn/Video/API%20Reference/oc/Classes/AgoraRtcEngineKit.html#//api/name/setChannelProfile:
        agoraKit.setChannelProfile(.liveBroadcasting)
        // set client role
        agoraKit.setClientRole(settings.role)
        
        // Step 3, Warning: only enable dual stream mode if there will be more than one broadcaster in the channel
        agoraKit.enableDualStreamMode(true)
        
        // Step 4, enable the video module
        agoraKit.enableVideo()
        // set video configuration
        agoraKit.setVideoEncoderConfiguration(
            AgoraVideoEncoderConfiguration(
                size: settings.dimension,
                frameRate: settings.frameRate,
                bitrate: AgoraVideoBitrateStandard,
                orientationMode: .adaptative
            )
        )
        
        // if current role is broadcaster, add local render view and start preview
        if settings.role == .broadcaster {
            addLocalSession()
            agoraKit.startPreview()
        }
        
        // Step 5, join channel and start group chat
        // If join  channel success, agoraKit triggers it's delegate function
        // 'rtcEngine(_ engine: AgoraRtcEngineKit, didJoinChannel channel: String, withUid uid: UInt, elapsed: Int)'
        
        let id = Int(userID_Get)
        let code = agoraKit.joinChannel(byToken: token, channelId: channelId, info: nil, uid:  UInt(id)) { (channel, uid, elapsed) in
            print("channel ************ -----\(channel)")
            
        }
        if code != 0 {
            DispatchQueue.main.async(execute: {
                print( "Join channel failed: \(code)")
            })
        }
        // Step 6, set speaker audio route
        agoraKit.setEnableSpeakerphone(true)
    }
    
    func addLocalSession() {
        let localSession = VideoSession.localSession()
        localSession.updateInfo(fps: settings.frameRate.rawValue)
        videoSessions.append(localSession)
        agoraKit.setupLocalVideo(localSession.canvas)
    }
    
    func leaveChannel() {
        // Step 1, release local AgoraRtcVideoCanvas instance
        agoraKit.setupLocalVideo(nil)
        // Step 2, leave channel and end group chat
        agoraKit.leaveChannel(nil)
        
        updateStatusAPI()
       
        // Step 3, if current role is broadcaster,  stop preview after leave channel
        if settings.role == .broadcaster {
            // here remove the live, comment , like data from firebase
            remove(child: FirebaseData.Comments)
            remove(child: FirebaseData.Time)
            remove(child: FirebaseData.LiveCount)
            agoraKit.stopPreview()
            
        }
        else{
          
            let userID =  Helpers.sharedInstance.getFromDefaults(strKey: Consatnt.User_Defaults.kUserID)
            removePost(withID: userID)
            
        }
        setIdleTimerActive(true)
        navigationController?.popViewController(animated: true)
    }
    
    func removePost(withID: String) {
        if let id  = UserDefaults.standard.object(forKey: "livecountValue") as? String, id != ""{
               let reference = ref.child(FirebaseData.LiveCount).child(settings.roomName ?? "").child(id)
            reference.removeValue { error, reference in
                if error != nil {
                          } else {
                    UserDefaults.standard.removeObject(forKey: "livecountValue")
                               UserDefaults.standard.removeObject(forKey: "channelName")
                       }
            }
        }
 
    }
}

// MARK: - AgoraRtcEngineDelegate
extension LiveViewController: AgoraRtcEngineDelegate {
    // first local video frame
    
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinedOfUid uid: UInt, elapsed: Int) {
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinChannel channel: String, withUid uid: UInt, elapsed: Int){
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, firstLocalVideoFrameWith size: CGSize, elapsed: Int) {
        
        if let selfSession = videoSessions.first {
            selfSession.updateInfo(resolution: size)
        }
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didRejoinChannel channel: String, withUid uid: UInt, elapsed: Int) {
    }
    
    // local stats
    func rtcEngine(_ engine: AgoraRtcEngineKit, reportRtcStats stats: AgoraChannelStats) {
        if let selfSession = videoSessions.first {
            selfSession.updateChannelStats(stats)
        }
    }
    
    // first remote video frame
    func rtcEngine(_ engine: AgoraRtcEngineKit, firstRemoteVideoDecodedOfUid uid: UInt, size: CGSize, elapsed: Int) {
        guard videoSessions.count <= maxVideoSession else {
            return
        }
        
        let userSession = videoSession(of: uid)
        userSession.updateInfo(resolution: size)
        agoraKit.setupRemoteVideo(userSession.canvas)
    }
    
    // user offline
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOfflineOfUid uid: UInt, reason: AgoraUserOfflineReason) {
        var indexToDelete: Int?
        for (index, session) in videoSessions.enumerated() where session.uid == uid {
            indexToDelete = index
            break
        }
        
        if let indexToDelete = indexToDelete {
            let deletedSession = videoSessions.remove(at: indexToDelete)
            deletedSession.hostingView.removeFromSuperview()
            
            // release canvas's view
            deletedSession.canvas.view = nil
        }
    }
    
    // remote video stats
    func rtcEngine(_ engine: AgoraRtcEngineKit, remoteVideoStats stats: AgoraRtcRemoteVideoStats) {
        if let session = getSession(of: stats.uid) {
            session.updateVideoStats(stats)
        }
    }
    
    // remote audio stats
    func rtcEngine(_ engine: AgoraRtcEngineKit, remoteAudioStats stats: AgoraRtcRemoteAudioStats) {
        if let session = getSession(of: stats.uid) {
            session.updateAudioStats(stats)
        }
    }
    
    // warning code
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurWarning warningCode: AgoraWarningCode) {
        print("warning code: \(warningCode.description)")
    }
    
    // error code
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurError errorCode: AgoraErrorCode) {
        print("Error code: \(errorCode.description)")
    }
    
}

extension LiveViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LiveCommentsTableViewCell", for: indexPath) as! LiveCommentsTableViewCell
        cell.transform = CGAffineTransform(rotationAngle: (-.pi))
        cell.configure(comment: comments[indexPath.row])
          let heightDescription = dynamicHeight(font: UIFont.systemFont(ofSize: 14), width: self.view.frame.size.width - 80, str:  comments[indexPath.row].comment)
        cell.viewCommentBack.layer.cornerRadius = (heightDescription + 28)/2
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let heightDescription = dynamicHeight(font: UIFont.systemFont(ofSize: 14), width: self.view.frame.size.width - 80, str:  comments[indexPath.row].comment)
        return heightDescription + 40
    }
}

