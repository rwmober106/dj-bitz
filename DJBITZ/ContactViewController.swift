//
//  ContactViewController.swift
//  DJBITZ
//
//  Created by dung on 2/21/19.
//  Copyright © 2019 BUGUNSOFT. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {

    @IBOutlet fileprivate weak var tfFirstName: DJBitzTextField!
    @IBOutlet fileprivate weak var tfLastName: DJBitzTextField!
    @IBOutlet fileprivate weak var tfEmail: DJBitzTextField!
    @IBOutlet fileprivate weak var tfDJPreference: DJBitzTextField!
    @IBOutlet fileprivate weak var tfDate: DJBitzTextField!
    @IBOutlet fileprivate weak var tfEvent: DJBitzTextField!
    @IBOutlet fileprivate weak var tvNote: UITextView!
    @IBOutlet fileprivate weak var viewNote: UIView!

    @IBOutlet fileprivate weak var btnDropDown: UIButton!
    @IBOutlet fileprivate weak var arrowDown: UIImageView!

    @IBOutlet fileprivate weak var btnSignOut: UIButton!
    @IBOutlet fileprivate weak var btnConfirm: UIButton!
    @IBOutlet fileprivate weak var btnProfile: UIButton!

    @IBOutlet fileprivate weak var verticalSpace: NSLayoutConstraint!
    private var currentTextField: UITextField?

    var DJPreferenceDr = DropDown()
    var IDs = [String]()
    var selectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        self.tfFirstName.layer.cornerRadius = self.tfFirstName.frame.height/2
        self.tfFirstName.clipsToBounds = true

        self.tfLastName.layer.cornerRadius = self.tfLastName.frame.height/2
        self.tfLastName.clipsToBounds = true

        self.tfEmail.layer.cornerRadius = self.tfEmail.frame.height/2
        self.tfEmail.clipsToBounds = true

        self.tfDJPreference.layer.cornerRadius = self.tfDJPreference.frame.height/2
        self.tfDJPreference.clipsToBounds = true

        self.tfDate.layer.cornerRadius = self.tfDate.frame.height/2
        self.tfDate.clipsToBounds = true

        self.tfEvent.layer.cornerRadius = self.tfEvent.frame.height/2
        self.tfEvent.clipsToBounds = true

        self.viewNote.layer.cornerRadius = self.tfFirstName.frame.height/2
        self.viewNote.clipsToBounds = true

        self.btnConfirm.layer.cornerRadius = self.btnConfirm.frame.height/2
        self.btnConfirm.clipsToBounds = true
        self.btnConfirm.isEnabled = false

        self.btnSignOut.layer.cornerRadius = self.btnSignOut.frame.height/2
        self.btnSignOut.clipsToBounds = true
        
        self.btnProfile.layer.cornerRadius = self.btnProfile.frame.height/2
        self.btnProfile.clipsToBounds = true

        var str = NSAttributedString(string: "First Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        self.tfFirstName.attributedPlaceholder = str

        str = NSAttributedString(string: "Last Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        self.tfLastName.attributedPlaceholder = str

        str = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        self.tfEmail.attributedPlaceholder = str

        str = NSAttributedString(string: "DJ Preference", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        self.tfDJPreference.attributedPlaceholder = str

        str = NSAttributedString(string: "Date", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        self.tfDate.attributedPlaceholder = str

        str = NSAttributedString(string: "Event", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        self.tfEvent.attributedPlaceholder = str
        
        if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            if let viewController = navi.viewControllers.first as? ViewController {
                viewController.activeActivity(true)
                DJBitzAPI.shared.getDJsList { (success, list, error) in
                    // Must call in main thread
                    DispatchQueue.main.async {
                        // Hide progress activity
                        viewController.activeActivity(false)
                        
                        if (!success) {
                            let alert = UIAlertController(title: "Error", message: "Error while loading data from server. Please check your network connection and try again later.", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                            self.present(alert, animated: true)
                            
                            return
                        }

                        self.setupDropDowns()
                        self.DJPreferenceDr.dismissMode = .onTap
                        self.DJPreferenceDr.direction = .any
                        self.setupDefaultDropDown()
                    }
                }
            }
        }
        
        self.tvNote.placeholder = "Note"
        self.addDoneButtonOnKeyboard()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
        
        if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            if let viewController = navi.viewControllers.first as? ViewController {
                if AppDelegate.shared().player != nil {
                    viewController.initUI()
                   if viewController.playerView != nil{
                        viewController.playerView.isHidden = false
                    }
                }
                else {
                    if viewController.playerView != nil{
                        viewController.playerView.isHidden = true
                    }
                    
                }
            }
        }
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(textViewDidBegin),
            name: UITextView.textDidBeginEditingNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(textViewDidEnd),
            name: UITextView.textDidEndEditingNotification,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(contactInputWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(animated)

        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK:- Action Handle
    @IBAction func onButtonSignOut(_ sender: UIButton) {
        updateStatusAPI()
        ReferenceHelper.sharedInstance.loginInfo = nil
        ReferenceHelper.sharedInstance.userInfo = nil
        UserDefaults.standard.removeObject(forKey: "firebase_token")
        if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            if let viewController = navi.viewControllers.first as? ViewController {
                if let loginController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
                    viewController.present(loginController, animated: true, completion: nil)
                
                if viewController.tabbar == nil{
                    return
                }
                
                viewController.tabbar.selectedItem = viewController.tabbar.items?.first
                viewController.tabbarController.selectedIndex = 0
                if AppDelegate.shared().player != nil {
                    viewController.onButtonClose(UIButton())
                }
            }
        }
    }
    }
    
    func updateStatusAPI(){
         DJBitzAPI.shared.updateLiveStatus( ) { (isSuccess, token, channelName) in

         }
     }
    
    @IBAction func onButtonConfirm(_ sender: UIButton) {
        if let navi = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            if let viewController = navi.viewControllers.first as? ViewController {
                viewController.activeActivity(true)
                
                DJBitzAPI.shared.DJSendrequest(tfEmail.text!, tfFirstName.text!, tfLastName.text!, self.IDs[self.selectedIndex], tfDate.text!, tfEvent.text!, tvNote.text!) { (success) in
                    viewController.activeActivity(false)
                    
                    var title = "SUCCESS"
                    var message = "Congratulation Email Send Successfully."
                    if !success {
                        title = "FAILED"
                        message = "Email was failed to send."
                    }

                    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    
                    self.present(alert, animated: true)

                }
            }
        }
    }

    @IBAction func onButtonDropDown(_ sender: UIButton) {
        self.DJPreferenceDr.show()
        self.view.bringSubviewToFront(arrowDown)
    }
    
    @IBAction func onButtonProfile(_ sender: Any) {
        guard let profileVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController else {
            return
        }
        navigationController?.pushViewController(profileVC, animated: true)
    }
    
    
    //MARK:- UITextFieldDelegate
    
    // placeholder position
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.currentTextField = textField
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        self.currentTextField = nil
        
        UIView.animate(withDuration: 0.2) {
            self.verticalSpace.constant = 0
            self.view.layoutIfNeeded()
        }

        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    //MARK:- UITextViewDelegate
    @objc func textViewDidBegin(_ notification: Notification) {
        self.contactInputWillShow(Notification(name: UITextView.textDidBeginEditingNotification))
    }
    
    @objc func textViewDidEnd(_ notification: Notification) {
        UIView.animate(withDuration: 0.2) {
            self.verticalSpace.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        if text == "\n" {
//            self.tvNote.resignFirstResponder()
//            return false
//        }
//        return true
//    }
    
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        tvNote.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction(){
        tvNote.resignFirstResponder()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:- Drop Down
    func setupDropDowns() {
        let dropDown = DropDown()
        
        dropDown.anchorView = self.btnDropDown
        dropDown.bottomOffset = CGPoint(x: 0, y: self.btnDropDown.bounds.height)
        
        let names = AppDelegate.shared().artistsList?.djsRecords!.map({ (artist) -> String in
            return artist.name!
        })

        if let Ids = AppDelegate.shared().artistsList?.djsRecords!.map({ (artist) -> String in
            return artist.id!
        }) {
            self.IDs = Ids
        }

        dropDown.dataSource = names!
        
        
        // Action triggered on selection
        dropDown.selectionAction = { [weak self] (index, item) in
            
            self?.tfDJPreference.text = item
            self?.selectedIndex = index
            
            self?.btnConfirm.isEnabled = true
        }
        
        self.DJPreferenceDr = dropDown
    }
    
    func setupDefaultDropDown() {
        DropDown.setupDefaultAppearance()
        
        self.DJPreferenceDr.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
        self.DJPreferenceDr.customCellConfiguration = nil
    }

    @objc func contactInputWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            print("show keyboard")
            
            var superview:UIView? = nil
            var editorFrame:CGRect? = nil
            if self.currentTextField != nil {
                superview = self.currentTextField!.superview
                editorFrame = self.currentTextField!.frame
            }
            else {
                superview = self.tvNote!.superview
                editorFrame = self.tvNote!.frame
            }
            
            guard superview != nil else {
                return
            }
            
            let textfieldFrame = superview!.convert(editorFrame!, to: self.view)
            
            let y = keyboardHeight - (self.view.frame.height - textfieldFrame.origin.y - textfieldFrame.height)
            if y > 0 {
                
                UIView.animate(withDuration: 0.2) {
                    self.verticalSpace.constant = self.verticalSpace.constant - y
                    
                    self.view.layoutIfNeeded()
                }
                
            }
            
        }
    }

}
