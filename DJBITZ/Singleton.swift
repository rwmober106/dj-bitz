//
//  Singleton.swift
//  DJBITZ
//
//  Created by Rakinder on 17/06/20.
//  Copyright © 2020 BUGUNSOFT. All rights reserved.
//

import UIKit

class Singleton: NSObject {
    static var shared = Singleton()
    
    func generateRandomDigits(_ digitNumber: Int) -> String {
        var number = ""
        for i in 0..<digitNumber {
            var randomNumber = arc4random_uniform(10)
            while randomNumber == 0 && i == 0 {
                randomNumber = arc4random_uniform(10)
            }
            number += "\(randomNumber)"
        }
        return number
    }

}
