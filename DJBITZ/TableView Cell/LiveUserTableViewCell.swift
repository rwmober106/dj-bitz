//
//  LiveUserTableViewCell.swift
//  DJBITZ
//
//  Created by Rakinder on 27/06/20.
//  Copyright © 2020 BUGUNSOFT. All rights reserved.
//

import UIKit


protocol liveJoinDelegate : class {
    func delegateJoin(int: Int)
}

class LiveUserTableViewCell: UITableViewCell {

     weak var delegate: liveJoinDelegate?
    @IBOutlet weak var btnJoinLive: UIButton!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgVUser: UIImageView!
    var index = Int()
    override func awakeFromNib() {
        super.awakeFromNib()
       
        // Initialization code
    }
    
    override func layoutSubviews() {
        btnJoinLive.layer.cornerRadius = 14
        imgVUser.layer.cornerRadius = 25
        imgVUser.layer.borderWidth = 2
        imgVUser.layer.borderColor = UIColor(red: 171/255, green: 38/255, blue: 250/255, alpha: 1.0).cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func joinLiveClicked(_ sender: Any) {
        delegate?.delegateJoin(int: index)
    }
    
    
}
