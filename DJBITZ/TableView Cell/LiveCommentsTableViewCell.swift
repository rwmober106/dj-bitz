//
//  LiveCommentsTableViewCell.swift
//  DJBITZ
//
//  Created by Rakinder on 04/06/20.
//  Copyright © 2020 BUGUNSOFT. All rights reserved.
//

import UIKit


class LiveCommentsTableViewCell: UITableViewCell {

   
    @IBOutlet weak var imgVLike: UIImageView!
    @IBOutlet var lblComment: UILabel!
    @IBOutlet var lblUsername: UILabel!
    @IBOutlet var imgVUser: UIImageView!
    @IBOutlet var viewCommentBack: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewCommentBack.layer.cornerRadius = 25
        // Initialization code
    }

    func configure(comment: Comments){
        lblComment.text = comment.comment
        lblUsername.text = comment.username
        if comment.profile_avatar != "" {
            imgVUser.sd_setImage(with: URL(string: String(format: "%@%@", DJBitzAPI.mainURL, comment.profile_avatar)), completed: nil)
        }
        if comment.isLike{
            imgVLike.isHidden = false
            lblComment.isHidden = true
        }
        else{
            imgVLike.isHidden = true
            lblComment.isHidden = false
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
